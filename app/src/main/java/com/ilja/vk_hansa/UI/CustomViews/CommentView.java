package com.ilja.vk_hansa.UI.CustomViews;

import android.content.Context;
import android.support.v7.widget.CardView;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.Model.Vk.Market.Comments.AdaptiveVkComments;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.UIHelps.CircleTransform;
import com.squareup.picasso.Picasso;

/**
 * Created by Ilja on 20.03.2016.
 */
public class CommentView extends CardView {

    private StringBuilder mStringBuilder;
    private ImageView mImageLikes;
    private ImageView mImagePhoto;
    private TextView mTextName;
    private TextView mTextData;
    private TextView mTextCommentContent;
    private TextView mTextLikesCount;
    private Picasso mImageLoader;

    public CommentView(Context mContext) {
        super(mContext);

        init();
    }

    private void init() {

        inflate(getContext(), R.layout.item_comment, this);

        mStringBuilder = new StringBuilder();

        mImageLikes = (ImageView) findViewById(R.id.image_comment_likes);
        mImagePhoto = (ImageView) findViewById(R.id.image_comment_photo);
        mTextName = (TextView) findViewById(R.id.text_comment_name);
        mTextData = (TextView) findViewById(R.id.text_comment_date);
        mTextCommentContent = (TextView) findViewById(R.id.text_comment_text);
        mTextLikesCount = (TextView) findViewById(R.id.text_comment_likes);

        this.setUseCompatPadding(true);

//        CardView.LayoutParams lp =  new CardView.LayoutParams(CardView.LayoutParams.WRAP_CONTENT,CardView.LayoutParams.WRAP_CONTENT);
//
//        lp.setMargins(100,100,100,0);
//
//
//        this.setLayoutParams(lp);

    }


    public void setAddaptiveComment(AdaptiveVkComments comment) {
        if (comment != null) {
            mTextName.setText(comment.getOwner_name());
            mTextData.setText(comment.getDate());
            mTextCommentContent.setText(comment.getText());
            mStringBuilder.setLength(0);
            mTextLikesCount.setText(mStringBuilder.append(comment.getLikes().getCount()));

            mImageLoader.with(getContext()).load(comment.getOwner_photo_url()).transform(new CircleTransform(false, 0)).into(mImagePhoto);
            mImageLoader.with(getContext()).load(R.drawable.ic_heart_orange).into(mImageLikes);

        }

    }

}
