package com.ilja.vk_hansa.UI.Activity;

import android.app.Activity;
import android.os.Bundle;
import android.support.v4.view.ViewPager;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.FullScreenImageAdapter;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.ProgressView;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;

import me.relex.circleindicator.CircleIndicator;

/**
 * Created by Ilja on 21.03.2016.
 */
public class FullScreenViewActivity extends Activity {


    private final String LOG_TAG = "FullScreenActivity";
    private FullScreenImageAdapter adapter;
    private ViewPager viewPager;
    private int mOwnerId;
    private int mItemId;
    private VKRequest mVkRequest;
    private VkItem mCurrentItem;
    private int mPosition;
    private StringBuilder mStringBuilder;
    private ProgressView mProgressView;
    private CircleIndicator mIndicator;


    private static class VkRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkLikeRequestListner";
        private final WeakReference<FullScreenViewActivity> mWeakRefParent;
        private VkAPIFactory mVkApiFactory;

        private VkRequestListner(FullScreenViewActivity activity) {
            this.mWeakRefParent = new WeakReference<>(activity);

            this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            //  Log.d(TAG, response.json.toString());
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onItemInfoLoaded(mVkApiFactory.getItem(response));
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);

            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onLoadError(boolean b) {


    }


    private void onItemInfoLoaded(VkItem item) {

        if (item != null) {


            mCurrentItem = item;

            adapter = new FullScreenImageAdapter(getApplicationContext(),
                    mCurrentItem.getPhotos());

            viewPager.setAdapter(adapter);


            viewPager.setCurrentItem(mPosition);

            if (item.getPhotos().size() > 1) mIndicator.setViewPager(viewPager);

        } else {

        }


    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fullscreen_view);

        mStringBuilder = new StringBuilder();

        viewPager = (ViewPager) findViewById(R.id.pager);
        mIndicator = (CircleIndicator) findViewById(R.id.indicator_view_pager_fullscreen);


        mPosition = getIntent().getIntExtra(HansaApplication.POSITION_ID, 0);
        mOwnerId = getIntent().getIntExtra(HansaApplication.OWNER_ID, 0);
        mItemId = getIntent().getIntExtra(HansaApplication.ITEM_ID, 0);

        //   Log.d(LOG_TAG,mPosition+" "+mOwnerId+" "+mItemId);

        getItemInfo();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVkRequest != null) mVkRequest.cancel();
    }

    private void getItemInfo() {


        //  mCurrentItemID = "-59846185_75156";
        mStringBuilder.setLength(0);
        mStringBuilder.append(mOwnerId).append("_").append(mItemId);
        mVkRequest = new VKRequest("market.getById", VKParameters.from("item_ids", mStringBuilder, VKApiConst.EXTENDED, "1"));

        mVkRequest.executeWithListener(new VkRequestListner(this));
    }


}
