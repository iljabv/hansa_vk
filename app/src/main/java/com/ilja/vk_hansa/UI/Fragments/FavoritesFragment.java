package com.ilja.vk_hansa.UI.Fragments;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.FavoritesViewPagerAdapter;


public class FavoritesFragment extends Fragment {

    private ViewPager mViewPager;

    static FavoritesFragment newInstance(int page) {
        FavoritesFragment pageFragment = new FavoritesFragment();
        Bundle arguments = new Bundle();
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container != null) {
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.fragment_favorites, null);

        mViewPager = (ViewPager) view.findViewById(R.id.viewpager_favorites);

        setupViewPager(mViewPager);

        TabLayout tabLayout = (TabLayout) view.findViewById(R.id.tabs_favorites);
        tabLayout.setupWithViewPager(mViewPager);

        return view;
    }

    private void setupViewPager(ViewPager viewPager) {
        FavoritesViewPagerAdapter adapter = new FavoritesViewPagerAdapter(getChildFragmentManager(), getActivity().getApplicationContext());

        adapter.addFragment(new FavoritesPageFragment(), getString(R.string.favorite));
        adapter.addFragment(new FavoritesPageFragment(), getString(R.string.recently_viewed));
        adapter.addFragment(new FavoritesPageFragment(), getString(R.string.interesting));
        viewPager.setAdapter(adapter);
    }

}
