package com.ilja.vk_hansa.UI.Fragments;

import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Hansa.HansaSearchResponse;
import com.ilja.vk_hansa.Model.Hansa.HansaSearchResponseItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.HansaSearchActivity;
import com.ilja.vk_hansa.UI.Adapters.VKItemAdapter;
import com.ilja.vk_hansa.Web.HansaParamsApi;
import com.ilja.vk_hansa.Web.IHansaAPI;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ilja on 09.03.2016.
 */

public class StartFragment extends Fragment implements View.OnClickListener {

    private final int PROGRESS_VISIBILITY = 0;
    private final int ERROR_VISIBILITY = 1;
    private final int SUCCESS_VISIBILITY = 2;
    private final int COUNT_ELEMENTS_ON_START = 6;

    private final int POPULAR_REQUEST = 0;
    private final int NEW_ITEMS_REQUEST = 1;

    private final String LOG_TAG = "StartFragment";
    private final String APIURL = "http://apihansa-tinymagic.rhcloud.com/";

    private SearchView mSearchView;
    private MenuItem mMenuItem;
    private Button mButtonStartSearch;
    private Call mHansaSearchCall;
    private TextView mTextItemsHansaCount;
    private TextView mTextItemsHansaCountLabel;
    private RecyclerView mRecyclerPopular;
    private RecyclerView mRecyclerNewItems;
    private GridLayoutManager mGridLayoutManagerNewItems;
    private GridLayoutManager mGridLayoutManagerPopular;
    private ProgressView mProgressView;

    private CardView mCardPopular;
    private CardView mCardNewItems;
    private CardView mCardDataBaseInfo;
    private FrameLayout mErrorView;
    private StringBuilder mStringBuilder;

    private HansaParamsApi newQuery;
    private IHansaAPI restApi;
    private VKRequest mVkItemsRequest;
    private Button mButtonMoreNewComments;
    private Button mButtonMorePopular;
    private List<VkItem> mVkPopularList;
    private List<VkItem> mVkNewItemsList;

    private VKItemAdapter mVkPopularAdapter;
    private VKItemAdapter mVkNewItemsAdapter;


    private static class VkItemsRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<StartFragment> mWeakRefParent;
        private VkAPIFactory mVkApiFactory;
        private int mType;

        private VkItemsRequestListner(StartFragment frag, int type) {
            this.mWeakRefParent = new WeakReference<>(frag);
            this.mVkApiFactory = new VkAPIFactory();
            this.mType = type;
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            //   Log.d(TAG, response.json.toString());
            if (mWeakRefParent.get() != null) {

                mWeakRefParent.get().onVkItemsLoaded(mVkApiFactory.getItemList(response), mType);
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onVkItemsLoaded(List<VkItem> itemList, int mType) {
        if (itemList != null) {
            switch (mType) {
                case POPULAR_REQUEST: {
                    mVkPopularList = itemList;

                    mRecyclerNewItems.setNestedScrollingEnabled(false);

                    mVkPopularAdapter = new VKItemAdapter(getContext(), mVkPopularList, true, false);

                    mRecyclerPopular.setAdapter(mVkPopularAdapter);

                    getItemsFromHansa(NEW_ITEMS_REQUEST);
                }
                break;
                case NEW_ITEMS_REQUEST: {

                    mVkNewItemsList = itemList;
                    mRecyclerNewItems.setNestedScrollingEnabled(false);
                    mVkNewItemsAdapter = new VKItemAdapter(getContext(), mVkNewItemsList, true, false);
                    mRecyclerNewItems.setAdapter(mVkNewItemsAdapter);
                    setVisibility(SUCCESS_VISIBILITY);
                    break;
                }
            }
        }

    }


    private static class HansaSearchCallback implements Callback<HansaSearchResponse> {

        private final String TAG = "HansaSearchCallback";
        private final WeakReference<StartFragment> mWeakRefParent;

        private int mType;
        private VkAPIFactory mVkApiFactory;

        private HansaSearchCallback(StartFragment frag, int type) {
            this.mWeakRefParent = new WeakReference<>(frag);
            this.mVkApiFactory = new VkAPIFactory();
            this.mType = type;
        }

        @Override
        public void onResponse(Response<HansaSearchResponse> response) {

            //      Log.d(TAG, response.body().getResults().get(0).getId() + "");

            if (mWeakRefParent.get() != null) {


                mWeakRefParent.get().onHansaResponseLoaded(response.body().getResults(), mType, response.body().getItemscount());

            }


        }

        @Override
        public void onFailure(Throwable t) {


            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }

        }
    }

    private void onHansaResponseLoaded(List<HansaSearchResponseItem> results, int type, Integer itemscount) {

        switch (type)

        {
            case POPULAR_REQUEST: {
                mStringBuilder.setLength(0);
                mTextItemsHansaCount.setText(mStringBuilder.append(itemscount));
                mTextItemsHansaCountLabel.setText(getResources().getQuantityString(R.plurals.items, itemscount));

                getItemsFromVK(results, type);

            }
            break;
            case NEW_ITEMS_REQUEST: {
                getItemsFromVK(results, type);
                break;
            }

        }


    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mHansaSearchCall != null && mHansaSearchCall.isExecuted()) mHansaSearchCall.cancel();
        if (mVkItemsRequest != null) mVkItemsRequest.cancel();

//        RefWatcher refWatcher = HansaApplication.getRefWatcher(getActivity());
//        refWatcher.watch(this);
    }


    private void onLoadError(boolean b) {

        setVisibility(ERROR_VISIBILITY);
    }


    static StartFragment newInstance(int page) {
        StartFragment pageFragment = new StartFragment();
        Bundle arguments = new Bundle();

        pageFragment.setArguments(arguments);
        return pageFragment;
    }


    @Override
    public void onPause() {
        super.onPause();

        // mScrollOffset = mLinearLayoutManager.findFirstVisibleItemPosition();

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mMenuItem != null) mMenuItem.collapseActionView();
        if (mVkNewItemsList != null) {
            mVkNewItemsAdapter = new VKItemAdapter(getContext(), mVkNewItemsList, true, false);
            mRecyclerNewItems.setAdapter(mVkNewItemsAdapter);


        }
        if (mVkPopularList != null) {
            mVkPopularAdapter = new VKItemAdapter(getContext(), mVkPopularList, true, false);
            mRecyclerPopular.setAdapter(mVkPopularAdapter);


        }
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    public class CustomGridLayoutManager extends GridLayoutManager {
        public CustomGridLayoutManager(Context context, int count) {
            super(context, count);
            //  super(context);
        }


        @Override
        public boolean canScrollVertically() {
            return false;
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_start, null);
        mButtonStartSearch = (Button) view.findViewById(R.id.button_start_search);
        mButtonStartSearch.setOnClickListener(this);

        mStringBuilder = new StringBuilder();
        mButtonMoreNewComments = (Button) view.findViewById(R.id.button_more_new_items);
        mButtonMoreNewComments.setOnClickListener(this);

        mButtonMorePopular = (Button) view.findViewById(R.id.button_more_popular);
        mButtonMorePopular.setOnClickListener(this);


        mTextItemsHansaCount = (TextView) view.findViewById(R.id.text_count_num);
        mTextItemsHansaCountLabel = (TextView) view.findViewById(R.id.text_count_items_text);
        mRecyclerPopular = (RecyclerView) view.findViewById(R.id.recyclerview_popular);
        mRecyclerNewItems = (RecyclerView) view.findViewById(R.id.recyclerview_newitems);

        mGridLayoutManagerPopular = new CustomGridLayoutManager(getActivity(), 2); //Creating LayoutManager

        //    mRecyclerPopular.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, getContext()));
        mRecyclerPopular.setLayoutManager(mGridLayoutManagerPopular); //Setting params
        //   mRecyclerPopular.setHasFixedSize(true); //Setting params

        mGridLayoutManagerNewItems = new CustomGridLayoutManager(getActivity(), 2); //Creating LayoutManager
        // mRecyclerNewItems.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, getContext()));
        mRecyclerNewItems.setLayoutManager(mGridLayoutManagerNewItems); //Setting params
        // mRecyclerNewItems.setHasFixedSize(true); //Setting params

        mProgressView = (ProgressView) view.findViewById(R.id.progressbar_start);

        mCardPopular = (CardView) view.findViewById(R.id.cardview_popular);
        mCardNewItems = (CardView) view.findViewById(R.id.cardview_new_items);
        mCardDataBaseInfo = (CardView) view.findViewById(R.id.cardview_databaseinfo);

       // mErrorView = (FrameLayout) view.findViewById(R.id.error_view);

        setVisibility(PROGRESS_VISIBILITY);

        Retrofit restAdapter = new Retrofit.Builder().baseUrl(APIURL).addConverterFactory(GsonConverterFactory.create()).build();
        restApi = restAdapter.create(IHansaAPI.class);

        getItemsFromHansa(POPULAR_REQUEST);

        setHasOptionsMenu(true);
        return view;
    }

    private void setVisibility(int i) {

        switch (i) {
            //Progress Visibility:
            case PROGRESS_VISIBILITY:
                mCardDataBaseInfo.setVisibility(View.GONE);
                mCardNewItems.setVisibility(View.GONE);
                mCardPopular.setVisibility(View.GONE);
               // mErrorView.setVisibility(View.GONE);
                mProgressView.setVisibility(View.VISIBLE);
                break;

            case ERROR_VISIBILITY:
                mCardDataBaseInfo.setVisibility(View.GONE);
                mCardNewItems.setVisibility(View.GONE);
                mCardPopular.setVisibility(View.GONE);
            //    mErrorView.setVisibility(View.VISIBLE);
                mProgressView.setVisibility(View.GONE);
                break;

            case SUCCESS_VISIBILITY:
                mCardDataBaseInfo.setVisibility(View.VISIBLE);
                mCardNewItems.setVisibility(View.VISIBLE);
                mCardPopular.setVisibility(View.VISIBLE);
           //     mErrorView.setVisibility(View.GONE);
                mProgressView.setVisibility(View.GONE);
                break;
        }
    }


    private void getItemsFromHansa(int type) {

        setVisibility(PROGRESS_VISIBILITY);

        switch (type) {
            case POPULAR_REQUEST:
                newQuery = new HansaParamsApi(3, null, -1, -1, 1, -1, -1, -1);
                break;

            case NEW_ITEMS_REQUEST:
                newQuery = new HansaParamsApi(1, null, -1, -1, 1, -1, -1, -1);
                break;
        }
        /******************************
         *
         * Alternative Using:
         *
         newQuery = new HansaParamsApi();
         newQuery.setmSort(mSortType).setmSearch(mQueryStr).setmMaxprice(mMaxPrice);
         ********************************/

        // onLoadMoreProgress.setVisibility(View.VISIBLE);

        mHansaSearchCall = restApi.getItemsWithParams(newQuery.GetMap());
        mHansaSearchCall.enqueue(new HansaSearchCallback(this, type));

    }


    private void getItemsFromVK(List<HansaSearchResponseItem> listItems, int type) {

        if (listItems.isEmpty()) return;


        mStringBuilder.setLength(0);
        for (int i = 0; i < COUNT_ELEMENTS_ON_START; i++) {
            mStringBuilder.append(listItems.get(i).getOwnerId()).append("_").append(listItems.get(i).getId()).append(",");
        }
        mStringBuilder.setLength(mStringBuilder.length() - 1);

        mVkItemsRequest = new VKRequest("market.getById", VKParameters.from("item_ids", mStringBuilder.toString(), VKApiConst.EXTENDED, "1"));

        mVkItemsRequest.executeWithListener(new VkItemsRequestListner(this, type));
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh_start:  // it is going to refer the search id name in main.xml
                getItemsFromHansa(POPULAR_REQUEST);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        //  super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_start, menu);

        //  Log.d(LOG_TAG,"Inflate Search view");

        SearchManager searchManager = (SearchManager) getActivity().getSystemService(Context.SEARCH_SERVICE);
        mMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) mMenuItem.getActionView();

        // mMenuItem.collapseActionView();

        ComponentName componentName = getActivity().getComponentName();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(componentName);
        mSearchView.setSearchableInfo(searchableInfo);

        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Intent intent = new Intent(getContext(), HansaSearchActivity.class);

                intent.putExtra(HansaApplication.SEARCH_QUERY, query);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                getContext().startActivity(intent);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {

                return false;
            }
        });


    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_start_search:
                mMenuItem.expandActionView();
                break;
            case R.id.button_more_new_items: {
                Intent intent = new Intent(getContext(), HansaSearchActivity.class);

                Bundle bundle = new Bundle();
                bundle.putInt("sortType", 1);
                bundle.putBoolean("isStartSearchNeeded", true);

                intent.putExtras(bundle);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                getContext().startActivity(intent);

            }
            break;

            case R.id.button_more_popular: {
                Intent intent = new Intent(getContext(), HansaSearchActivity.class);

                Bundle bundle = new Bundle();
                bundle.putInt("sortType", 3);
                bundle.putBoolean("isStartSearchNeeded", true);

                intent.putExtras(bundle);

                intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                getContext().startActivity(intent);
            }


            break;
        }
    }


}
