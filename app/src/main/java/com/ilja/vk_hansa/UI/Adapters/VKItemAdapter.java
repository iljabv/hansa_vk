package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.UI.CustomViews.ItemSearchView;

import java.util.List;

/**
 * Created by Egorov on 05.02.2016.
 */
public class VKItemAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static class VkItemHolder extends RecyclerView.ViewHolder {

        public VkItemHolder(View itemView) {
            super(itemView);

        }
    }


    private static final String LOG_TAG = "VKItemAdapter";
    private boolean mLayoutType;
    private List<VkItem> itemList;
    private Context mContext;
    private boolean isLikeVisivle;
    private ItemSearchView mItemSearchView;

    public VKItemAdapter(Context context, List<VkItem> itemList, boolean layoutType, boolean isLikeVisible) {
        this.itemList = itemList;
        this.mContext = context;
        this.isLikeVisivle = isLikeVisible;
        this.mLayoutType = layoutType;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        VkItemHolder pvh;

        v = new ItemSearchView(mContext, mLayoutType, isLikeVisivle);
        pvh = new VkItemHolder(v);
        return pvh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (itemList != null) {

            mItemSearchView = (ItemSearchView) holder.itemView;

            mItemSearchView.setItem(itemList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return this.itemList.size();
    }
}
