package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;

import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Fragments.FavoritesPageFragment;

import java.util.ArrayList;
import java.util.List;


/**
 * Created by Ilja on 10.02.2016.
 */
public class FavoritesViewPagerAdapter extends FragmentPagerAdapter {
    private final List<FavoritesPageFragment> mFragments = new ArrayList<>();
    private final List<String> mFragmentTitles = new ArrayList<>();
    private final Context mContext;


    private int[] imageResId = {
            R.drawable.ic_heart_white_padding,
            R.drawable.ic_action_ic_visibility_white,
            R.drawable.ic_basket_white
    };

    public FavoritesViewPagerAdapter(FragmentManager fm, Context context) {
        super(fm);
        this.mContext = context;
    }

    public void addFragment(FavoritesPageFragment fragment, String title) {
        mFragments.add(fragment);
        mFragmentTitles.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return mFragments.get(position).newInstance(position);
    }

    @Override
    public int getCount() {
        return mFragments.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {

        Drawable image = ContextCompat.getDrawable(mContext, imageResId[position]);
        image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
        SpannableString sb = new SpannableString(" ");
        ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
        sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        return sb;


    }
}
