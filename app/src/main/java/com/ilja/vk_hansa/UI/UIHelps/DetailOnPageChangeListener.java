package com.ilja.vk_hansa.UI.UIHelps;

import android.support.v4.view.ViewPager;

/**
 * Created by Ilja on 21.03.2016.
 */
public class DetailOnPageChangeListener extends ViewPager.SimpleOnPageChangeListener {

    private int currentPage = 0;

    @Override
    public void onPageSelected(int position) {
        currentPage = position;
    }

    public final int getCurrentPage() {
        return currentPage;
    }
}