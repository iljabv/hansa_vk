package com.ilja.vk_hansa.UI.UIHelps;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

public class CollapsingButtonAnimation extends Animation {
    private int height;
    private int width;

    private View view;
    private float heightStep;
    private float widthStep;

    public CollapsingButtonAnimation(View view, int height, int width) {
        this.view = view;
        this.height = height;
        this.width = width;
        this.heightStep = (0 - height);
        this.widthStep = (0 - width);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        view.getLayoutParams().height = (int) (height + heightStep * interpolatedTime);
        view.getLayoutParams().width = (int) (width + widthStep * interpolatedTime);

        view.requestLayout();
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
