package com.ilja.vk_hansa.UI.Activity;


import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Fragments.CategoryFragment;
import com.ilja.vk_hansa.UI.Fragments.FavoritesFragment;
import com.ilja.vk_hansa.UI.Fragments.ManageStoresFragment;
import com.ilja.vk_hansa.UI.Fragments.MyStoresFragment;
import com.ilja.vk_hansa.UI.Fragments.StartFragment;
import com.ilja.vk_hansa.UI.UIHelps.CircleTransform;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.squareup.picasso.Picasso;
import com.vk.sdk.VKSdk;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private String LOG_TAG = "HomeActivity";
    private int mPosition;
    private DrawerLayout mDrawer;
    private Toolbar toolbar;
    private NavigationView nvDrawer;
    private View header;
    private ActionBarDrawerToggle drawerToggle;
    private boolean doubleBackToExitPressedOnce = false;
    private VKRequest storeAlbumsVKRequest;
    private ImageView avatar;
    private ArrayList<String> customBackStack; //backstack container pairs {tag: Fragment}
    private TextView avatarName;
    private FragmentManager fragmentManager;
    private boolean isAvatarLoaded = true;
    private FragmentTransaction fragmentTransaction;
    private ImageView backgroundImage;
    private String avatarUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
        boolean loginSuccess = prefs.getBoolean(getString(R.string.login_check), false);

        if (!loginSuccess) {
            startActivity(new Intent(this, LoginActivity.class));
        }
        mPosition = 0;
        setContentView(R.layout.activity_default);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        fragmentManager = getSupportFragmentManager();
//        String[] fingerprints = VKUtil.getCertificateFingerprint(this, this.getPackageName());
//        Log.d("KEY", Arrays.toString(fingerprints));


        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        nvDrawer = (NavigationView) findViewById(R.id.nvView);
        // Setup drawer view
        setupDrawerContent(nvDrawer);

        drawerToggle = setupDrawerToggle();


        mDrawer.setDrawerListener(drawerToggle);
        header = nvDrawer.inflateHeaderView(R.layout.nav_drawer_header);
        backgroundImage = (ImageView)header.findViewById(R.id.image_background);
        avatar = (ImageView) header.findViewById(R.id.image_avatar_photo);
        avatarName = (TextView) header.findViewById(R.id.text_avatar_name);

        loadAvatar();

        customBackStack = new ArrayList<>();
        setStartFragment();
    }


    @Override
    public void onResume()
    {
        super.onResume();
        Picasso.with(getApplicationContext()).load(R.drawable.back_avatar_500).into(backgroundImage);
        if(avatarUrl!=null)
        {
            Picasso.with(HomeActivity.this).load(avatarUrl).transform(new CircleTransform(true, getResources().getColor(R.color.warm_white))).into(avatar);
        }
        else {loadAvatar();}
    }

    @Override
    public void onPause()
    {
        super.onPause();
        avatar.setImageResource(0);
        backgroundImage.setImageResource(0);
    }


    private void setStartFragment() {
        Class fragmentClass = StartFragment.class;
        Fragment fragment = null;
        try {
            fragment = (Fragment) fragmentClass.newInstance();
        } catch (Exception e) {
            e.printStackTrace();
        }

        fragmentManager = getSupportFragmentManager();

        fragmentTransaction = fragmentManager.beginTransaction();

        //  fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);

        fragmentTransaction.replace(R.id.flContent, fragment);

        fragmentTransaction.commit();

        customBackStack.add("Hansa VK"); //Temporary until start fragment got it's name. Adding into backStack
//        // Highlight the selected item, update the title, and close the drawer
//        menuItem.setChecked(true);
//        setTitle(menuItem.getTitle());
//        mDrawer.closeDrawers();

    }

    private void setupDrawerContent(NavigationView navigationView) {
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {
                        selectDrawerItem(menuItem);
                        return true;
                    }
                });
    }

    private ActionBarDrawerToggle setupDrawerToggle() {
        return new ActionBarDrawerToggle(this, mDrawer, toolbar, R.string.drawer_open, R.string.drawer_close) {
            @Override
            public void onDrawerOpened(View drawerView) {
                if (!isAvatarLoaded) {
                    loadAvatar();
                }

                super.onDrawerOpened(drawerView);
                //invalidateOptionsMenu();
            }
        };
    }

    public void selectDrawerItem(MenuItem menuItem) {
        // Create a new fragment and specify the planet to show based on
        // position
        Fragment fragment = null;
        boolean toHansaSearch = false, logout = false;
        String tag = null;

        Bundle bundle = new Bundle();


        Class fragmentClass = null;
        switch (menuItem.getItemId()) {

            case R.id.nav_menu_home:
                if (mPosition == 0) {
                    mDrawer.closeDrawers();
                    return;
                }
                fragmentClass = StartFragment.class;
                tag = "start_fragment";
                mPosition = 0;
                //  toHansaSearch = true;
                break;
            case R.id.nav_menu_category:
                if (mPosition == 1) {
                    mDrawer.closeDrawers();
                    return;
                }
                fragmentClass = CategoryFragment.class;
                tag = "category_fragment";
                mPosition = 1;
                break;
            case R.id.nav_menu_favorites:
                if (mPosition == 2) {
                    mDrawer.closeDrawers();
                    return;
                }
                fragmentClass = FavoritesFragment.class;
                tag = "favorites_fragment";
                mPosition = 2;
                break;
            case R.id.nav_menu_mystores:
                if (mPosition == 3) {
                    mDrawer.closeDrawers();
                    return;
                }
                fragmentClass = MyStoresFragment.class;

                tag = "stores_fragment";
                mPosition = 3;
                break;
            case R.id.nav_menu_manage:
                if (mPosition == 4) {
                    mDrawer.closeDrawers();
                    return;
                }
                fragmentClass = ManageStoresFragment.class;

                tag = "manage_fragment";
                mPosition = 4;
                break;
            case R.id.nav_menu_logout:
                logout = true;
                mPosition = 0;
                VKSdk.logout();
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(getString(R.string.login_check), Boolean.FALSE);
                edit.commit();
                Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                finish();
                return;

            default:
                fragmentClass = StartFragment.class;  //TODO: Set normal default value
        }

        if (!logout) {
            try {
                fragment = (Fragment) fragmentClass.newInstance();
            } catch (Exception e) {
                e.printStackTrace();
            }



                /*try{
                    fragmentManager.popBackStackImmediate(tag, FragmentManager.POP_BACK_STACK_INCLUSIVE);
                    Log.d(LOG_TAG, "Success pop, TAG: " + tag);
                } catch (Exception e){ Log.d(LOG_TAG, "Error pop, TAG: " + tag + ", " + e.getMessage()); } */

            //customBackStack.remove(tag); //removing previous instance of new fragment
            customBackStack.remove(menuItem.getTitle().toString());


            /*if (tag == "category_fragment") { //in case subcategory fragment is active
                customBackStack.remove("category_fragment_2");
            }*/
            if (menuItem.getTitle().toString().equals(getResources().getString(R.string.category))) {
                customBackStack.remove("category_fragment_2");
            }

            fragmentTransaction = fragmentManager.beginTransaction();

            fragmentTransaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);

            fragmentTransaction.replace(R.id.flContent, fragment);

            //fragmentTransaction.addToBackStack(null);

            //fragmentTransaction.addToBackStack(tag);

            //customBackStack.put(tag, fragment); //new instance in backstack now
            customBackStack.add(menuItem.getTitle().toString());


            fragmentTransaction.commit();
            // Highlight the selected item, update the title, and close the drawer
            menuItem.setChecked(true);
            setTitle(menuItem.getTitle());
            mDrawer.closeDrawers();
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        if (storeAlbumsVKRequest != null) storeAlbumsVKRequest.cancel();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // The action bar home/up action should open or close the drawer.
        if (drawerToggle.onOptionsItemSelected(item)) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    // Make sure this is the method with just `Bundle` as the signature
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        drawerToggle.syncState();
    }


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggles
        drawerToggle.onConfigurationChanged(newConfig);
    }


    private static class VKRequestAvatarName extends VKRequest.VKRequestListener {
        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<HomeActivity> mWeakRefParent;
        private VkAPIFactory mVkApiFactory;

        private VKRequestAvatarName(HomeActivity activity) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);

            if (mWeakRefParent.get() != null) {

                mWeakRefParent.get().onUserInfoLoaded(mVkApiFactory.getAvatar(response));
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onLoadError(boolean b) {
        isAvatarLoaded = false;
        Picasso.with(HomeActivity.this).load(R.drawable.vk_dog).
                transform(new CircleTransform(true, Color.WHITE)).into(avatar);
    }

    private void onUserInfoLoaded(String[] parsed) {
        if (parsed != null) {
            isAvatarLoaded = true;

            // Log.d(LOG_TAG, "URL=" + parsed[2]);
            avatarUrl = parsed[2];
            Picasso.with(HomeActivity.this).load(avatarUrl).transform(new CircleTransform(true, getResources().getColor(R.color.warm_white))).into(avatar);
            //   Picasso.with(HomeActivity.this).load(parsed[2]).into(avatar);

            avatarName.setText(parsed[0] + " " + parsed[1]);
        }

    }


    private void loadAvatar() {

        storeAlbumsVKRequest = new VKRequest("users.get", VKParameters.from(VKApiConst.FIELDS, "photo_200_orig"));

        storeAlbumsVKRequest.executeWithListener(new VKRequestAvatarName(this));
    }

    @Override
    public void onBackPressed() { //Going back through backstack

        if (customBackStack.size() >= 2) {


            String tag = null;
            Class fragmentClass = null;
            Resources res = getResources();

            //Iterator iterator = customBackStack.keySet().iterator();
            //while (iterator.hasNext()) { tag = (String) iterator.next(); }
            //customBackStack.remove(tag);

            customBackStack.remove(customBackStack.size() - 1); //Removing current last;

            tag = customBackStack.get(customBackStack.size() - 1);


            if (tag.equals(res.getString(R.string.home)) || tag.equals(res.getString(R.string.app_name))) {
                fragmentClass = StartFragment.class;
            } else if (tag.equals(res.getString(R.string.category)) || tag.equals("category_fragment_2")) {
                fragmentClass = CategoryFragment.class;
            } else if (tag.equals(res.getString(R.string.favorites))) {
                fragmentClass = FavoritesFragment.class;
            } else if (tag.equals(res.getString(R.string.my_stores))) {
                fragmentClass = MyStoresFragment.class;
            } else if (tag.equals(res.getString(R.string.manage_store))) {
                fragmentClass = ManageStoresFragment.class;
            }

            if (fragmentClass != null) {
                try {
                    Fragment fragment = (Fragment) fragmentClass.newInstance();
                    fragmentTransaction = fragmentManager.beginTransaction();
                    fragmentTransaction.setCustomAnimations(R.anim.slide_in_right, R.anim.slide_out_right);
                    fragmentTransaction.replace(R.id.flContent, fragment);
                    fragmentTransaction.commit();
                } catch (Exception e) {
                    e.printStackTrace();
                }

            }

            if (tag.equals("category_fragment_2")) {
                this.setTitle(getResources().getString(R.string.category));
            } else {
                this.setTitle(tag);
            }

            setNewMenuItemSelection(tag);

        } else { //THERE IS A PLACE TO HANDLE START_PAGE RETURN
            super.onBackPressed();
        }
    }

    public void removeItemFromBackStack(String tag) {
        this.customBackStack.remove(tag);

    }

    public void addItemToBackStack(String tag) {
        this.customBackStack.add(tag);
    }


    private void setNewMenuItemSelection(String name) {
        int id;
        Resources res = getResources();
        Log.d(LOG_TAG, name);

        if (name.equals(res.getString(R.string.home)) || name.equals(res.getString(R.string.app_name))) {
            id = R.id.nav_menu_home;
            mPosition = 0;
        } else if (name.equals(res.getString(R.string.category)) || name.equals("category_fragment_2")) {
            id = R.id.nav_menu_category;
            mPosition = 1;
        } else if (name.equals(res.getString(R.string.favorites))) {
            id = R.id.nav_menu_favorites;
            mPosition = 2;
        } else if (name.equals(res.getString(R.string.my_stores))) {
            id = R.id.nav_menu_mystores;
            mPosition = 3;
        } else if (name.equals(res.getString(R.string.manage_store))) {
            id = R.id.nav_menu_manage;
            mPosition = 4;
        } else {
            return;
        }


        nvDrawer.getMenu().findItem(id).setChecked(true);
    }
}
