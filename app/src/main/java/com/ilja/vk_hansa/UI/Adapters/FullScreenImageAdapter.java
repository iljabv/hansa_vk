package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.ilja.vk_hansa.Model.Vk.VkPhotoItem;
import com.ilja.vk_hansa.R;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ilja on 21.03.2016.
 */
public class FullScreenImageAdapter extends PagerAdapter {

    private Context mContext;
    private List<VkPhotoItem> photoList;
    private LayoutInflater inflater;

    // constructor
    public FullScreenImageAdapter(Context activity,
                                  List<VkPhotoItem> imagePaths) {
        this.mContext = activity;
        this.photoList = imagePaths;
    }

    @Override
    public int getCount() {
        return this.photoList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == ((RelativeLayout) object);
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        ImageView imgDisplay;
        final ProgressBar mProgressView;

        inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View viewLayout = inflater.inflate(R.layout.layout_fullscreen_image, container, false);

        imgDisplay = (ImageView) viewLayout.findViewById(R.id.imgDisplay);
        mProgressView = (ProgressBar) viewLayout.findViewById(R.id.progressbar_fullscreen);
        mProgressView.setVisibility(View.VISIBLE);

        //TODO: make normal callback
        Picasso.with(mContext).load(photoList.get(position).getHighResPhoto()).into(imgDisplay, new com.squareup.picasso.Callback() {
            @Override
            public void onSuccess() {
                mProgressView.setVisibility(View.GONE);
            }

            @Override
            public void onError() {
                mProgressView.setVisibility(View.GONE);
            }

        });


        ((ViewPager) container).addView(viewLayout);

        return viewLayout;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        ((ViewPager) container).removeView((RelativeLayout) object);

    }

}