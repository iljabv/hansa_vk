package com.ilja.vk_hansa.UI.Fragments;

import android.app.Dialog;
import android.app.DialogFragment;
import android.content.res.Resources;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.ilja.vk_hansa.Model.Category;
import com.ilja.vk_hansa.Model.CategoryHelper;
import com.ilja.vk_hansa.R;

import java.util.List;

/**
 * Created by Egorov on 06.02.2016.
 */
public class SearchConfigurationDialogFragment extends DialogFragment implements View.OnClickListener {

    public interface SearchConfigurationListener {
        void onApplyChanges(Bundle params);
    }

    final String LOG_TAG = "SearchConfiguration";

    private CategoryHelper categoryHelper;

    private Button closeButton;
    private Button applyButton;
    private EditText minPrice;
    private EditText maxPrice;
    private Spinner sectionSelector;
    private Spinner categorySelector;
    private RadioGroup sortSelector;

    private Category[] sectionItems;
    private Category[] categoryItems;

    private Bundle bundle;
    public int selectedCategory;
    public int selectedSection;
    private boolean startBoolean = false;
    private LinearLayout categoryLayout;
    private View separator;

    ArrayAdapter<Category> sectionArrayAdapter;
    ArrayAdapter<Category> categoryArrayAdapter;

    private int CHOOSE_CATEGORY = -1001;

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle("Параметры поиска"); //onCreate bundle handler with current params is needed

        View v = inflater.inflate(R.layout.fragment_search_configuration, null);

        categoryHelper = new CategoryHelper();

        closeButton = (Button) v.findViewById(R.id.button_close);
        applyButton = (Button) v.findViewById(R.id.button_apply_parameters_changes);
        minPrice = (EditText) v.findViewById(R.id.min_price_parameter);
        maxPrice = (EditText) v.findViewById(R.id.max_price_parameter);
        sectionSelector = (Spinner) v.findViewById(R.id.spinner_section_select);
        categorySelector = (Spinner) v.findViewById(R.id.spinner_category_select);
        sortSelector = (RadioGroup) v.findViewById(R.id.radio_group_sort);

        categoryLayout = (LinearLayout) v.findViewById(R.id.layout_category_container);
        separator = v.findViewById(R.id.separator_bottom_search_configuration);
        bundle = getArguments();

        applyButton.setOnClickListener(this);
        closeButton.setOnClickListener(this);

        h.sendEmptyMessage(0);
        h.sendEmptyMessage(1);


        sectionSelector.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                List<Category> tempList;
                switch (sectionItems[position].getId()) { //notifyDataSetChanged DOESN'T WORK!!
                    case (0):
                        tempList = categoryHelper.getClothesSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-1):
                        tempList = categoryHelper.getChildSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-2):
                        tempList = categoryHelper.getElectronicSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-3):
                        tempList = categoryHelper.getComputerSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-4):
                        tempList = categoryHelper.getTransportSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-5):
                        tempList = categoryHelper.getRealtySubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-6):
                        tempList = categoryHelper.getHomeSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-7):
                        tempList = categoryHelper.getBeautySubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-8):
                        tempList = categoryHelper.getSportSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-9):
                        tempList = categoryHelper.getGiftSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-10):
                        tempList = categoryHelper.getPetsSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-11):
                        tempList = categoryHelper.getFoodSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-12):
                        tempList = categoryHelper.getServiceSubCategories(getResources());
                        tempList.add(0, new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY));
                        categoryEnable(tempList.toArray(new Category[tempList.size()]));
                        break;

                    case (-1001): //case: ВЫБЕРИТЕ КАТЕГОРИЮ
                        categoryDisable();
                        break;
                }
                if (startBoolean) {
                    h.sendEmptyMessage(2);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return v;
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {
            case R.id.button_apply_parameters_changes: {
                SearchConfigurationListener activity = (SearchConfigurationListener) getActivity();

                if (categorySelector.isEnabled()){
                    if (categorySelector.getSelectedItemPosition() == 0){
                        Toast.makeText(getActivity(), "Пожалуйста, выберите подкатегорию", Toast.LENGTH_SHORT).show();
                        return;
                    }
                }

                int min, max, categoryID, sortID = 0, rev = 0;

                try {
                    min = Integer.parseInt(minPrice.getText().toString());
                } catch (NumberFormatException e) { //Not checked case
                    min = -1;
                }

                try {
                    max = Integer.parseInt(maxPrice.getText().toString());
                } catch (NumberFormatException e) { //Not checked case
                    max = -1;
                }

                if (categorySelector.isEnabled()) {
                    Log.d(LOG_TAG, "ENABLED: " + Integer.toString(((Category) categorySelector.getSelectedItem()).getId()));
                    categoryID = ((Category) categorySelector.getSelectedItem()).getId();
                } else {
                    Log.d(LOG_TAG, "DISABLED");
                    categoryID = -1;
                }

                if (sortSelector.getCheckedRadioButtonId() != -1) {
                    int sort = sortSelector.getCheckedRadioButtonId();

                    switch (sort) {
                        case (R.id.radio_cheap):
                            sortID = 2;
                            break;
                        case (R.id.radio_expensive):
                            sortID = 2;
                            rev = 1;
                            break;
                        case (R.id.radio_newest):
                            sortID = 1;
                            break;
                        case (R.id.radio_popular):
                            sortID = 3;
                            break;
                    }
                }

                Bundle bundle = new Bundle(); //Bundle with parameters
                bundle.putInt("min", min);
                bundle.putInt("max", max);
                bundle.putInt("categoryID", categoryID);
                bundle.putInt("sortType", sortID);
                bundle.putInt("rev", rev);

                activity.onApplyChanges(bundle);

                this.dismiss();

                break;
            }

            case R.id.button_close:
                this.dismiss();
                break;
        }

    }


    public void categoryEnable(Category[] container) {
        categorySelector.setEnabled(true); //ENABLE SUBCATEGORIES SPINNER
        categorySelector.setClickable(true);
        categoryItems = container;
        categoryArrayAdapter = new ArrayAdapter<Category>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categoryItems);
        categorySelector.setAdapter(categoryArrayAdapter);
    }

    public void categoryDisable() {
        categoryItems = new Category[]{new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY)};
        categoryArrayAdapter = new ArrayAdapter<Category>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categoryItems);
        categorySelector.setAdapter(categoryArrayAdapter);
        categorySelector.setEnabled(false); //DISABLE SUBCATEGORIES SPINNER
        categorySelector.setClickable(false);
    }

    private Category[] getMainCategories() {
        Resources res = getResources();
        Category[] mainCategories = new Category[]{
                new Category("Выберите категорию", "no_photo", CHOOSE_CATEGORY),
                new Category("Гардероб", "no_photo", res.getInteger(R.integer.section_clothes_id)),
                new Category("Детские товары", "no_photo", res.getInteger(R.integer.section_child_id)),
                new Category("Электроника", "no_photo", res.getInteger(R.integer.section_electronics_id)),
                new Category("Компьютеры и ПО", "no_photo", res.getInteger(R.integer.section_computers_id)),
                new Category("Транспорт", "no_photo", res.getInteger(R.integer.section_transport_id)),
                new Category("Недвижимость", "no_photo", res.getInteger(R.integer.section_realty_id)),
                new Category("Дом и дача", "no_photo", res.getInteger(R.integer.section_home_id)),
                new Category("Красота и здоровье", "no_photo", res.getInteger(R.integer.section_beauty_id)),
                new Category("Спорт и отдых", "no_photo", res.getInteger(R.integer.section_sport_id)),
                new Category("Досуг и подарки", "no_photo", res.getInteger(R.integer.section_gift_id)),
                new Category("Домашние питомцы", "no_photo", res.getInteger(R.integer.section_pets_id)),
                new Category("Продукты питания", "no_photo", res.getInteger(R.integer.section_food_id)),
                new Category("Услуги", "no_photo", res.getInteger(R.integer.section_service_id))};

        return mainCategories;
    }

    @Override
    public void onStart() {
        super.onStart();

        Dialog dialog = getDialog();

        Point screen_size = new Point();
        getActivity().getWindowManager().getDefaultDisplay().getSize(screen_size);
        int dialog_width = (int) (screen_size.x * 0.9); //90 percent of screen width, select better if needed

        if (dialog != null) {
            dialog.getWindow().setLayout(dialog_width, ViewGroup.LayoutParams.WRAP_CONTENT);
        }
    }

    private Handler h = new Handler(new Handler.Callback() {
        @Override
        public boolean handleMessage(Message msg) {
            switch (msg.what) {
                case 0:
                    sectionItems = getMainCategories();
                    categoryItems = new Category[]{new Category("Выберите подкатегорию", "no_photo", CHOOSE_CATEGORY)};

                    sectionArrayAdapter = new ArrayAdapter<Category>(getActivity(), android.R.layout.simple_spinner_dropdown_item, sectionItems);
                    categoryArrayAdapter = new ArrayAdapter<Category>(getActivity(), android.R.layout.simple_spinner_dropdown_item, categoryItems);

                    sectionSelector.setAdapter(sectionArrayAdapter);
                    categorySelector.setAdapter(categoryArrayAdapter);
                    startBoolean = true;
                    break;

                case 1:

                    boolean isCategoryNeeded = true;
                    if (bundle != null) {
                        isCategoryNeeded = bundle.getBoolean("category", true);

                        if (!isCategoryNeeded) {

                            categoryLayout.setVisibility(View.GONE);
                            separator.setVisibility(View.GONE);
                        } else {
                            int currentSection = bundle.getInt("section", 0);

                            if (currentSection != -1) { //doesn't work, need to select active category
                                //categorySelector.setSelection((currentSection % 100));
                                sectionSelector.setSelection((currentSection / 100) + 1);
                                //  Log.d(LOG_TAG, Integer.toString(currentSection));
                                sectionSelector.getOnItemSelectedListener();
                            }
                        }
                    }
                    break;

                case 2:
                    if (bundle != null) {
                        int section = bundle.getInt("section", 0);

                        if (section < 12 && section >= 0) {
                            categorySelector.setSelection(section);
                        }
                        else if(section == -1) {
                            Log.d(LOG_TAG, "Nothing selected");
                        }
                        else {
                            categorySelector.setSelection((section % 100) + 1);
                        }
                        //    Log.d(LOG_TAG, "Section: " + Integer.toString(section));
                    }
                    startBoolean = false;
                    break;
            }
            return false;
        }

        ;
    });
}
