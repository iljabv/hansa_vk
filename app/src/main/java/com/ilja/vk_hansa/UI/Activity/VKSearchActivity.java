package com.ilja.vk_hansa.UI.Activity;

import android.app.DialogFragment;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.VKItemAdapter;
import com.ilja.vk_hansa.UI.Fragments.SearchConfigurationDialogFragment;
import com.ilja.vk_hansa.UI.UIHelps.EndlessRecyclerOnScrollListener;
import com.ilja.vk_hansa.UI.UIHelps.SpacesItemDecoration;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class VKSearchActivity extends AppCompatActivity implements SearchConfigurationDialogFragment.SearchConfigurationListener {


    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY = 6;

    private final String LOG_TAG = "VKSearchActivity";

    private List<VkItem> mVkItemList;

    //views
    private TextView mErrorText;
    private Button mErrorButton;
    private ImageView mImageError;
    private FrameLayout mErrorView;
    private MenuItem mMenuItem;
    private SearchView mSearchView;
    private ImageButton changeColsButton;
    private ImageButton changeQueryParametersButton;
    private ProgressView mProgressView;
    private RecyclerView mRecyclerView; //RecyclerView item
    private DialogFragment configurationDialog;
    private TextView textItemsCount;
    //search params
    private String mQueryStr;
    private int mMinPrice;
    private int mMaxPrice;
    private int mCategoryID;
    private int mSortType;
    private int maxItemCount;
    private int mRev;
    private int mOffset;
    private int mCount;
    private int mRealCountTest; //for testing

    private boolean mSearchType; //0 - by group, 1 - by album
    private int mGroupID;
    private int mAlbumID;


    //UI help
    private VkAPIFactory mVkAPIresolver;
    private VKItemAdapter itemAdapter; //Adapter for RecyclerView
    private GridLayoutManager mGridLayoutManager; //RVManager, needed to dynamically change columns number
    private EndlessRecyclerOnScrollListener mEndlessScrollListener;
    private StringBuilder mStringBuilder;

    //web


    private VKRequest mVkRequest;
    private int mScrollOffset;
    private boolean isDataCached = false;


    private static class VkItemsRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<VKSearchActivity> mWeakRefParent;
        private VkAPIFactory mVkApiFactory;
        private boolean mReset;

        private VkItemsRequestListner(VKSearchActivity activity, boolean reset) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mVkApiFactory = new VkAPIFactory();
            mReset = reset;
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            int maxItemCount = 0;

            //   Log.d(TAG, "onComplete() called with: " + "response = [" + response + "]");
            VKSearchActivity activity = mWeakRefParent.get();
            if (activity != null) {
                activity.onItemPageLoaded(mVkApiFactory.getItemList(response), mReset, mVkApiFactory.getItemCount(response));
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            //   Log.d(TAG, "Attempt failed: " + attemptNumber + " " + totalAttempts);

            VKSearchActivity activity = mWeakRefParent.get();
            if (activity != null) {
                activity.onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            // Log.d(TAG, "Error " + error.errorMessage);

            VKSearchActivity activity = mWeakRefParent.get();
            if (activity != null) {
                activity.onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onLoadError(boolean type) {
        setVisibility(ERROR_VISIBILITY);
    }

    private void onItemPageLoaded(List<VkItem> itemList, boolean reset, int max_items) {

       if(itemList!=null && itemList.size()>0){

        if (reset) {
            resetRecyclerView();
            this.maxItemCount = max_items;
            mStringBuilder.setLength(0);
            textItemsCount.setText(mStringBuilder
                    .append(getApplicationContext().getResources().getQuantityString(R.plurals.found, maxItemCount))
                    .append(" ")
                    .append(maxItemCount)
                    .append(" ")
                    .append(getApplicationContext().getResources().getQuantityString(R.plurals.items, maxItemCount)));

            mRealCountTest = 0;
        }

        mVkItemList.addAll(itemList);
        mRealCountTest += itemList.size();

        updateRecyclerView();

        setVisibility(SUCCESS_VISIBILITY);
       }
        else
       {
          setVisibility(NOT_FOUND_VISIBILITY);
       }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vksearch);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.search));
        mVkAPIresolver = new VkAPIFactory();
        mStringBuilder = new StringBuilder();
        mRealCountTest = 0;
        mCount = 20;
        mOffset = 0;
        maxItemCount = 0;
        mSortType = 0;      // params for query; null or -1 default if parameter not used in query
        mMinPrice = -1; //-1
        mMaxPrice = -1;
        mCategoryID = -1;
        mRev = 0; //default
        mSortType = 0; //default

        textItemsCount = (TextView) findViewById(R.id.text_test_elements_count);
        mErrorView = (FrameLayout) findViewById(R.id.error_view);
        mErrorButton = (Button) findViewById(R.id.button_error);
        mErrorText = (TextView) findViewById(R.id.text_error);
        mImageError = (ImageView) findViewById(R.id.image_error);
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view); //Getting RView
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, this));
        mGridLayoutManager = new GridLayoutManager(this, 2); //Creating LayoutManager
        mRecyclerView.setLayoutManager(mGridLayoutManager); //Setting params
        mRecyclerView.setHasFixedSize(true); //Setting params
        mVkItemList = new ArrayList<>();
        itemAdapter = new VKItemAdapter(this, mVkItemList, true, true);
        mRecyclerView.setAdapter(itemAdapter);

        mEndlessScrollListener = new EndlessRecyclerOnScrollListener(mGridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {

                getVkItems(false, current_page);

            }
        };

        mRecyclerView.addOnScrollListener(mEndlessScrollListener);


        mProgressView = (ProgressView) findViewById(R.id.progressBar_VK_loadmore);

        View.OnClickListener colsChangeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.removeOnScrollListener(mEndlessScrollListener); //preventing additional request

                int currentColumnsCount = mGridLayoutManager.getSpanCount();

                changeColsButton.setVisibility(View.VISIBLE);
                changeColsButton.setAlpha(0.0f);
                mRecyclerView.setAlpha(0.0f);

                if (currentColumnsCount == 2)
                    changeColsButton.setImageResource(R.drawable.ic_view_module_orange);
                else changeColsButton.setImageResource(R.drawable.ic_view_list_orange);

                changeColsButton.animate().alpha(1.0f);
                mRecyclerView.animate().alpha(1.0f).setDuration(500);

                mGridLayoutManager = new GridLayoutManager(VKSearchActivity.this, currentColumnsCount == 2 ? 1 : 2);
                mRecyclerView.setLayoutManager(mGridLayoutManager);

                mEndlessScrollListener = new EndlessRecyclerOnScrollListener(mGridLayoutManager) {
                    @Override
                    public void onLoadMore(int current_page) {

                        getVkItems(false, current_page);
                    }
                };
                mRecyclerView.addOnScrollListener(mEndlessScrollListener); //adding new endlessScroll

                itemAdapter = new VKItemAdapter(VKSearchActivity.this, mVkItemList, currentColumnsCount == 2 ? false : true, true);
                mRecyclerView.setAdapter(itemAdapter);

            }
        };

        changeColsButton = (ImageButton) findViewById(R.id.button_change_columns);
        changeColsButton.setOnClickListener(colsChangeListener);

        configurationDialog = new SearchConfigurationDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean("category", false);
        configurationDialog.setArguments(bundle);

        changeQueryParametersButton = (ImageButton) findViewById(R.id.button_change_query_parameters);
        changeQueryParametersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RotateAnimation ra = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                //ra.setFillAfter(true);
                ra.setDuration(300);
                changeQueryParametersButton.startAnimation(ra);

                configurationDialog.show(getFragmentManager(), "configuration_dialog");
            }
        });


        mSearchType = getIntent().getExtras().getBoolean("search_type", false);
        if (!mSearchType) {
            mGroupID = getIntent().getExtras().getInt("group_id");
            mAlbumID = 0;
        } else {
            mGroupID = getIntent().getExtras().getInt("group_id");
            mAlbumID = getIntent().getExtras().getInt("album_id");
        }

        setVisibility(START_VISIBILITY);

        getVkItems(true, 1);

    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVkRequest != null) mVkRequest.cancel();

    }


    @Override
    public void onPause() {
        super.onPause();

        setVisibility(ALL_INVISIBLE);
        mScrollOffset = mGridLayoutManager.findFirstVisibleItemPosition();
        itemAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mVkItemList != null) {

            setVisibility(SUCCESS_VISIBILITY);
            itemAdapter = new VKItemAdapter(this, mVkItemList, (mGridLayoutManager.getSpanCount() == 2) ? true : false, true);
            mRecyclerView.setAdapter(itemAdapter);

            //   mEndlessScrollListener.Reload();
            mRecyclerView.scrollToPosition(mScrollOffset);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home_store:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        //  Log.d(LOG_TAG,"Inflate Search view");

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mMenuItem = menu.findItem(R.id.action_search);
        mSearchView = (SearchView) mMenuItem.getActionView();

        ComponentName componentName = getComponentName();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(componentName);
        mSearchView.setSearchableInfo(searchableInfo);
        //  mSearchView.setMaxWidth(20000);//it is dirty hack for expand in landscape
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //    Log.d(LOG_TAG, "Start search: " + query);
                //
                mQueryStr = query;
                getVkItems(true, 1);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) { //If query is empty - getting FULL item list, maybe need to store initial query in special container to load faster this case
                //Log.d(LOG_TAG, newText);
                if (newText.isEmpty() && mQueryStr != null) {
                    if (!mQueryStr.isEmpty()) {
                        mQueryStr = newText;

                        getVkItems(true, 1);
                    }
                }
                return false;
            }
        });


        return super.onCreateOptionsMenu(menu);


    }

    public void getVkItems(final boolean newQuery, int page_to_load) {

        mOffset = (page_to_load - 1) * mCount;

        if (mOffset > maxItemCount && !newQuery) return;

        if(newQuery) setVisibility(PROGRESS_VISIBILITY);
        else setVisibility(PART_PROGRESS_VISIBILITY);

        if (!mSearchType) { //Only by group search
            if (mQueryStr != null) {
                //   Log.d(LOG_TAG, mQueryStr);
            }
            mVkRequest = new VKRequest("market.search", VKParameters.from(VKApiConst.OWNER_ID, -mGroupID, VKApiConst.Q, mQueryStr, VKApiConst.EXTENDED, "1", VKApiConst.OFFSET, mOffset,
                    VKApiConst.COUNT, mCount, VKApiConst.SORT, mSortType, VKApiConst.REV, mRev));
            if (mMinPrice != -1)
                mVkRequest.addExtraParameter("price_from", Integer.toString(mMinPrice) + "00");
            if (mMaxPrice != -1)
                mVkRequest.addExtraParameter("price_to", Integer.toString(mMaxPrice) + "00");
        } else { //By Album search
            mVkRequest = new VKRequest("market.search", VKParameters.from(VKApiConst.OWNER_ID, -mGroupID, VKApiConst.ALBUM_ID, mAlbumID, VKApiConst.Q, mQueryStr, VKApiConst.EXTENDED, "1", VKApiConst.OFFSET, mOffset,
                    VKApiConst.COUNT, mCount, VKApiConst.SORT, mSortType, VKApiConst.REV, mRev));
            if (mMinPrice != -1)
                mVkRequest.addExtraParameter("price_from", Integer.toString(mMinPrice) + "00");
            if (mMaxPrice != -1)
                mVkRequest.addExtraParameter("price_to", Integer.toString(mMaxPrice) + "00");
        }

        mVkRequest.executeWithListener(new VkItemsRequestListner(this, newQuery));
    }

    private void updateRecyclerView() {
        if (itemAdapter != null) {
            itemAdapter.notifyDataSetChanged(); //Refresh our RecyclerView (Works faster(?) then item-by-item)
        }
    }

    private void resetRecyclerView() {
        int currentColumnsCount = mGridLayoutManager.getSpanCount();
        mRecyclerView.setAdapter(null);

        mVkItemList.clear();
        itemAdapter = new VKItemAdapter(this, mVkItemList, (currentColumnsCount == 2) ? true : false, true);
        mRecyclerView.setAdapter(itemAdapter);

        mEndlessScrollListener.Reload();
    }


    @Override
    public void onApplyChanges(Bundle bundle) {
        if (bundle != null) {
            this.mMinPrice = bundle.getInt("min", -1);
            this.mMaxPrice = bundle.getInt("max", -1);
            this.mCategoryID = bundle.getInt("categoryID", -1);
            this.mSortType = bundle.getInt("sortType", 0);

            this.mRev = bundle.getInt("rev", 0);

            RotateAnimation ra = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            ra.setDuration(300);
            changeQueryParametersButton.startAnimation(ra);

            getVkItems(true, 1); //Search on changes apply
        }
    }

    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));
                mErrorButton.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getVkItems(true, 1);
                    }
                });

            }
            break;

            case NOT_FOUND_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.nothing_found));

                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);
                mErrorButton.setVisibility(View.GONE);
                mErrorButton.setOnClickListener(null);

            }
            break;

            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case PART_PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
        }
    }



}
