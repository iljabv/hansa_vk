package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.ItemDetalizationActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ilja on 13.02.2016.
 */
public class LastItemsAdapter extends RecyclerView.Adapter<LastItemsAdapter.PersonViewHolder> {

    public static class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {


        private TextView priceText;
        private ImageView mainPhotoImage;
        private Context mContext;
        private int ownerId;
        private int itemId;


        PersonViewHolder(View itemView, Context context) {

            super(itemView);

            this.priceText = (TextView) itemView.findViewById(R.id.text_store_lastitems_price);
            this.mainPhotoImage = (ImageView) itemView.findViewById(R.id.image_store_lastitems_photo);
            this.mContext = context;
            itemView.setOnClickListener(this);

        }


        public void onClick(View v) {

            Intent intent = new Intent(mContext, ItemDetalizationActivity.class);
            intent.putExtra(HansaApplication.ITEM_ID, itemId);
            intent.putExtra(HansaApplication.OWNER_ID, ownerId);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }


    }

    private Picasso mImageLoader;
    private List<VkItem> mArrayItems;
    private VkItem mVkItem;
    private Context mContext;

    private final String LOG_TAG = "LastItemsAdapter";

    public LastItemsAdapter(List<VkItem> array_items, Context context) {
        this.mArrayItems = array_items;
        this.mContext = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {


        View v;
        PersonViewHolder pvh;
        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_store_lastitems_element, viewGroup, false);
        pvh = new PersonViewHolder(v, mContext);
        return pvh;
    }


    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {


        mVkItem = mArrayItems.get(i);

        // personViewHolder.
        personViewHolder.itemId = mVkItem.getId();
        personViewHolder.ownerId = mVkItem.getOwnerId();
        personViewHolder.priceText.setText(mVkItem.getPrice().getText());

        if (mVkItem.getPhotos().get(0) != null) {  //TODO: Set default image if getPhotos() == null
            mImageLoader.with(mContext).load(mVkItem.getPhotos().get(0).getBestExistingPhoto()).into(personViewHolder.mainPhotoImage);
        } else {
            mImageLoader.with(mContext).load(R.drawable.vk_dog).into(personViewHolder.mainPhotoImage);
        }

    }


    @Override
    public int getItemCount() {
        return mArrayItems.size();
    }
}