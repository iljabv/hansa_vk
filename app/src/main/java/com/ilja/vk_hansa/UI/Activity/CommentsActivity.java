package com.ilja.vk_hansa.UI.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.Comments.AdaptiveVkComments;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.VkCommentsAdapter;
import com.ilja.vk_hansa.UI.UIHelps.EndlessRecyclerOnScrollListener;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class CommentsActivity extends AppCompatActivity {

    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY=6;
    private FrameLayout mErrorView;
    private RecyclerView mRecyclerView;
    private ProgressView mProgressView;
    private StringBuilder mStringBuilder;
    private int mOwnerId;
    private int mItemId;
    private int mCount = 20;
    private int mOffset = 0;
    private int maxCommentsCount;
    private TextView mErrorText;
    private Button mErrorButton;
    private VKRequest mVkRequest;
    private List<AdaptiveVkComments> mCommentList;
    private EndlessRecyclerOnScrollListener mEndlessScrollListener;
    private GridLayoutManager mGridLayoutManager;
    private VkCommentsAdapter mCommentsAdapter;
    private int mScrollOffset;
    private ImageView mImageError;


    private static class VkRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkLikeRequestListner";
        private final WeakReference<CommentsActivity> mWeakRefParent;
        private boolean mReset;  //used in like response
        private int mResponseType;  // 0 - like, 1 - item, 2 - wiki, 3 - comments
        private VkAPIFactory mVkApiFactory;

        private VkRequestListner(CommentsActivity activity, boolean reset) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mReset = reset;
            this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            //     Log.d(TAG, response.json.toString());
            if (mWeakRefParent.get() != null) {


                mWeakRefParent.get().onCommentsLoaded(mVkApiFactory.getAdaptiveComments(response), mVkApiFactory.getItemCount(response), mReset);


            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);

            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onCommentsLoaded(List<AdaptiveVkComments> adaptiveComments, int itemCount, boolean mReset) {

       if(adaptiveComments!=null && adaptiveComments.size()>0) {

           if (mReset) {
               maxCommentsCount = itemCount;
               resetRecyclerView();
           }

           setVisibility(SUCCESS_VISIBILITY);
           mCommentList.addAll(adaptiveComments);
           updateRecyclerView();

       }
    }

    private void onLoadError(boolean b) {
        setVisibility(ERROR_VISIBILITY);
    }


    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));

                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getComments(true, 1);
                    }
                });

            }
            break;
            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case PART_PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_comments);

        mStringBuilder = new StringBuilder();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        getSupportActionBar().setTitle(getApplicationContext().getString(R.string.comments));

        mCommentList = new ArrayList<>();

        mErrorView = (FrameLayout) findViewById(R.id.error_view);
        mErrorButton = (Button) findViewById(R.id.button_error);
        mErrorText = (TextView) findViewById(R.id.text_error);
        mImageError = (ImageView) findViewById(R.id.image_error);

        mProgressView = (ProgressView) findViewById(R.id.progressbar_comments);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_comments);

        mProgressView.setVisibility(View.VISIBLE);
        mErrorView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.GONE);

        mGridLayoutManager = new GridLayoutManager(this, 1); //Creating LayoutManager
        mRecyclerView.setLayoutManager(mGridLayoutManager); //Setting params
        mRecyclerView.setHasFixedSize(true); //Setting params


        mEndlessScrollListener = new EndlessRecyclerOnScrollListener(mGridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {

                getComments(false, current_page);

            }
        };

        mRecyclerView.addOnScrollListener(mEndlessScrollListener);


        mOwnerId = getIntent().getIntExtra(HansaApplication.OWNER_ID, 0);
        mItemId = getIntent().getIntExtra(HansaApplication.ITEM_ID, 0);

        setVisibility(START_VISIBILITY);
        getComments(true, 1);
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        if (mVkRequest != null) mVkRequest.cancel();
    }


    @Override
    public void onPause() {
        super.onPause();
        setVisibility(ALL_INVISIBLE);
        mScrollOffset = mGridLayoutManager.findFirstVisibleItemPosition();
        mCommentsAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCommentList != null) {
            mCommentsAdapter = new VkCommentsAdapter(this, mCommentList);
            mRecyclerView.setAdapter(mCommentsAdapter);

            mEndlessScrollListener.Reload();
            mRecyclerView.scrollToPosition(mScrollOffset);
            setVisibility(SUCCESS_VISIBILITY);
        }

    }


    private void getComments(boolean reset, int page) {


        mOffset = (page - 1) * mCount;

        if (mOffset > maxCommentsCount && !reset) {
            return;
        }

       if(reset) setVisibility(PROGRESS_VISIBILITY);
       else setVisibility(PART_PROGRESS_VISIBILITY);

        mVkRequest = new VKRequest("market.getComments", VKParameters.from("owner_id", mOwnerId, "item_id", mItemId, "need_likes", "1", VKApiConst.SORT, "desc", VKApiConst.COUNT, mCount, VKApiConst.OFFSET, mOffset, VKApiConst.EXTENDED, "1", VKApiConst.FIELDS, "photo_100"));


        mVkRequest.executeWithListener(new VkRequestListner(this, reset));

    }


    private void updateRecyclerView() {
        if (mCommentsAdapter != null) {
            mCommentsAdapter.notifyDataSetChanged(); //Refresh our RecyclerView (Works faster(?) then item-by-item)
        }
    }

    private void resetRecyclerView() {

        mStringBuilder.setLength(0);
        getSupportActionBar().setTitle(mStringBuilder.append(maxCommentsCount).append(" ").append(getApplicationContext().getResources().getQuantityString(R.plurals.comments, maxCommentsCount)));

        mRecyclerView.setAdapter(null);

        mCommentList.clear();
        mCommentsAdapter = new VkCommentsAdapter(this, mCommentList);

        mRecyclerView.setAdapter(mCommentsAdapter);

        mEndlessScrollListener.Reload();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_detalization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home_store:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                return true;
            case R.id.action_refresh_store:

                getComments(true, 1);
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


}
