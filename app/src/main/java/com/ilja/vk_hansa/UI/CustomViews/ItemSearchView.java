package com.ilja.vk_hansa.UI.CustomViews;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.ItemDetalizationActivity;
import com.squareup.picasso.Picasso;

/**
 * Created by Ilja on 02.03.2016.
 */
public class ItemSearchView extends CardView implements View.OnClickListener {

    private final String LOG_TAG = "ItemSearchView";
    private boolean mType; //true columns, false list
    private boolean mIsExtendVisible;
    private int mItemId;
    private int mOwnerId;
    private StringBuilder mStringBuilder;

    private TextView mTextName;
    private TextView mTextPrice;
    private ImageView mImagePhoto;
    private TextView mTextCategory;
    private TextView mTextLikesCount;
    private TextView mTextViewsCount;
    private ImageView mImageLikesIcon;
    private ImageView mImageViewsIcon;
    private RelativeLayout mExtendRelativeView;
    private Picasso mImageLoader;


    public ItemSearchView(Context context, AttributeSet attrs) {

        super(context, attrs);


    }


    public ItemSearchView(Context mContext, boolean mLayoutType, boolean isLikeVisivle) {
        super(mContext);

        mType = mLayoutType;
        mIsExtendVisible = isLikeVisivle;
        init();
    }

    private void init() {
        if (mType) inflate(getContext(), R.layout.item_searchresponse, this);
        else {
            inflate(getContext(), R.layout.item_searchresponse_listed, this);
            mTextName = (TextView) findViewById(R.id.text_item_name);
        }


        mTextPrice = (TextView) findViewById(R.id.text_item_price);
        mImagePhoto = (ImageView) findViewById(R.id.image_item_photo);
        mTextCategory = (TextView) findViewById(R.id.text_item_category);
        mTextLikesCount = (TextView) findViewById(R.id.text_item_likes);
        mTextViewsCount = (TextView) findViewById(R.id.text_item_views);
        mImageLikesIcon = (ImageView) findViewById(R.id.image_item_likes);
        mImageViewsIcon = (ImageView) findViewById(R.id.image_item_views);
        mExtendRelativeView = (RelativeLayout) findViewById(R.id.relativelayout_item_extend);

        if (!mIsExtendVisible) mExtendRelativeView.setVisibility(View.GONE);

        this.setOnClickListener(this);

        mStringBuilder = new StringBuilder();

        this.setUseCompatPadding(true);
    }


    public void setItem(VkItem item) {
        mItemId = item.getId();
        mOwnerId = item.getOwnerId();
        if (!mType) mTextName.setText(item.getTitle());

        if (mIsExtendVisible) {
            mStringBuilder.setLength(0);
            mTextLikesCount.setText(mStringBuilder.append(item.getLikes().getCount()));

            mStringBuilder.setLength(0);
            mTextViewsCount.setText(mStringBuilder.append(item.getViewsCount()));

            mImageLoader.with(getContext()).load(R.drawable.ic_visibility_orange).into(mImageViewsIcon);
            mImageLoader.with(getContext()).load(R.drawable.ic_heart_orange).into(mImageLikesIcon);
        }

        mTextPrice.setText(item.getPrice().getText());
        mTextCategory.setText(item.getCategory().getName());

        if (item.getPhotos() != null && item.getPhotos().size() > 0) {
            mImageLoader.with(getContext()).load(item.getPhotos().get(0).getBestExistingPhoto()).
                    error(R.drawable.vk_error_dog).into(mImagePhoto);
        } else { //if no photo
            mImageLoader.with(getContext()).load(R.drawable.vk_error_dog).into(mImagePhoto);
        }

    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getContext(), ItemDetalizationActivity.class);
        mStringBuilder.setLength(0);

        Log.d(LOG_TAG, String.valueOf(mItemId));

        intent.putExtra(HansaApplication.ITEM_ID, mItemId);
        intent.putExtra(HansaApplication.OWNER_ID, mOwnerId);

        getContext().startActivity(intent);
    }
}
