package com.ilja.vk_hansa.UI.UIHelps;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.util.DisplayMetrics;
import android.view.View;

/**
 * Created by Ilja on 12.02.2016.
 */
public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private int space;
    private int space_dp;
    private Context context;

    public int convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        int dp = (int) (px / ((float) metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT));
        return dp;
    }


    public SpacesItemDecoration() {


    }


    public SpacesItemDecoration(int space, Context context) {
        this.context = context;
        this.space = space;
        this.space_dp = convertPixelsToDp(space, context);

    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {


        outRect.left = space_dp;
        // outRect.right = space;
        outRect.bottom = space_dp;

        // Add top margin only for the first item to avoid double space between items
        //if(parent.getChildLayoutPosition(view) == 0)
        //    outRect.top = space;
    }
}