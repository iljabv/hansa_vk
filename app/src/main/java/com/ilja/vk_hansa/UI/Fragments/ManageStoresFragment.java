package com.ilja.vk_hansa.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.Model.Hansa.HansaCrawlerResponse;
import com.ilja.vk_hansa.Model.Hansa.HansaCrawlerResponseItems;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.ManagesStoresAdapter;
import com.ilja.vk_hansa.Web.IHansaAPI;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;


public class ManageStoresFragment extends Fragment {
    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY = 6;
    //const
    private final String APIURL = "http://vkcrawler-tinymagic.rhcloud.com/";
    private final String IN_BASE = "IN_BASE";
    private final String NOT_FOUND = "NOT_FOUND";
    private final String PROCESSING = "PROCESSING";
    private final String LOG_TAG = "ManageStoresFragment";

    //ui
    private RecyclerView mRecyclerView;
    private ProgressView mProgressView;

    private TextView mErrorText;
    private Button mErrorButton;
    private ImageView mImageError;
    private FrameLayout mErrorView;

    private List<VkStoreItem> currentStores;
    private ManagesStoresAdapter mManageStoreAdapter;
    private VkAPIFactory mVkAPIresolver;
    private StringBuilder mStringBuilder;

    //web
    private IHansaAPI mRestApi;
    private VKRequest userStoresRequest;
    private Call getStatusesRequest;
    private LinearLayoutManager mLinearLayoutManager;
    private int mScrollOffset;


    private static class HansaStatusCallback implements Callback<HansaCrawlerResponse> {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<ManageStoresFragment> mWeakRefParent;

        private boolean mReset;
        private VkAPIFactory mVkApiFactory;

        private HansaStatusCallback(ManageStoresFragment fragment) {
            this.mWeakRefParent = new WeakReference<>(fragment);
            this.mVkApiFactory = new VkAPIFactory();

        }

        @Override
        public void onResponse(Response<HansaCrawlerResponse> response) {


            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onHansaResponseLoaded(response.body().getResponse().getItems());
            }

        }

        @Override
        public void onFailure(Throwable t) {
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }

        }
    }

    private void onHansaResponseLoaded(List<HansaCrawlerResponseItems> list_res) {


        if (list_res != null) {
            for (int i = 0; i < list_res.size(); i++) {
                if (list_res.get(i).getId().equals(currentStores.get(i).getId().toString())) {

                    switch (list_res.get(i).getStatus()) {
                        case NOT_FOUND:
                            currentStores.get(i).setmStatusRegistration(0);
                            break;
                        case IN_BASE:
                            currentStores.get(i).setmStatusRegistration(2);
                            break;
                        case PROCESSING:
                            currentStores.get(i).setmStatusRegistration(1);
                            break;
                        default:
                            currentStores.get(i).setmStatusRegistration(-1);
                            break;
                    }
                }
            }

            resetRecyclerView();
            setVisibility(SUCCESS_VISIBILITY);
        }


    }


    //TODO: handle error UI
    private void onLoadError(boolean b) {
        setVisibility(ERROR_VISIBILITY);
    }


    private static class VkStoresRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<ManageStoresFragment> mWeakRefParent;
        private VkAPIFactory mVkApiFactory;

        private VkStoresRequestListner(ManageStoresFragment fragment) {
            this.mWeakRefParent = new WeakReference<>(fragment);
            this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onStoresLoaded(mVkApiFactory.getStoreAdminList(response));
            }
        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);

            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);

            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onStoresLoaded(List<VkStoreItem> storeAdminList) {
//        Log.d(LOG_TAG, "LOADED");
//

        if (storeAdminList != null && storeAdminList.size() > 0) {

            Log.d(LOG_TAG, "OK");
            currentStores = storeAdminList;

            for (int i = 0; i < storeAdminList.size(); i++) {
                storeAdminList.get(i).setmStatusRegistration(-1);
            }

            getGroupStatus();

            return;

        } else {
            setVisibility(NOT_FOUND_VISIBILITY);
        }
    }


    static ManageStoresFragment newInstance(int page) {

        ManageStoresFragment pageFragment = new ManageStoresFragment();
        Bundle arguments = new Bundle();
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onPause() {
        super.onPause();
        setVisibility(ALL_INVISIBLE);
        mScrollOffset = mLinearLayoutManager.findFirstVisibleItemPosition();
        mManageStoreAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();

        if (currentStores != null && currentStores.size()>0) {
            setVisibility(SUCCESS_VISIBILITY);
            mManageStoreAdapter = new ManagesStoresAdapter(currentStores, getContext());
            mRecyclerView.setAdapter(mManageStoreAdapter);

            mRecyclerView.scrollToPosition(mScrollOffset);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (getStatusesRequest != null && getStatusesRequest.isExecuted()) {
            getStatusesRequest.cancel();
        }
        if (userStoresRequest != null) userStoresRequest.cancel();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_my_stores, null);
        mStringBuilder = new StringBuilder();
        currentStores = new ArrayList<VkStoreItem>();
        mVkAPIresolver = new VkAPIFactory();

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_stores);
        mLinearLayoutManager = new LinearLayoutManager(getContext());
        mRecyclerView.setLayoutManager(mLinearLayoutManager);
        mRecyclerView.setHasFixedSize(true);

        mErrorView = (FrameLayout) view.findViewById(R.id.error_view);
        mErrorButton = (Button) view.findViewById(R.id.button_error);
        mErrorText = (TextView) view.findViewById(R.id.text_error);
        mImageError = (ImageView) view.findViewById(R.id.image_error);


        mProgressView = (ProgressView) view.findViewById(R.id.progressbar_mystorestore);
        Retrofit restAdapter = new Retrofit.Builder().baseUrl(APIURL).addConverterFactory(GsonConverterFactory.create()).build();

        mRestApi = restAdapter.create(IHansaAPI.class);
        setHasOptionsMenu(true);

        setVisibility(START_VISIBILITY);

        getGroups();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        inflater.inflate(R.menu.menu_mystores, menu);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_refresh:  // it is going to refer the search id name in main.xml

                getGroups();

                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    private void getGroups() {

        setVisibility(PROGRESS_VISIBILITY);

        userStoresRequest = new VKRequest("groups.get", VKParameters.from(VKApiConst.EXTENDED, "1", VKApiConst.FIELDS, "members_count,place,verified,site,market", VKApiConst.COUNT, "200")); //TODO pages if groups count  >200
        userStoresRequest.executeWithListener(new VkStoresRequestListner(this));
    }

    private void getGroupStatus() {

        mStringBuilder.setLength(0);
        for (int i = 0; i < currentStores.size(); i++) {
            mStringBuilder.append(currentStores.get(i).getId()).append(",");
        }

        mStringBuilder.setLength(mStringBuilder.length() - 1);
        // Log.d(LOG_TAG, mStringBuilder.toString());

        getStatusesRequest = mRestApi.getStatusesStores(mStringBuilder.toString());
        getStatusesRequest.enqueue(new HansaStatusCallback(this));
    }

    private void updateRecyclerView() {
        if (mManageStoreAdapter != null) {
            mManageStoreAdapter.notifyDataSetChanged();
        }
    }


    public void resetRecyclerView() {
        mManageStoreAdapter = new ManagesStoresAdapter(currentStores, getContext());
        mRecyclerView.setAdapter(mManageStoreAdapter);
    }


    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));
                mErrorButton.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getGroups();
                    }
                });

            }
            break;

            case NOT_FOUND_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.you_have_not_stores));

                Picasso.with(getContext()).load(R.drawable.vk_error_dog).into(mImageError);
                mErrorButton.setVisibility(View.GONE);
                mErrorButton.setOnClickListener(null);

            }
            break;

            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case PART_PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
        }
    }

}
