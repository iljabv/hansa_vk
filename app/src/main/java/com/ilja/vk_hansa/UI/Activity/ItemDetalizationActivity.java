package com.ilja.vk_hansa.UI.Activity;

import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.view.ViewPager;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilja.vk_hansa.DatabaseHelper;
import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.Comments.AdaptiveVkComments;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.ItemDetalizationViewPagerAdapter;
import com.ilja.vk_hansa.UI.CustomViews.CommentView;
import com.ilja.vk_hansa.UI.Fragments.WriteCommentDialogFragment;
import com.ilja.vk_hansa.UI.Fragments.WriteMessageDialogFragment;
import com.ilja.vk_hansa.UI.UIHelps.DetailOnPageChangeListener;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.ImageButton;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.List;

import me.relex.circleindicator.CircleIndicator;

public class ItemDetalizationActivity extends AppCompatActivity implements View.OnClickListener {
    private final String LOG_TAG = "ItemDetaliationActivity";

    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY = 6;

    //model
    private String mCurrentItemID;
    private VkItem mCurrentItem;
    private boolean mLike = false;
    private String mWikiUrl;

    //UI help
    private ViewPager mViewPager;
    private VkAPIFactory mVkApiFactory;
    private StringBuilder mStringBuilder;

    //UI
    private FrameLayout mErrorView;
    private TextView mErrorText;
    private com.rey.material.widget.Button mErrorButton;
    private ImageView mImageError;
    private ItemDetalizationViewPagerAdapter mPhotoAdapter;
    private FloatingActionButton mFloatingActionButton;
    private TextView textName;
    private TextView textDescription;
    private TextView textPrice;
    private TextView textCategory;
    private TextView textLikes;
    private TextView textViews;
    private CircleIndicator mIndicator;
    private Button buttonWrite;
    private ImageButton buttonStore;
    private ImageButton buttonExtraInfo;
    private Toolbar toolbar;
    private CollapsingToolbarLayout collapsingToolbarLayout;
    private ProgressView mProgressBar;
    private int mSellerId;
    private LinearLayout mCommentContainer;
    /* for DB */
    private DatabaseHelper sqlHelper;

    //web
    private VKRequest mVkRequest;
    private int mOwnerId;
    private int mItemId;
    private TextView textCommentsCount;
    private Button buttonMoreComments;
    public DetailOnPageChangeListener pageListener;
    private Button buttonComment;




    private NestedScrollView mScrollView;
    private ProgressView mProgressView;
    private boolean onStartLikeLoad = true;

    @Override
    public void onClick(View v) {
        switch (v.getId()) {


            case R.id.button_send_comment:
                if (mCurrentItem != null && mCurrentItem.getCanComment() == 1) {

                    DialogFragment messageDialog = new WriteCommentDialogFragment();

                    Bundle bundle = new Bundle();
                    bundle.putInt(HansaApplication.ITEM_ID, mItemId);
                    bundle.putInt(HansaApplication.OWNER_ID, mOwnerId);
                    messageDialog.setArguments(bundle);

                    messageDialog.show(getFragmentManager(), "write_message_dialog");

                    sqlHelper.setInterested(mCurrentItem);
                }
                break;

            case R.id.button_item_write:
                if (mCurrentItem != null) {

                    DialogFragment messageDialog = new WriteMessageDialogFragment();

                    Bundle bundle = new Bundle();
                    bundle.putString(HansaApplication.ITEM_ID, mCurrentItemID);
                    bundle.putInt(HansaApplication.SELLER_ID, mSellerId);
                    messageDialog.setArguments(bundle);

                    messageDialog.show(getFragmentManager(), "write_message_dialog");

                    sqlHelper.setInterested(mCurrentItem);
                }
                break;

            case R.id.button_item_store:
                if (mCurrentItem != null) {
                    Intent intent = new Intent(getApplicationContext(), StoreActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    intent.putExtra("id", -mCurrentItem.getOwnerId());

                    getApplicationContext().startActivity(intent);
                }
                break;
            case R.id.button_item_extrainfo:
                if (mWikiUrl != null) {
                    Intent intent = new Intent(getApplicationContext(), ExtraInfoStoreActivity.class);

                    intent.putExtra(HansaApplication.WIKI_URL, mWikiUrl);

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    getApplicationContext().startActivity(intent);

                }
                break;

            case R.id.button_more_comments:
                if (mCurrentItem != null) {

                    Intent intent = new Intent(getApplicationContext(), CommentsActivity.class);

                    intent.putExtra(HansaApplication.ITEM_ID, mItemId);
                    intent.putExtra(HansaApplication.OWNER_ID, mOwnerId);

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    getApplicationContext().startActivity(intent);

                }
                break;
            case R.id.floating_action_button_item:
                if (mCurrentItem != null) {
                    onSetDelLike(mLike);
                }
                break;

        }
    }


    private static class VkRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkRequestListner";
        private final WeakReference<ItemDetalizationActivity> mWeakRefParent;
        private boolean mCurLike;  //used in like response
        private int mResponseType;  // 0 - like, 1 - item, 2 - wiki, 3 - comments
        private VkAPIFactory mVkApiFactory;

        private VkRequestListner(ItemDetalizationActivity activity, int response_type, boolean cur_like) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mCurLike = cur_like;
            this.mResponseType = response_type;
            this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            Log.d(TAG, response.json.toString());
            if (mWeakRefParent.get() != null) {
                switch (mResponseType) {
                    case 0:
                        mWeakRefParent.get().onLikeLoaded(mCurLike);
                        break;
                    case 1:
                        mWeakRefParent.get().onItemInfoLoaded(mVkApiFactory.getItem(response));
                        break;
                    case 2:
                        mWeakRefParent.get().onStoreLoaded(mVkApiFactory.getStore(response));
                        break;
                    case 3:
                        mWeakRefParent.get().onCommentsLoaded(mVkApiFactory.getAdaptiveComments(response), mVkApiFactory.getItemCount(response));
                        break;
                }
              //  mWeakRefParent.get().removeErrorViewIfNeeded();
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            Log.d(TAG, "Type:" + mResponseType);
            if (mWeakRefParent.get() != null && mResponseType != 3) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);

            Log.d(TAG, error.errorMessage + " code: " + error.errorCode);
            if (mWeakRefParent.get() != null) {
                if (mResponseType != 3) {
                    mWeakRefParent.get().onLoadError(true);
                } else {
                    mWeakRefParent.get().onLoadCommentsError(true);
                }
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onCommentsLoaded(List<AdaptiveVkComments> adaptiveComments, int itemCount) {

        if (mCommentContainer.getVisibility() == View.GONE) {
            mCommentContainer.setVisibility(View.VISIBLE);
            buttonComment.setVisibility(View.VISIBLE);
        }

        // Log.d(LOG_TAG, "Comment Count: " + itemCount);
        mStringBuilder.setLength(0);
        textCommentsCount.setText(mStringBuilder.append(itemCount).append(" ").append(getResources().getQuantityString(R.plurals.comments, itemCount)));

        if (itemCount > 5) {
            buttonMoreComments.setVisibility(View.VISIBLE);
        }

        if (adaptiveComments != null) {

            for (int i = 0; i < adaptiveComments.size(); i++) {

                CommentView view = new CommentView(getApplicationContext());

                view.setAddaptiveComment(adaptiveComments.get(i));

                mCommentContainer.addView(view);


            }

        }

    }


    private void onLoadError(boolean b) {
        setVisibility(ERROR_VISIBILITY);
    }

    private void onLoadCommentsError(boolean b) {
        textCommentsCount.setText(getString(R.string.comments_disabled));
        mCommentContainer.setVisibility(View.GONE);
        buttonComment.setVisibility(View.GONE);
    }

    private void onItemInfoLoaded(VkItem item) {

        if (item != null) {
            mCurrentItem = item;
            updateUI();
        } else {

        }

        getStoreWiki();

    }


    private void onLikeLoaded(boolean mCurLike) {

        if (mCurLike) {
            mLike = false;
        } else {
            mLike = true;
        }
        updateFab();
    }


//    private void removeErrorViewIfNeeded() {
//        if (errorView.getVisibility() != View.GONE) {
//            errorView.setVisibility(View.GONE);
//            mViewPager.setVisibility(View.VISIBLE);
//        }
//        mScrollView.setVisibility(View.VISIBLE);
//        //mProgressBar.setVisibility(View.GONE);
//        mProgressView.setVisibility(View.GONE);
//    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        mStringBuilder = new StringBuilder();

        mOwnerId = getIntent().getIntExtra(HansaApplication.OWNER_ID, 0);
        mItemId = getIntent().getIntExtra(HansaApplication.ITEM_ID, 0);


        mCurrentItemID = mStringBuilder.append(mOwnerId).append("_").append(mItemId).toString();

        //  Log.d(LOG_TAG, mCurrentItemID);

        mStringBuilder.setLength(0);


        mVkApiFactory = new VkAPIFactory();
        sqlHelper = new DatabaseHelper(getApplicationContext());

        setContentView(R.layout.activity_item_detalization);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(" ");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


        mCommentContainer = (LinearLayout) findViewById(R.id.cardview_lastcomment_container);
        mViewPager = (ViewPager) findViewById(R.id.viewpager_item_photos);
        mIndicator = (CircleIndicator) findViewById(R.id.indicator_view_pager);

        pageListener = new DetailOnPageChangeListener();
        mViewPager.setOnPageChangeListener(pageListener);
        // mViewPager.setOnClickListener(this);

        mProgressBar = (ProgressView) findViewById(R.id.progressBar_test);
        collapsingToolbarLayout = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        textDescription = (TextView) findViewById(R.id.text_item_description);
        textPrice = (TextView) findViewById(R.id.text_item_price);
        textCategory = (TextView) findViewById(R.id.text_item_category);
        textLikes = (TextView) findViewById(R.id.text_item_likes);
        textViews = (TextView) findViewById(R.id.text_item_views);
        textCommentsCount = (TextView) findViewById(R.id.text_comments_count);

        buttonComment = (Button) findViewById(R.id.button_send_comment);
        buttonWrite = (Button) findViewById(R.id.button_item_write);
        buttonStore = (ImageButton) findViewById(R.id.button_item_store);
        buttonExtraInfo = (ImageButton) findViewById(R.id.button_item_extrainfo);
        buttonMoreComments = (Button) findViewById(R.id.button_more_comments);
        buttonMoreComments.setVisibility(View.GONE);

        mFloatingActionButton = (FloatingActionButton) findViewById(R.id.floating_action_button_item);

        mErrorView = (FrameLayout) findViewById(R.id.error_view);
        mErrorButton = (com.rey.material.widget.Button) findViewById(R.id.button_error);
        mErrorText = (TextView) findViewById(R.id.text_error);
        mImageError = (ImageView) findViewById(R.id.image_error);

        RelativeLayout errorLayout = (RelativeLayout) findViewById(R.id.relativelayout_error);
        FrameLayout.LayoutParams lp = new FrameLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT);
        lp.gravity = Gravity.TOP | Gravity.CENTER_HORIZONTAL;

        errorLayout.setLayoutParams(lp);


        mScrollView = (NestedScrollView) findViewById(R.id.item_detalization_scrollview);
        mProgressView = (ProgressView) findViewById(R.id.progress_bar_detalization_loading);

        buttonComment.setOnClickListener(this);
        buttonExtraInfo.setOnClickListener(this);
        buttonStore.setOnClickListener(this);
        buttonWrite.setOnClickListener(this);
        buttonMoreComments.setOnClickListener(this);
        mFloatingActionButton.setOnClickListener(this);


        //    mCommentContainer.addView(addView1,lp);
        // setStatusBarTranslucent(true);
        getItemInfo();
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVkRequest != null) {
            mVkRequest.cancel();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_item_detalization, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home_store:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                return true;
            case R.id.action_refresh_store:
                refresh();
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }


    public void refresh() {
        mCommentContainer.removeAllViews();
        getItemInfo();
    }


    public void refreshComments() {
        mCommentContainer.removeAllViews();
        getLastComments();
    }

    private void onSetDelLike(boolean cur_like) {

        if (!mLike)
            mVkRequest = new VKRequest("likes.add", VKParameters.from("type", "market", VKApiConst.OWNER_ID, mCurrentItem.getOwnerId(), "item_id", mCurrentItem.getId()));
        else {
            mVkRequest = new VKRequest("likes.delete", VKParameters.from("type", "market", VKApiConst.OWNER_ID, mCurrentItem.getOwnerId(), "item_id", mCurrentItem.getId()));
        }

        if (mVkRequest != null)

            mVkRequest.executeWithListener(new VkRequestListner(this, 0, cur_like));

    }

    private void updateFab() {
        //mFloatingActionButton.setAlpha(0.0f);
        if (onStartLikeLoad) {
            if (mLike) {
                mFloatingActionButton.setImageResource(R.drawable.ic_heart_orange);
            } else {
                mFloatingActionButton.setImageResource(R.drawable.ic_heart_outline_orange);
            }

            onStartLikeLoad = false;
        }
        else {
            if (mLike) {
                ImageViewAnimatedChange(mFloatingActionButton, R.drawable.ic_heart_orange);
            } else {
                ImageViewAnimatedChange(mFloatingActionButton, R.drawable.ic_heart_outline_orange);
            }
        }

        //mFloatingActionButton.animate().alpha(1.0f).setDuration(1000).setStartDelay(100);
    }

    public void ImageViewAnimatedChange(final FloatingActionButton v, final int drawableId) {
        final Animation fadeAnimation = AnimationUtils.loadAnimation(this, android.R.anim.fade_out);
        final Animation gainAnimation  = AnimationUtils.loadAnimation(this, android.R.anim.fade_in);
        fadeAnimation.setDuration(200);
        gainAnimation.setDuration(200);

        fadeAnimation.setAnimationListener(new Animation.AnimationListener()
        {
            @Override public void onAnimationStart(Animation animation) {}
            @Override public void onAnimationRepeat(Animation animation) {}
            @Override public void onAnimationEnd(Animation animation)
            {
                v.setImageResource(drawableId);
                gainAnimation.setAnimationListener(new Animation.AnimationListener() {
                    @Override public void onAnimationStart(Animation animation) {}
                    @Override public void onAnimationRepeat(Animation animation) {}
                    @Override public void onAnimationEnd(Animation animation) {}
                });
                v.startAnimation(gainAnimation);
            }
        });
        v.startAnimation(fadeAnimation);
    }

    private void getItemInfo() {
        setVisibility(PROGRESS_VISIBILITY);

        //  mCurrentItemID = "-59846185_75156";
        mVkRequest = new VKRequest("market.getById", VKParameters.from("item_ids", mCurrentItemID, VKApiConst.EXTENDED, "1"));

        mVkRequest.executeWithListener(new VkRequestListner(this, 1, false));
    }

    private void getStoreWiki() {


        mVkRequest = new VKRequest("groups.getById", VKParameters.from(VKApiConst.GROUP_ID, -mCurrentItem.getOwnerId(), VKApiConst.FIELDS, "market"));

        mVkRequest.executeWithListener(new VkRequestListner(this, 2, false));

    }

    private void onStoreLoaded(VkStoreItem store) {

        if (store != null) {
            if (store.getMarket() != null) {
                if (store.getMarket().getWiki() != null) {
                    mWikiUrl = store.getMarket().getWiki().getViewUrl();
                    buttonExtraInfo.setEnabled(true);
                } else {
                    mWikiUrl = null;
                    buttonExtraInfo.setVisibility(View.INVISIBLE);
                    buttonExtraInfo.setEnabled(false);
                }
                if (store.getMarket().getContactId() != null) {
                    mSellerId = store.getMarket().getContactId();
                } else {
                    mSellerId = 0;
                }
            }
        }

        getLastComments();

    }

    private void getLastComments() {


        //  if(mCurrentItem)
        mVkRequest = new VKRequest("market.getComments", VKParameters.from("owner_id", mOwnerId, "item_id", mItemId, "need_likes", "1", VKApiConst.SORT, "desc", VKApiConst.COUNT, "5", VKApiConst.EXTENDED, "1", VKApiConst.FIELDS, "photo_100"));


        mVkRequest.executeWithListener(new VkRequestListner(this, 3, false));

    }


    @Override
    public void onPause() {
        super.onPause();

        setVisibility(ALL_INVISIBLE);
        mPhotoAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mCurrentItem != null) {
            setVisibility(SUCCESS_VISIBILITY);
            mPhotoAdapter = new ItemDetalizationViewPagerAdapter(this, mCurrentItem.getPhotos(), mItemId, mOwnerId);
            mViewPager.setAdapter(mPhotoAdapter);
            mViewPager.setCurrentItem(pageListener.getCurrentPage());
        }
    }

    private void updateUI() {
        updateDB();

        mStringBuilder.setLength(0);
        //mCustomPagerAdapter = new CustomPagerAdapter(this);



        collapsingToolbarLayout.setTitle(mCurrentItem.getTitle());
        collapsingToolbarLayout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar);
        collapsingToolbarLayout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar);


        mPhotoAdapter = new ItemDetalizationViewPagerAdapter(this, mCurrentItem.getPhotos(), mItemId, mOwnerId);
        mViewPager.setAdapter(mPhotoAdapter);
        if (mCurrentItem.getPhotos().size() > 1) mIndicator.setViewPager(mViewPager);


        if (mCurrentItem.getCanComment() == 1) buttonComment.setVisibility(View.VISIBLE);

        textDescription.setText(mCurrentItem.getDescription());
        textPrice.setText(mCurrentItem.getPrice().getText());
        textCategory.setText(mStringBuilder.append(mCurrentItem.getCategory().getSection().getName()).append(" : ").append(mCurrentItem.getCategory().getName()));

        //     Log.d(LOG_TAG, "Views Count" + mCurrentItem.getViewsCount());
        textViews.setText(mCurrentItem.getViewsCount().toString());
        textLikes.setText(mCurrentItem.getLikes().getCount().toString());

        if (mCurrentItem.getLikes().getUserLikes() == 1) {
            mLike = true;
        } else {
            mLike = false;
        }
        updateFab();

      setVisibility(SUCCESS_VISIBILITY);
    }

    private void updateDB() {
        sqlHelper.insertItem(mCurrentItem);

    }


    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mScrollView.getVisibility() != View.GONE)
                    mScrollView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mScrollView.getVisibility() != View.GONE)
                    mScrollView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mScrollView.getVisibility() != View.GONE)
                    mScrollView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));
                mErrorButton.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getItemInfo();
                    }
                });

            }
            break;
            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mScrollView.getVisibility() != View.VISIBLE)
                    mScrollView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mScrollView.getVisibility() != View.GONE)
                    mScrollView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

        }
    }
}
