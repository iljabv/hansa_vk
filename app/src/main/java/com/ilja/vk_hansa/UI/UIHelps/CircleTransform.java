package com.ilja.vk_hansa.UI.UIHelps;

import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;

import com.squareup.picasso.Transformation;

public class CircleTransform implements Transformation {
    int borderColor;
    boolean isBorderNeeded;

    public CircleTransform(boolean isBorderNeeded, int borderColor) {
        super();
        this.isBorderNeeded = isBorderNeeded;
        this.borderColor = borderColor;
    }

    @Override
    public Bitmap transform(Bitmap source) {
        int size = Math.min(source.getWidth(), source.getHeight());

        int x = (source.getWidth() - size) / 2;
        int y = (source.getHeight() - size) / 2;

        Bitmap squaredBitmap = Bitmap.createBitmap(source, x, y, size, size);
        if (squaredBitmap != source) {
            source.recycle();
        }

        Bitmap bitmap = Bitmap.createBitmap(size, size, source.getConfig());

        Canvas canvas = new Canvas(bitmap);
        Paint paint = new Paint();
        BitmapShader shader = new BitmapShader(squaredBitmap,
                BitmapShader.TileMode.CLAMP, BitmapShader.TileMode.CLAMP);
        paint.setShader(shader);
        paint.setAntiAlias(true);

        float r = size / 2f;
        canvas.drawCircle(r, r, r, paint);

        squaredBitmap.recycle();

        if (isBorderNeeded) {
            Paint borderer = new Paint();
            //borderer.setColor(Color.WHITE);
            borderer.setColor(borderColor);
            borderer.setStyle(Paint.Style.STROKE);
            borderer.setAntiAlias(true);
            borderer.setStrokeWidth(3);
            canvas.drawCircle(r, r, r - 2, borderer);
        }

        return bitmap;
    }

    @Override
    public String key() {
        return "circle";
    }
}
