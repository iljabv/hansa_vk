package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ProgressBar;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.VkPhotoItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.FullScreenViewActivity;
import com.ilja.vk_hansa.UI.Activity.ItemDetalizationActivity;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by Ilja on 01.02.2016.
 */
public class ItemDetalizationViewPagerAdapter extends PagerAdapter implements View.OnClickListener {

    private Picasso mImageLoader;
    private Context mContext;
    private LayoutInflater mLayoutInflater;
    private List<VkPhotoItem> mPhotoList;
    private ProgressBar mProgressBar;
    private VkPhotoItem mCurrentPhotoItem;
    private int mItemId;
    private int mOwnerId;
    private int mCurrentPosition;


    public ItemDetalizationViewPagerAdapter(Context context, List<VkPhotoItem> list_photo, int item_id, int owner_id) {
        this.mPhotoList = list_photo;
        this.mContext = context;
        this.mLayoutInflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.mItemId = item_id;
        this.mOwnerId = owner_id;
    }

    @Override
    public int getCount() {
        return mPhotoList.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view == object;
    }

    @Override
    public Object instantiateItem(ViewGroup container, int position) {


        View itemView = mLayoutInflater.inflate(R.layout.item_page_photo, container, false);

        final ImageView imageView = (ImageView) itemView.findViewById(R.id.image_item_photo);

        mCurrentPosition = position;
        if (mPhotoList != null && mPhotoList.size() > 0) {
            mCurrentPhotoItem = mPhotoList.get(position);


            mImageLoader.with(mContext).load(mCurrentPhotoItem.getBestExistingPhoto()).error(R.drawable.vk_error_dog).into(imageView);
        } else {
            mImageLoader.with(mContext).load(R.drawable.vk_error_dog).into(imageView);
        }
        itemView.setOnClickListener(this);
        container.addView(itemView);
        return itemView;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((FrameLayout) object);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(mContext, FullScreenViewActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra(HansaApplication.ITEM_ID, mItemId);
        intent.putExtra(HansaApplication.OWNER_ID, mOwnerId);

        intent.putExtra(HansaApplication.POSITION_ID, ((ItemDetalizationActivity) mContext).pageListener.getCurrentPage());
        mContext.startActivity(intent);
    }
}
