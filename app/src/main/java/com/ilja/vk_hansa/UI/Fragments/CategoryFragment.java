package com.ilja.vk_hansa.UI.Fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Category;
import com.ilja.vk_hansa.Model.CategoryHelper;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.HomeActivity;
import com.ilja.vk_hansa.UI.Adapters.CategoryAdapter;
import com.ilja.vk_hansa.UI.UIHelps.SpacesItemDecoration;

import java.util.List;


public class CategoryFragment extends Fragment {
    private int mScrollOffset;

    static CategoryFragment newInstance(int page) {
        CategoryFragment pageFragment = new CategoryFragment();
        Bundle arguments = new Bundle();
        // arguments.putInt(ARGUMENT_PAGE_NUMBER, page);
        pageFragment.setArguments(arguments);
        return pageFragment;
    }

    private CategoryHelper categoryHelper;
    private RecyclerView mRecyclerView;
    private GridLayoutManager lLayout;
    public CategoryAdapter rcAdapter;
    public List<Category> rowListItem;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        categoryHelper = new CategoryHelper();
    }


    @Override
    public void onPause() {
        super.onPause();

        mScrollOffset = lLayout.findFirstVisibleItemPosition();
        rcAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();

        rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());
        mRecyclerView.setAdapter(rcAdapter);
        mRecyclerView.scrollToPosition(mScrollOffset);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container != null) { // Removing overdrawing of fragments
            container.removeAllViews();
        }

        View view = inflater.inflate(R.layout.fragment_category, null);

        lLayout = new GridLayoutManager(getContext(), 2);

        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler_view_category);
        mRecyclerView.setHasFixedSize(true);
        mRecyclerView.setLayoutManager(lLayout);

        mRecyclerView.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, getContext())); //space between elements in grid

        int datasetID;
        Bundle bundle = this.getArguments();

        if (bundle != null) {
            datasetID = bundle.getInt("id", -100);
        } else {
            datasetID = -100;
        }

        switch (datasetID) { //Switching dataset to show
            case (-100):
                rowListItem = categoryHelper.getMainCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;


            case (0):
                rowListItem = categoryHelper.getClothesSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-1):
                rowListItem = categoryHelper.getChildSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-2):
                rowListItem = categoryHelper.getElectronicSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-3):
                rowListItem = categoryHelper.getComputerSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-4):
                rowListItem = categoryHelper.getTransportSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-5):
                rowListItem = categoryHelper.getRealtySubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-6):
                rowListItem = categoryHelper.getHomeSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-7):
                rowListItem = categoryHelper.getBeautySubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-8):
                rowListItem = categoryHelper.getSportSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-9):
                rowListItem = categoryHelper.getGiftSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-10):
                rowListItem = categoryHelper.getPetsSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-11):
                rowListItem = categoryHelper.getFoodSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            case (-12):
                rowListItem = categoryHelper.getServiceSubCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;

            default:
                rowListItem = categoryHelper.getMainCategories(getResources());

                rcAdapter = new CategoryAdapter(getContext(), rowListItem, (HomeActivity) getActivity());

                break;
        }

        mRecyclerView.setAdapter(rcAdapter);

        return view;
    }
}
