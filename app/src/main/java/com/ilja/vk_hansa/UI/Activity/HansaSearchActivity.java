package com.ilja.vk_hansa.UI.Activity;

import android.app.DialogFragment;
import android.app.SearchManager;
import android.app.SearchableInfo;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.RotateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Hansa.HansaSearchResponse;
import com.ilja.vk_hansa.Model.Hansa.HansaSearchResponseItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.VKItemAdapter;
import com.ilja.vk_hansa.UI.Fragments.SearchConfigurationDialogFragment;
import com.ilja.vk_hansa.UI.UIHelps.EndlessRecyclerOnScrollListener;
import com.ilja.vk_hansa.UI.UIHelps.SpacesItemDecoration;
import com.ilja.vk_hansa.Web.HansaParamsApi;
import com.ilja.vk_hansa.Web.IHansaAPI;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

public class HansaSearchActivity extends AppCompatActivity implements SearchConfigurationDialogFragment.SearchConfigurationListener {

    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY = 6;
    // String constants
    private final String APIURL = "http://apihansa-tinymagic.rhcloud.com/";
    private String LOG_TAG = "HansaSearchActivity";

    private List<VkItem> mVkItemList; //Container for request results

    // views
    private TextView mErrorText;
    private Button mErrorButton;
    private ImageView mImageError;


    private RecyclerView mRecyclerView; //RecyclerView item
    private FrameLayout mErrorView;
    private MenuItem mMenuItem;
    private SearchView mSearchView;
    private ProgressView mProgressView;
    private TextView textItemsCount;
    private ImageButton changeColsButton;
    private ImageButton changeQueryParametersButton; //Button on second toolbar opens SearchConfig
    private String initTextQuery;

    // UI help
    private VKItemAdapter itemAdapter; //Adapter for RecyclerView
    private GridLayoutManager mGridLayoutManager; //RVManager, needed to dynamically change columns number
    private StringBuilder mStringBuilder;
    private EndlessRecyclerOnScrollListener mEndlessScrollListener;
    private DialogFragment configurationDialog;
    HansaParamsApi newQuery;
    //search params
    private int mSortType;      // params for query; null or -1 default if parameter not used in query
    private int mMinPrice;
    private int mMaxPrice;
    private int mCategoryID;
    private String mQueryStr;
    private int mPage;
    private int mAfterData;
    private int mLikes;
    private int mCountElements = 0;
    private final int mCountOnPage = 20;  // default parameter for server, does not change
    private int mCurrentPage; //page for endless scroll
    private int mMaxPage;
    private int mRev;

    // API requests and listeners
    private VKRequest mVkItemsRequest;
    private Call mHansaSearchCall;
    private VkAPIFactory mVkApiFactory;
    private IHansaAPI restApi;
    private int mScrollOffset;
    private boolean isDataCached = false;


    private static class HansaSearchCallback implements Callback<HansaSearchResponse> {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<HansaSearchActivity> mWeakRefParent;

        private boolean mReset;
        private VkAPIFactory mVkApiFactory;

        private HansaSearchCallback(HansaSearchActivity activity, boolean reset) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mVkApiFactory = new VkAPIFactory();
            this.mReset = reset;
        }

        @Override
        public void onResponse(Response<HansaSearchResponse> response) {

            if (mWeakRefParent.get() != null) {

                mWeakRefParent.get().onHansaResponseLoaded(response.body().getResults(), mReset, response.body().getMaxpage(), response.body().getItemscount());

            }

        }

        @Override
        public void onFailure(Throwable t) {


            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }

        }
    }


    private static class VkItemsRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<HansaSearchActivity> mWeakRefParent;
        private VkAPIFactory mVkApiFactory;

        private VkItemsRequestListner(HansaSearchActivity activity) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);

            if (mWeakRefParent.get() != null) {

                mWeakRefParent.get().onItemPageLoaded(mVkApiFactory.getItemList(response));
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_hansa_search);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(getString(R.string.search));
        mStringBuilder = new StringBuilder();
        mVkApiFactory = new VkAPIFactory();
        Retrofit restAdapter = new Retrofit.Builder().baseUrl(APIURL).addConverterFactory(GsonConverterFactory.create()).build();
        restApi = restAdapter.create(IHansaAPI.class);


        mCurrentPage = 1;
        mMaxPage = -1;
        mRev = 0;
        mSortType = 0;      // params for query; null or -1 default if parameter not used in query
        mMinPrice = -1; //-1
        mMaxPrice = -1;
        mCategoryID = -1;
        mQueryStr = null;
        mPage = 1;
        mAfterData = -1;
        mLikes = -1;

        Bundle bundle = getIntent().getExtras();
        boolean isStartSearchNeeded = false;
        if (bundle != null) {
            this.mMinPrice = bundle.getInt("min", -1);
            this.mMaxPrice = bundle.getInt("max", -1);
            this.mCategoryID = bundle.getInt("categoryID", -1);
            this.mSortType = bundle.getInt("sortType", -1);

            isStartSearchNeeded = bundle.getBoolean("isStartSearchNeeded", false);
        }

        mProgressView = (ProgressView) findViewById(R.id.progressBar_VK_loadmore);


        textItemsCount = (TextView) findViewById(R.id.text_test_elements_count);


        mErrorView = (FrameLayout) findViewById(R.id.error_view);
        mErrorButton = (Button) findViewById(R.id.button_error);
        mErrorText = (TextView) findViewById(R.id.text_error);
        mImageError = (ImageView) findViewById(R.id.image_error);
        // RecyclerView init block start
        mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view); //Getting RView
        mGridLayoutManager = new GridLayoutManager(this, 2); //Creating LayoutManager
        mRecyclerView.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, this));
        mRecyclerView.setLayoutManager(mGridLayoutManager); //Setting params
        mRecyclerView.setHasFixedSize(true); //Setting params

        mEndlessScrollListener = new EndlessRecyclerOnScrollListener(mGridLayoutManager) {
            @Override
            public void onLoadMore(int current_page) {
                getItemsfromAPI(false, current_page);
            }
        };

        mRecyclerView.addOnScrollListener(mEndlessScrollListener);

        mVkItemList = new ArrayList<>(); //Creating container

        itemAdapter = new VKItemAdapter(this, mVkItemList, true, true); //Creating adapter
        mRecyclerView.setAdapter(itemAdapter); //Setting created adapter


        // RecyclerView init block end

        //Button init block start
        View.OnClickListener colsChangeListener = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mRecyclerView.removeOnScrollListener(mEndlessScrollListener); //preventing additional request

                int currentColumnsCount = mGridLayoutManager.getSpanCount();

                changeColsButton.setVisibility(View.VISIBLE); //animation block
                changeColsButton.setAlpha(0.0f);
                mRecyclerView.setAlpha(0.0f);

                if (currentColumnsCount == 2) //changing icon
                    changeColsButton.setImageResource(R.drawable.ic_view_module_orange);
                else changeColsButton.setImageResource(R.drawable.ic_view_list_orange);

                changeColsButton.animate().alpha(1.0f).setDuration(500); //animation block
                mRecyclerView.animate().alpha(1.0f).setDuration(500);

                mGridLayoutManager = new GridLayoutManager(HansaSearchActivity.this, currentColumnsCount == 2 ? 1 : 2);
                mRecyclerView.setLayoutManager(mGridLayoutManager);

                mEndlessScrollListener = new EndlessRecyclerOnScrollListener(mGridLayoutManager) {
                    @Override
                    public void onLoadMore(int current_page) {
                        getItemsfromAPI(false, current_page);
                    }
                };
                mRecyclerView.addOnScrollListener(mEndlessScrollListener); //adding new endlessScroll

                itemAdapter = new VKItemAdapter(HansaSearchActivity.this, mVkItemList, currentColumnsCount == 2 ? false : true, true);
                mRecyclerView.setAdapter(itemAdapter); //adding new adapter
            }
        };

        changeColsButton = (ImageButton) findViewById(R.id.button_change_columns);
        changeColsButton.setOnClickListener(colsChangeListener);


        configurationDialog = new SearchConfigurationDialogFragment();


        changeQueryParametersButton = (ImageButton) findViewById(R.id.button_change_query_parameters);
        changeQueryParametersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                RotateAnimation ra = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
                //ra.setFillAfter(true);
                ra.setDuration(300);
                changeQueryParametersButton.startAnimation(ra);

                Bundle bundle_params = new Bundle();
                bundle_params.putBoolean("category", true);
                bundle_params.putInt("section", mCategoryID);
                configurationDialog.setArguments(bundle_params);

                configurationDialog.show(getFragmentManager(), "configuration_dialog");
                //((SearchConfigurationDialogFragment) configurationDialog).setSelection(mCategoryID);
            }
        });

        initTextQuery = getIntent().getStringExtra(HansaApplication.SEARCH_QUERY);

        if (isStartSearchNeeded) {
            getItemsfromAPI(true, 1);
        }
        //  getItemsfromAPI(true, 1);
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (mVkItemsRequest != null) mVkItemsRequest.cancel();
        if (mHansaSearchCall != null && mHansaSearchCall.isExecuted()) {
            mHansaSearchCall.cancel();
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
            case R.id.action_home_store:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        //super.onCreateOptionsMenu(menu, inflater);
        menu.clear();
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_search, menu);

        SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
        mMenuItem = menu.findItem(R.id.action_search);

        if (initTextQuery != null) {
            mMenuItem.expandActionView();
        }
        mSearchView = (SearchView) mMenuItem.getActionView();


        ComponentName componentName = getComponentName();
        SearchableInfo searchableInfo = searchManager.getSearchableInfo(componentName);
        mSearchView.setSearchableInfo(searchableInfo);
        mSearchView.setMaxWidth(2000);
        mSearchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {

                mPage = 1;
                mQueryStr = query;
                getItemsfromAPI(true, mPage);

                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) { //Can work not properly there!!!!

                /*if (newText.isEmpty() && mQueryStr != null) { //Request onEmpty query, needs rework
                    if (!mQueryStr.isEmpty()) {
                        Log.d(LOG_TAG, "Start search");
                        mPage = 1;
                        mQueryStr = newText;
                        getItemsfromAPI(true, mPage);
                    }
                }*/

                return false;
            }
        });


        if (initTextQuery != null) {
            Handler h = new Handler() {
                public void handleMessage(android.os.Message msg) {
                    switch (msg.what) {
                        case 0:
                            mSearchView.setQuery(initTextQuery, true);
                            break;

                        case 1:
                            View view = HansaSearchActivity.this.getCurrentFocus();

                            if (view != null) {
                                InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
                                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
                            }
                            break;
                    }
                }

                ;
            };
            //mSearchView.setQuery(initTextQuery, true);
            h.sendEmptyMessage(0);
            h.sendEmptyMessage(1);
        }
        return super.onCreateOptionsMenu(menu);
    }

    private void updateRecyclerView() {

        if (itemAdapter != null) {
            itemAdapter.notifyDataSetChanged(); //Refresh our RecyclerView (Works faster(?) then item-by-item)
        }
    }

    private void resetRecyclerView() {

        mRecyclerView.setAdapter(null);

        mVkItemList.clear();
        itemAdapter = new VKItemAdapter(this, mVkItemList, (mGridLayoutManager.getSpanCount() == 2) ? true : false, true);
        mRecyclerView.setAdapter(itemAdapter);

        //    mEndlessScrollListener.Reload();

    }


    @Override
    public void onPause() {
        super.onPause();

        setVisibility(ALL_INVISIBLE);
        mScrollOffset = mGridLayoutManager.findFirstVisibleItemPosition();
        itemAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();

        if (mVkItemList != null) {
            itemAdapter = new VKItemAdapter(this, mVkItemList, (mGridLayoutManager.getSpanCount() == 2) ? true : false, true);
            mRecyclerView.setAdapter(itemAdapter);

            mEndlessScrollListener.Reload();
            mRecyclerView.scrollToPosition(mScrollOffset);
            setVisibility(SUCCESS_VISIBILITY);
        }
    }

    private void getItemsfromAPI(final boolean reset, int cur_page) {

        mPage = cur_page;

        if (mPage > mMaxPage && !reset) return;

        //    Log.d(LOG_TAG, "Start request");
        if (reset) setVisibility(PROGRESS_VISIBILITY);
        else setVisibility(PART_PROGRESS_VISIBILITY);

        newQuery = new HansaParamsApi(mSortType, mQueryStr, mMaxPrice, mMinPrice, mPage, mAfterData, mCategoryID, mRev);

        /******************************
         *
         * Alternative Using:
         *
         newQuery = new HansaParamsApi();
         newQuery.setmSort(mSortType).setmSearch(mQueryStr).setmMaxprice(mMaxPrice);
         ********************************/


        mHansaSearchCall = restApi.getItemsWithParams(newQuery.GetMap());
        mHansaSearchCall.enqueue(new HansaSearchCallback(this, reset));

    }


    private void getItemsFromVK(List<HansaSearchResponseItem> listItems) {

        if (listItems.isEmpty()) return;


        mStringBuilder.setLength(0);
        for (int i = 0; i < listItems.size(); i++) {
            mStringBuilder.append(listItems.get(i).getOwnerId()).append("_").append(listItems.get(i).getId()).append(",");
        }
        mStringBuilder.setLength(mStringBuilder.length() - 1);

        mVkItemsRequest = new VKRequest("market.getById", VKParameters.from("item_ids", mStringBuilder.toString(), VKApiConst.EXTENDED, "1"));

        mVkItemsRequest.executeWithListener(new VkItemsRequestListner(this));
    }

    private void onHansaResponseLoaded(List<HansaSearchResponseItem> listItems, boolean reset, int maxpage, Integer maxitems) {

//        mProgressView.setVisibility(View.GONE);
//        if (mErrorView.getVisibility() != View.GONE) {
//            mErrorView.setVisibility(View.GONE);
//        }
        if (listItems != null && listItems.size() > 0) {
            if (reset) {
                mMaxPage = maxpage;
                mStringBuilder.setLength(0);
                textItemsCount.setText(mStringBuilder
                        .append(getApplicationContext().getResources().getQuantityString(R.plurals.found, maxitems))
                        .append(" ")
                        .append(maxitems)
                        .append(" ")
                        .append(getApplicationContext().getResources().getQuantityString(R.plurals.items, maxitems)));


                resetRecyclerView();
                mCountElements = 0;
            }

            getItemsFromVK(listItems);
        } else {
            setVisibility(NOT_FOUND_VISIBILITY);
        }
    }


    private void onItemPageLoaded(List<VkItem> vkItemList) {

        if (vkItemList != null && vkItemList.size() > 0) {
            mVkItemList.addAll(vkItemList);
            mCountElements += vkItemList.size();

               setVisibility(SUCCESS_VISIBILITY);

            updateRecyclerView();
        }

    }


    //TODO: set error background image
    private void onLoadError(boolean type) //false attempt failed, true error
    {
        setVisibility(ERROR_VISIBILITY);
    }

    @Override
    public void onApplyChanges(Bundle bundle) {
        if (bundle != null) {
            this.mMinPrice = bundle.getInt("min", -1);
            this.mMaxPrice = bundle.getInt("max", -1);
            //this.mCategoryID = bundle.getInt("categoryID", -1);
            this.mSortType = bundle.getInt("sortType", 0);
            this.mRev = bundle.getInt("rev", 0);

            int newCategory = bundle.getInt("categoryID", -1);
            if (newCategory > 0 || newCategory == -1) {
                //  Log.d(LOG_TAG, "Category: " + Integer.toString(newCategory));
                this.mCategoryID = newCategory;
            }

            RotateAnimation ra = new RotateAnimation(0, 180, Animation.RELATIVE_TO_SELF, 0.5f, Animation.RELATIVE_TO_SELF, 0.5f);
            ra.setDuration(300);
            changeQueryParametersButton.startAnimation(ra);

            getItemsfromAPI(true, 1);
        }
    }


    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));
                mErrorButton.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getItemsfromAPI(true, 1);
                    }
                });

            }
            break;

            case NOT_FOUND_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.nothing_found));

                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);
                mErrorButton.setVisibility(View.GONE);
                mErrorButton.setOnClickListener(null);

            }
            break;

            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case PART_PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
        }
    }


}
