package com.ilja.vk_hansa.UI.Fragments;

import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.DatabaseHelper;
import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.VKItemAdapter;
import com.ilja.vk_hansa.UI.UIHelps.SpacesItemDecoration;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**

 */
public class FavoritesPageFragment extends Fragment {

    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY = 6;

    private static final String KEY_POSITION = "position";
    private final String LOG_TAG = "FavoritesPageFragment";
    private final int COUNT = 30;
    private int mPosition;
    private RecyclerView mRecyclerView;
    private VKItemAdapter mVkItemAdapter;
    private List<VkItem> mItemsList;
    private GridLayoutManager mGridLayoutManager;
    private VkAPIFactory mVkApiFactory;
    private VKRequest vkRequest;

    private TextView mErrorText;
    private Button mErrorButton;
    private ImageView mImageError;
    private FrameLayout mErrorView;

    private DatabaseHelper sqlHelper;
    private SQLiteDatabase db;
    private int mCountItems = -1;
    private ProgressView mProgressView;


    public static FavoritesPageFragment newInstance(int position) {
        FavoritesPageFragment frag = new FavoritesPageFragment();
        Bundle args = new Bundle();

        args.putInt(KEY_POSITION, position);
        frag.setArguments(args);

        return (frag);
    }


    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_favorites_page, container, false);

        mPosition = getArguments().getInt(KEY_POSITION, -1);
        mVkApiFactory = new VkAPIFactory();
        //  TextView text = (TextView) view.findViewById(R.id.test_textview);
        mItemsList = new ArrayList<>();

        mVkItemAdapter = new VKItemAdapter(getContext(), mItemsList, true, false);
        mProgressView = (ProgressView)view.findViewById(R.id.progressbar_favorites);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerview_favorites);

        mRecyclerView.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, getContext()));
        mGridLayoutManager = new GridLayoutManager(getContext(), 2); //Creating LayoutManager
        mRecyclerView.setLayoutManager(mGridLayoutManager); //Setting params
        mRecyclerView.setHasFixedSize(true); //Setting params
        mRecyclerView.setAdapter(mVkItemAdapter);

        mErrorView = (FrameLayout) view.findViewById(R.id.error_view);
        mErrorButton = (Button) view.findViewById(R.id.button_error);
        mErrorText = (TextView) view.findViewById(R.id.text_error);
        mImageError = (ImageView) view.findViewById(R.id.image_error);

        sqlHelper = new DatabaseHelper(getContext());

//        switch (mPosition) {
//            case 0:
//                break;
//            case 1:
//                getRecentlyViewed(false);
//                break;
//            case 2:
//                getRecentlyViewed(true);
//                break;
//        }

        return (view);
    }


    private static class FavoritesRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "FavoritesRequestListner";
        private final WeakReference<FavoritesPageFragment> mWeakRefParent;
        private final int page;
        private VkAPIFactory mVkApiFactory;

        public FavoritesRequestListner(FavoritesPageFragment frag, int page) {
            this.mWeakRefParent = new WeakReference<>(frag);
            this.page = page;
            mVkApiFactory = new VkAPIFactory();

        }

        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onItemPageLoaded(mVkApiFactory.getItemList(response), mVkApiFactory.getItemCount(response), page);
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);

            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }

    }

    private void onLoadError(boolean b) {
       setVisibility(ERROR_VISIBILITY);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        if (vkRequest != null) vkRequest.cancel();
    }


    protected void onItemPageLoaded(List<VkItem> list, int max_count, int page) {

        if (list != null && list.size() > 0) {
            Log.d(LOG_TAG, "LOADED");

            mItemsList.addAll(list);
            Log.d(LOG_TAG, list.get(0).getTitle() + list.get(0).getDescription());
            if (page == 0) {
                mCountItems = max_count;
            }

            setVisibility(SUCCESS_VISIBILITY);

            updateUI();
        } else {
            // Log.d(LOG_TAG, "NOTHING TO SHOW");
           setVisibility(NOT_FOUND_VISIBILITY);
        }


    }


    @Override
    public void onPause() {
        super.onPause();

        setVisibility(ALL_INVISIBLE);
        mVkItemAdapter = null;
    }


    @Override
    public void onResume() {
        super.onResume();
        setVisibility(SUCCESS_VISIBILITY);
        mItemsList.clear();

        switch (mPosition) {
            case 0:
                getFavorites(0);
                break;
            case 1:
                getRecentlyViewed(false);
                break;
            case 2:
                getRecentlyViewed(true);
                break;
        }

    }

    private void getFavorites(int page) {
        int offset = page * COUNT;
        if (offset > mCountItems && page > 0) return;

       if (page==0) setVisibility(PROGRESS_VISIBILITY);

        vkRequest = new VKRequest("fave.getMarketItems", VKParameters.from(VKApiConst.COUNT, COUNT, VKApiConst.OFFSET, offset, VKApiConst.EXTENDED, "1"));

        vkRequest.executeWithListener(new FavoritesRequestListner(this, page));


    }

    private void getRecentlyViewed(boolean interest) {
        // Log.d(LOG_TAG, "Get recently viewed " + mPosition);
        sqlHelper.addRecentlyViewedItems(mItemsList, interest);

        if (mItemsList != null && mItemsList.size() > 0) {
            updateUI();
        } else {
           setVisibility(NOT_FOUND_VISIBILITY);
        }
    }

    private void updateUI() {

        if (mVkItemAdapter != null) {
            mVkItemAdapter.notifyDataSetChanged();
        }
    }



    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));
                mErrorButton.setVisibility(View.VISIBLE);
                Picasso.with(getContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getFavorites(0);
                    }
                });

            }
            break;

            case NOT_FOUND_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.nothing_found));

                Picasso.with(getContext()).load(R.drawable.vk_error_dog).into(mImageError);
                mErrorButton.setVisibility(View.GONE);
                mErrorButton.setOnClickListener(null);

            }
            break;

            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case PART_PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;
        }
    }


}

