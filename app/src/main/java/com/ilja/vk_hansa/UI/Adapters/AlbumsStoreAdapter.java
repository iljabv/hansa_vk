package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkAlbumItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.VKSearchActivity;
import com.ilja.vk_hansa.UI.CustomViews.HeaderStoreView;
import com.ilja.vk_hansa.UI.UIHelps.SpacesItemDecoration;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 29.01.2016.
 */
public class AlbumsStoreAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private final int TYPE_HEADER = 0;
    private final int TYPE_ITEM = 2;
    private final int TYPE_LASTITEMS = 1;
   // private final int TYPE_POPULAR= 2;


    public void setItemsCount(int itemsCount) {
        this.mItemsCount = itemsCount;
    }


    public class mAlbumItemViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private TextView textName;
        private ImageView imagePhoto;
        private TextView textCount;
        private Context mContext;
        private int album_id;
        private int group_id;

        // RelativeLayout containerRelativeLayout;
        mAlbumItemViewHolder(View itemView, Context context) {

            super(itemView);
            this.textName = (TextView) itemView.findViewById(R.id.text_album_name);
            this.imagePhoto = (ImageView) itemView.findViewById(R.id.image_album_photo);
            this.textCount = (TextView) itemView.findViewById(R.id.text_album_count);
            this.mContext = context;
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            //Toast.makeText(context, "Clicked", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(mContext, VKSearchActivity.class);
            Bundle extras = new Bundle();
            extras.putBoolean("search_type", true);
            extras.putInt("album_id", album_id);
            //  Log.d("Search", "album: " + Integer.toString(album_id));
            extras.putInt("group_id", group_id);
            //   Log.d("Search", "group: " + Integer.toString(group_id));

            intent.putExtras(extras);
            intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            mContext.startActivity(intent);
        }
    }

    public boolean isHeader(int position) {
        return position == 0;
    }


    public class LastItemsViewHolder extends RecyclerView.ViewHolder {

        private int type;  // 0 - popular, 1 - newest
        private List<VkItem> mListItems;
        private TextView mTextDesc;
        private RecyclerView mRecyclerView;
        private Context mContext;
        private LastItemsAdapter mLastItemsAdapter;
        private ImageView mImageView;

        LastItemsViewHolder(View itemView, Context context) {
            super(itemView);
            this.mListItems = new ArrayList<VkItem>();
            this.mTextDesc = (TextView) itemView.findViewById(R.id.text_store_lastitems);
            this.mRecyclerView = (RecyclerView) itemView.findViewById(R.id.recyclerview_lastitems);
            this.mRecyclerView.setLayoutManager(new LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false));
            this.mRecyclerView.addItemDecoration(new SpacesItemDecoration(HansaApplication.RECYCLERVIEW_SPACES, context));
            this.mImageView = (ImageView) itemView.findViewById(R.id.image_new_desc);
            this.mContext = context;
            setAdapter(mListItems);
        }

        public void setAdapter(List<VkItem> list) {
            //     mListItems = list;

            mLastItemsAdapter = new LastItemsAdapter(mListItems, mContext);
            mRecyclerView.setAdapter(mLastItemsAdapter);
        }

        public void updateAdapter() {
            mLastItemsAdapter.notifyDataSetChanged();
        }
    }

    public class HeaderViewHolder extends RecyclerView.ViewHolder {

        HeaderViewHolder(View itemView) {
            super(itemView);
        }


    }

    private List<VkItem> mArrayLastItems;
    private List<VkItem> mArrayPopularItems;
    private List<VkAlbumItem> mArrayAlbums;
    private StringBuilder mStringBuilder;
    private Picasso mImageLoader;

    private mAlbumItemViewHolder mAlbumItemHolder;
    private HeaderStoreView mHeaderStoreView;
    private LastItemsViewHolder mLastItemsView;
    private LastItemsViewHolder mPopularItemsView;
    private VkAlbumItem mVkAlbum;
    private int mItemsCount;
    private VkStoreItem mCurrentStore;
    private Context mContext;
    private final String LOG_TAG = "MyAlbumsAdapter";

    public AlbumsStoreAdapter(VkStoreItem cur_store, List<VkItem> mLastItemsList,List<VkItem> mPopularItemsList, List<VkAlbumItem> array_albums, Context context, int items_count) {
        this.mCurrentStore = cur_store;
        this.mArrayAlbums = array_albums;
        this.mContext = context;
        this.mArrayLastItems = mLastItemsList;
        this.mArrayPopularItems = mPopularItemsList;
        this.mStringBuilder = new StringBuilder();
        this.mItemsCount = items_count;
    }


    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v;
        RecyclerView.ViewHolder pvh;

        if (i == TYPE_HEADER) {
            v = new HeaderStoreView(viewGroup.getContext());
            pvh = new HeaderViewHolder(v);

        } else if (i == TYPE_LASTITEMS) {
            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_store_lastitems, viewGroup, false);
            pvh = new LastItemsViewHolder(v, mContext);

        }
//        else if (i==TYPE_POPULAR) {
//            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_store_lastitems, viewGroup, false);
//            pvh = new LastItemsViewHolder(v, mContext);
//        }
            else {

            v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_album, viewGroup, false);
            pvh = new mAlbumItemViewHolder(v, mContext);
        }

        return pvh;
    }


    @Override
    public int getItemViewType(int position) {


        switch (position) {
            case 0:
                return TYPE_HEADER;
            case 1:
                return TYPE_LASTITEMS;
//            case 2:
//                return TYPE_LASTITEMS;
            default:
                return TYPE_ITEM;
        }


    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int i) {


        if (i == TYPE_HEADER && mCurrentStore != null) {
            mHeaderStoreView = (HeaderStoreView) holder.itemView;
            mHeaderStoreView.setStore(mCurrentStore, mItemsCount);

        } else if (i == TYPE_LASTITEMS && mArrayLastItems != null) {

            mLastItemsView = (LastItemsViewHolder) holder;

            mLastItemsView.mTextDesc.setText(mContext.getString(R.string.about_last_items));
            Picasso.with(mContext).load(R.drawable.ic_action_new).into(mLastItemsView.mImageView);
            mLastItemsView.mListItems.clear();
            mLastItemsView.mListItems.addAll(mArrayLastItems);
            mLastItemsView.updateAdapter();

            //      mLastItemsView.setAdapter(mArrayLastItems);

        }
        /*else if (i ==TYPE_POPULAR && mArrayPopularItems != null) {

            mPopularItemsView = (LastItemsViewHolder) holder;

            mPopularItemsView.mTextDesc.setText(mContext.getString(R.string.about_last_popular_items));
            Picasso.with(mContext).load(R.drawable.ic_action_star).into(mLastItemsView.mImageView);
            mPopularItemsView.mListItems.clear();
            mPopularItemsView.mListItems.addAll(mArrayPopularItems);
            mPopularItemsView.updateAdapter();

            //      mLastItemsView.setAdapter(mArrayLastItems);

        }*/
        else if (mArrayAlbums != null) {
            mStringBuilder.setLength(0);

            mAlbumItemHolder = (mAlbumItemViewHolder) holder;
            mVkAlbum = mArrayAlbums.get(i - 2);

            mAlbumItemHolder.textName.setText(mVkAlbum.getTitle());
            mAlbumItemHolder.textCount.setText(mStringBuilder.append(mVkAlbum.getCount()));


            if (mVkAlbum.getPhoto() != null)
                mImageLoader.with(mContext).load(mVkAlbum.getPhoto().getBestExistingPhoto()).error(R.drawable.album_default_icon).into(mAlbumItemHolder.imagePhoto);
            else {
                mImageLoader.with(mContext).load(R.drawable.album_default_icon).into(mAlbumItemHolder.imagePhoto);
            }

            mAlbumItemHolder.album_id = mVkAlbum.getId();
            if (mCurrentStore != null) mAlbumItemHolder.group_id = mCurrentStore.getId();
        }


    }


    @Override
    public int getItemCount() {
        return mArrayAlbums.size() + 2;
    }
}