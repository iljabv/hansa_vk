package com.ilja.vk_hansa.UI.UIHelps;

import android.view.View;
import android.view.animation.Animation;
import android.view.animation.Transformation;

/**
 * Created by Egorov on 20.03.2016.
 */
public class ExpandingButtonAnimation extends Animation {
    private int height;
    private int width;

    private View view;
    private float heightStep;
    private float widthStep;

    public ExpandingButtonAnimation(View view, int height, int width, int oldHeight, int oldWidth) {
        this.view = view;
        this.height = oldHeight;
        this.width = oldWidth;

        this.heightStep = (height - oldHeight);
        this.widthStep = (width - oldWidth);
    }

    @Override
    protected void applyTransformation(float interpolatedTime, Transformation t) {
        view.getLayoutParams().height = (int) (height + heightStep * interpolatedTime);
        view.getLayoutParams().width = (int) (width + widthStep * interpolatedTime);

        view.requestLayout();
    }

    @Override
    public boolean willChangeBounds() {
        return true;
    }
}
