package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.Model.Category;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.HansaSearchActivity;
import com.ilja.vk_hansa.UI.Activity.HomeActivity;
import com.ilja.vk_hansa.UI.Fragments.CategoryFragment;

import java.io.InputStream;
import java.util.List;

/**
 * Created by Egorov on 27.01.2016.
 */
public class CategoryAdapter extends RecyclerView.Adapter<CategoryAdapter.CategoryHolder> {

    public static class CategoryHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        public TextView categoryName;
        public ImageView categoryPhoto;


        public int categoryID = 0;
        public Context context;
        LayoutInflater inflater;
        HomeActivity homeActivity;

        public CategoryHolder(View itemView, Context context, HomeActivity homeActivity) {

            super(itemView);
            itemView.setOnClickListener(this);

            this.categoryName = (TextView) itemView.findViewById(R.id.category_name);
            this.categoryPhoto = (ImageView) itemView.findViewById(R.id.category_photo);

            this.context = context;
            this.inflater = LayoutInflater.from(context);
            this.homeActivity = homeActivity;
        }

        @Override
        public void onClick(View view) {

            if (categoryID < 1) {

                Bundle bundle = new Bundle();
                bundle.putInt("id", categoryID); //Put arguments for new fragment

                Fragment fragment = new CategoryFragment(); //New fragment creating
                fragment.setArguments(bundle);

                //homeActivity.removeItemFromBackStack("category_fragment_2");
                FragmentTransaction transaction = homeActivity.getSupportFragmentManager().beginTransaction();
                transaction.setCustomAnimations(R.anim.slide_in_left, R.anim.slide_out_left, R.anim.slide_in_right, R.anim.slide_out_right);
                transaction.replace(R.id.flContent, fragment);
                //homeActivity.addItemToBackStack("category_fragment_2");
                transaction.commit();
            } else {

                Bundle bundle = new Bundle();
                bundle.putInt("categoryID", categoryID);
                bundle.putBoolean("isStartSearchNeeded", true);

                Intent _intent = new Intent(context, HansaSearchActivity.class);
                _intent.putExtras(bundle);
                context.startActivity(_intent);
            }

        }
    }

    private List<Category> mCategoryList;
    private Context mContext;
    private LayoutInflater mInflater;
    private HomeActivity mHomeActivity;
    private Category mCategoryItem;

    public CategoryAdapter(Context context, List<Category> itemList, HomeActivity homeActivity) {
        this.mCategoryList = itemList;
        this.mContext = context;

        this.mInflater = LayoutInflater.from(context);
        this.mHomeActivity = homeActivity;
    }

    @Override
    public CategoryHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category, null);
        CategoryHolder rcv = new CategoryHolder(layoutView, mContext, mHomeActivity);
        return rcv;
    }

    @Override
    public void onBindViewHolder(CategoryHolder holder, int position) {

        mCategoryItem = mCategoryList.get(position);

        holder.categoryName.setText(mCategoryItem.getName());

        try {
            InputStream is = mContext.getAssets().open(mCategoryItem.getPhoto());
            Drawable d = Drawable.createFromStream(is, null);

            holder.categoryPhoto.setImageDrawable(d);
            //holder.categoryPhoto.setImageResource(mCategoryItem.getPhoto());
        } catch (Exception e) {
            e.printStackTrace();
        }

        holder.categoryID = mCategoryItem.getId();
    }

    @Override
    public int getItemCount() {
        return this.mCategoryList.size();
    }
}
