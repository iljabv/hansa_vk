package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.StoreActivity;
import com.ilja.vk_hansa.UI.UIHelps.CircleTransform;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MyStoresAdapter extends RecyclerView.Adapter<MyStoresAdapter.PersonViewHolder> {


    public static class PersonViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        private int mId;
        private TextView nameText;
        private TextView membersCountText;
        private ImageView mainPhotoImage;
        private RelativeLayout containerRelativeLayout;
        private Context mContext;

        PersonViewHolder(View itemView, final Context context) {

            super(itemView);
            this.mContext = context;

            this.containerRelativeLayout = (RelativeLayout) itemView.findViewById(R.id.relativelayout_mystores);
            this.nameText = (TextView) itemView.findViewById(R.id.text_comment_name);
            this.membersCountText = (TextView) itemView.findViewById(R.id.text_comment_date);
            this.mainPhotoImage = (ImageView) itemView.findViewById(R.id.image_comment_photo);
            this.containerRelativeLayout.setOnClickListener(this);

        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(mContext, StoreActivity.class);
            intent.putExtra("id", mId);
            mContext.startActivity(intent);
        }
    }

    private Picasso mImageLoader;
    private StringBuilder mStringBuilder;
    private List<VkStoreItem> mArrayStores;
    private Context mContext;
    private final String LOG_TAG = "MyStoresAdapter";
    private VkStoreItem curStore;

    public MyStoresAdapter(List<VkStoreItem> array_stores, Context context) {
        this.mArrayStores = array_stores;
        this.mContext = context;
        this.mStringBuilder = new StringBuilder();
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {

        View v;
        PersonViewHolder pvh;

        v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_mystores, viewGroup, false);
        pvh = new PersonViewHolder(v, mContext);

        return pvh;
    }


    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {


        curStore = mArrayStores.get(i);

        mStringBuilder.setLength(0);
        personViewHolder.mId = curStore.getId();
        personViewHolder.nameText.setText(curStore.getName());
        personViewHolder.membersCountText.setText(mStringBuilder.append(curStore.getMembersCount()).append(" ").append(mContext.getResources().getQuantityString(R.plurals.members, curStore.getMembersCount())));

        if (curStore.getPhoto200() != null) {  //TODO: set default image if getPhoto200() == null
            mImageLoader.with(mContext).load(curStore.getPhoto200()).transform(new CircleTransform(false, 0)).into(personViewHolder.mainPhotoImage);
        } else {
            mImageLoader.with(mContext).load(R.drawable.vk_dog).into(personViewHolder.mainPhotoImage);
        }

    }


    @Override
    public int getItemCount() {
        return mArrayStores.size();
    }
}