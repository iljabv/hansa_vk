package com.ilja.vk_hansa.UI.Fragments;

import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.R;
import com.rey.material.widget.Button;
import com.rey.material.widget.ProgressView;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;

/**
 * Created by Ilja on 19.03.2016.
 */
public class WriteMessageDialogFragment extends DialogFragment implements View.OnClickListener {

    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;

    private final String LOG_TAG = "WriteMessageDialog";
    private Button mButtonSend;
    // private Button mButtonSenAuto;
    private CheckBox mCheckBoxAttach;
    private EditText mEditText;
    private TextView mTextLabel;
    private VKRequest mVkRequest;
    private int mSellerId;
    private ProgressView mProgressView;
    private StringBuilder mStringBuilder;
    private String mItemId;
    private RelativeLayout mRelativeLayoutSend;
    private RelativeLayout mRelativeLayoutSuccess;
    private Button mButtonOpenDialog;
    private Button mButtonClose;
    private FrameLayout mErrorView;

    private static class VkRequestListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkWriteRequestListner ";
        private final WeakReference<WriteMessageDialogFragment> mWeakRefParent;

        private VkRequestListner(WriteMessageDialogFragment parent_view) {
            this.mWeakRefParent = new WeakReference<>(parent_view);

        }

        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            if (mWeakRefParent.get() != null) {

                mWeakRefParent.get().onMessageSended();
            }
        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            if (mWeakRefParent.get() != null) {
                //  Log.d(LOG_TAG,error.toString()+error.errorMessage);
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onLoadError(boolean b) {

        setVisibility(ERROR_VISIBILITY);

    }

    private void onMessageSended() {

        setVisibility(SUCCESS_VISIBILITY);

    }

    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mRelativeLayoutSend.getVisibility() != View.VISIBLE)
                    mRelativeLayoutSend.setVisibility(View.VISIBLE);
                if (mRelativeLayoutSuccess.getVisibility() != View.GONE)
                    mRelativeLayoutSuccess.setVisibility(View.GONE);
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE) mErrorView.setVisibility(View.GONE);
            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mRelativeLayoutSend.getVisibility() != View.INVISIBLE)
                    mRelativeLayoutSend.setVisibility(View.INVISIBLE);
                if (mRelativeLayoutSuccess.getVisibility() != View.GONE)
                    mRelativeLayoutSuccess.setVisibility(View.GONE);
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE) mErrorView.setVisibility(View.GONE);
            }
            break;
            case ERROR_VISIBILITY: {
                if (mRelativeLayoutSend.getVisibility() != View.INVISIBLE)
                    mRelativeLayoutSend.setVisibility(View.INVISIBLE);
                if (mRelativeLayoutSuccess.getVisibility() != View.GONE)
                    mRelativeLayoutSuccess.setVisibility(View.GONE);
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);
            }
            break;
            case SUCCESS_VISIBILITY: {
                if (mRelativeLayoutSend.getVisibility() != View.GONE)
                    mRelativeLayoutSend.setVisibility(View.GONE);
                if (mRelativeLayoutSuccess.getVisibility() != View.VISIBLE)
                    mRelativeLayoutSuccess.setVisibility(View.VISIBLE);
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE) mErrorView.setVisibility(View.GONE);
            }
            break;
        }
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_send:
                sendVkmessage(mEditText.getText().toString(), mCheckBoxAttach.isChecked());
                break;

         /*   case R.id.button_send_auto:
                sendVkmessage(getString(R.string.auto_generated_message), true);
                break;*/

            case R.id.button_close:
                this.dismiss();
                break;
            case R.id.button_open_dialog:

                mStringBuilder.setLength(0);
                mStringBuilder.append("https://vk.com/im?sel=").append(mSellerId);
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mStringBuilder.toString()));
                this.startActivity(intent);

                break;
        }
    }


    private void sendVkmessage(String text, Boolean attach) {

        setVisibility(PROGRESS_VISIBILITY);

        mStringBuilder.setLength(0);


        //   Log.d(LOG_TAG, mSellerId + "");
        //   Log.d(LOG_TAG, mItemId);
        //   Log.d(LOG_TAG, mStringBuilder.append("market").append(mItemId).toString());

        if (attach) {
            mVkRequest = new VKRequest("messages.send", VKParameters.from("user_id", mSellerId, "message", text, "attachment", mStringBuilder.append("market").append(mItemId).toString()));
        } else {
            mVkRequest = new VKRequest("messages.send", VKParameters.from("user_id", mSellerId, "message", text));

        }

        mVkRequest.executeWithListener(new VkRequestListner(this));


    }


    @Override
    public void onDestroy() {
        super.onDestroy();

        if (mVkRequest != null) mVkRequest.cancel();
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        getDialog().setTitle(getString(R.string.send_message)); //onCreate bundle handler with current params is needed

        View v = inflater.inflate(R.layout.fragment_write_message, null);

        mErrorView = (FrameLayout) v.findViewById(R.id.error_view);
        mStringBuilder = new StringBuilder();
        mRelativeLayoutSuccess = (RelativeLayout) v.findViewById(R.id.relativelayout_message_success);

        mButtonOpenDialog = (Button) v.findViewById(R.id.button_open_dialog);
        mButtonClose = (Button) v.findViewById(R.id.button_close);

        mRelativeLayoutSend = (RelativeLayout) v.findViewById(R.id.relativelayout_send_message);
        mButtonSend = (Button) v.findViewById(R.id.button_send);
        //   mButtonSenAuto = (Button) v.findViewById(R.id.button_send_auto);
        mCheckBoxAttach = (CheckBox) v.findViewById(R.id.checkBox_attach_item);
        mEditText = (EditText) v.findViewById(R.id.edittext_message);
        mTextLabel = (TextView) v.findViewById(R.id.label_about);
        mProgressView = (ProgressView) v.findViewById(R.id.progressbar_writemessage);

        mButtonSend.setOnClickListener(this);
        //  mButtonSenAuto.setOnClickListener(this);
        mButtonOpenDialog.setOnClickListener(this);
        mButtonClose.setOnClickListener(this);

        Bundle bundle = this.getArguments();
        mItemId = bundle.getString(HansaApplication.ITEM_ID);
        mSellerId = bundle.getInt(HansaApplication.SELLER_ID);

        if (mSellerId == 0) {
            //   Toast.makeText(getActivity(), "Seller ID = 0", Toast.LENGTH_SHORT);
            this.dismiss();
        }

        setVisibility(START_VISIBILITY);

        return v;
    }

}
