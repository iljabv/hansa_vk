package com.ilja.vk_hansa.UI.Activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.Market.VkAlbumItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Adapters.AlbumsStoreAdapter;
import com.ilja.vk_hansa.UI.UIHelps.EndlessRecyclerOnScrollListener;
import com.ilja.vk_hansa.UI.UIHelps.SpacesItemDecoration;
import com.ilja.vk_hansa.Web.VkAPIFactory;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

public class StoreActivity extends AppCompatActivity {

    private final int START_VISIBILITY = 0;
    private final int PROGRESS_VISIBILITY = 1;
    private final int ERROR_VISIBILITY = 2;
    private final int SUCCESS_VISIBILITY = 3;
    private final int NOT_FOUND_VISIBILITY = 4;
    private final int ALL_INVISIBLE = 5;
    private final int PART_PROGRESS_VISIBILITY = 6;

    private final int PAGE_COUNT = 10;
    private final String LOG_TAG = "Store Activity";

    private FrameLayout mErrorView;
    private TextView mErrorText;
    private com.rey.material.widget.Button mErrorButton;
    private ImageView mImageError;

    private TextView mTextStoreName;
    private TextView mTextStoreDesc;
    private TextView mTextStoreStatus;
    private TextView mTextMembersCount;
    private TextView mTextPrices;
    private Button mButtonMessages;
    private Button mButtonJoin;
    private List<VkAlbumItem> mAlbumsList;
    private List<VkItem> mLastItemsList;
    private RecyclerView mRecyclerView;
    private GridLayoutManager mGridLayoutManager;
    private ImageView mImageStorePhoto;
    private int mCurStorID;
    private VkStoreItem mCurrentStore;
    private VkAPIFactory mVkApiFactory;
    private AlbumsStoreAdapter mAlbumAdapter;
    private EndlessRecyclerOnScrollListener mEndlessScrollListener;
    private int maxAlbums;
    private View errorView;

    private VKRequest mVkRequest;
    private ProgressView mProgressView;
    private int mItemsCount;
    private int mScrollOffset;
    private boolean isDataCached = false;
    private List<VkItem> mPopularItemsList;

    private static class VkItemsRequestListner extends VKRequest.VKRequestListener {

        private final int STORE_REQUEST = 0;
        private final int LASTITEMS_REQUEST = 1;
        private final int ALBUMS_REQUEST = 2;
        private final int POPULARITEMS_REQUEST = 3;

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<StoreActivity> mWeakRefParent;
        private boolean mReset; //used only with getAlbums
        private VkAPIFactory mVkApiFactory;
        private int mResponseType;  // 0 - getStore, 1 - getLastItems, 2 - getAlbums

        private VkItemsRequestListner(StoreActivity activity, int response_type) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mVkApiFactory = new VkAPIFactory();
            this.mResponseType = response_type;
        }


        private VkItemsRequestListner(StoreActivity activity, int response_type, boolean reset) {
            this.mWeakRefParent = new WeakReference<>(activity);
            this.mVkApiFactory = new VkAPIFactory();
            this.mReset = reset;
            this.mResponseType = response_type;
        }

        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);
            if (mWeakRefParent.get() != null) {

                switch (mResponseType) {
                    case STORE_REQUEST:

                        mWeakRefParent.get().onStoreLoaded(mVkApiFactory.getStore(response));
                        break;
                    case LASTITEMS_REQUEST:
                        mWeakRefParent.get().onLastItemsLoaded(mVkApiFactory.getItemList(response), mVkApiFactory.getItemCount(response));
                        break;
                    case POPULARITEMS_REQUEST:
                        mWeakRefParent.get().onPopularItemsLoaded(mVkApiFactory.getItemList(response), mVkApiFactory.getItemCount(response));
                        break;
                    case ALBUMS_REQUEST:
                        mWeakRefParent.get().onAlbumsLoaded(mVkApiFactory.getAlbumsList(response), mVkApiFactory.getItemCount(response), mReset);
                        break;
                    default:
                        //  Log.d(TAG, "Unknown response type");
                }
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            //    Log.d(TAG, "Attempt failed: " + attemptNumber + " " + totalAttempts);


            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            // Log.d(TAG, "Error " + error.errorMessage);


            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onPopularItemsLoaded(List<VkItem> itemList, int itemCount) {
        if (itemList != null) {
            if (itemList.size() < 1) {

            } else {

                mPopularItemsList.clear();
                mPopularItemsList.addAll(itemList);
               // mItemsCount = itemCount;
                updateAlbumsRecyclerView();
            }

        }
    }

    private void onAlbumsLoaded(List<VkAlbumItem> albumsList, int itemCount, boolean reset) {

        if (albumsList != null) {
            maxAlbums = itemCount;


            if (albumsList.size() < 1) {

            } else {

                if (reset) {
                    mAlbumsList.clear();
                    mEndlessScrollListener = new EndlessRecyclerOnScrollListener(mGridLayoutManager) {
                        @Override
                        public void onLoadMore(int current_page) {

                            getAlbums(current_page, false);
                        }
                    };

                    mRecyclerView.addOnScrollListener(mEndlessScrollListener);
                }
                mAlbumsList.addAll(albumsList);
                updateAlbumsRecyclerView();


            }
        }
    }

    private void onLastItemsLoaded(List<VkItem> itemList, int itemCount) {
        if (itemList != null) {
            if (itemList.size() < 1) {

            } else {

                mLastItemsList.clear();
                mLastItemsList.addAll(itemList);
                mItemsCount = itemCount;
                updateAlbumsRecyclerView();
            }

        }

    }


    private void onLoadError(boolean b) {
        // Toast.makeText(getApplicationContext(), "Error", Toast.LENGTH_SHORT);
        setVisibility(ERROR_VISIBILITY);
    }

    private void onStoreLoaded(VkStoreItem store) {
        if (store != null) {


            mCurrentStore = store;
            getSupportActionBar().setTitle(mCurrentStore.getName());

            resetRecyclerView();
            getLastItems(1);
          //  getLastItems(3);
            getAlbums(1, true);

            setVisibility(SUCCESS_VISIBILITY);
        }


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_store, menu);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_search_store:
                if (mCurStorID != 0) {
                    Intent intent = new Intent(getApplicationContext(), VKSearchActivity.class);
                    Bundle extras = new Bundle();
                    extras.putBoolean("search_type", false);
                    //extras.putInt("album_id", album_id); Log.d("Search", "album: " + Integer.toString(album_id));
                    extras.putInt("group_id", mCurStorID);
                    //   Log.d("Search", "group: " + Integer.toString(mCurStorID));

                    intent.putExtras(extras);
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
                return true;
            case R.id.action_home_store:
                Intent intent = new Intent(getApplicationContext(), HomeActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
                return true;
            case R.id.action_refresh_store:
                getStore();
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }


    @Override
    protected void onDestroy() {
        super.onDestroy();

        if (mVkRequest != null) mVkRequest.cancel();
    }


    @Override
    public void onPause() {
        super.onPause();
        setVisibility(ALL_INVISIBLE);
        mScrollOffset = mGridLayoutManager.findFirstVisibleItemPosition();
        mAlbumAdapter = null;

    }


    @Override
    public void onResume() {
        super.onResume();
        if (mAlbumsList != null && mLastItemsList != null && mCurrentStore != null) {

            setVisibility(SUCCESS_VISIBILITY);
            mAlbumAdapter = mAlbumAdapter = new AlbumsStoreAdapter(mCurrentStore, mLastItemsList,mPopularItemsList, mAlbumsList, getApplicationContext(), mItemsCount);
            mRecyclerView.setAdapter(mAlbumAdapter);

            mRecyclerView.scrollToPosition(mScrollOffset);
        }
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_store);

        maxAlbums = 10;
        mVkApiFactory = new VkAPIFactory();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle(" ");
        mLastItemsList = new ArrayList<>();
        mPopularItemsList = new ArrayList<>();
        mAlbumsList = new ArrayList<>();

        Intent intent = getIntent();
        mCurStorID = intent.getIntExtra(HansaApplication.ID, 0);
        mProgressView = (ProgressView) findViewById(R.id.progressbar_store);
        mRecyclerView = (RecyclerView) findViewById(R.id.recyclerview_albums);

        mErrorView = (FrameLayout) findViewById(R.id.error_view);
        mErrorButton = (com.rey.material.widget.Button) findViewById(R.id.button_error);
        mErrorText = (TextView) findViewById(R.id.text_error);
        mImageError = (ImageView) findViewById(R.id.image_error);

        //   mRecyclerView.setAdapter(mAlbumAdapter);

        //     resetRecyclerView();
        getStore();

    }

    private void getAlbums(int current_page, boolean reset) {


        int offset = (current_page - 1) * PAGE_COUNT;

        if (maxAlbums < offset && !reset) {
            //  Log.d(LOG_TAG, "All albums were returned ");
            return;
        }



        mVkRequest = new VKRequest("market.getAlbums", VKParameters.from(VKApiConst.OWNER_ID, -mCurStorID, VKApiConst.COUNT, 10, VKApiConst.OFFSET, offset));

        mVkRequest.executeWithListener(new VkItemsRequestListner(this, 2, reset));

    }


    void getStore() {


        setVisibility(PROGRESS_VISIBILITY);

        mVkRequest = new VKRequest("groups.getById", VKParameters.from(VKApiConst.GROUP_ID, mCurStorID, VKApiConst.FIELDS, "place,status,description,members_count,verified,site,market"));

        mVkRequest.executeWithListener(new VkItemsRequestListner(this, 0));


    }


    void getLastItems(int type) {

        if(type==1) mVkRequest = new VKRequest("market.search", VKParameters.from(VKApiConst.OWNER_ID, -mCurStorID, VKApiConst.SORT, "1", VKApiConst.REV, "1", VKApiConst.COUNT, "20", VKApiConst.EXTENDED, "1"));
        else if(type==3) mVkRequest = new VKRequest("market.search", VKParameters.from(VKApiConst.OWNER_ID, -mCurStorID, VKApiConst.SORT, "3", VKApiConst.COUNT, "20", VKApiConst.EXTENDED, "1"));


        mVkRequest.executeWithListener(new VkItemsRequestListner(this, type));
    }


    void resetRecyclerView() {

        mRecyclerView.addItemDecoration(new SpacesItemDecoration(4, this));

        mGridLayoutManager = new GridLayoutManager(this, 2);
        mGridLayoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {

                switch (position) {
                    case 0:
                        return mGridLayoutManager.getSpanCount();
                    case 1:
                        return mGridLayoutManager.getSpanCount();
//                    case 2:
//                        return mGridLayoutManager.getSpanCount();
                    default:
                        return 1;
                }
//                return mAlbumAdapter.isHeader(position) ? mGridLayoutManager.getSpanCount() : 1;
            }
        });

        mRecyclerView.setLayoutManager(mGridLayoutManager);

        if (mCurrentStore != null) {
            mAlbumAdapter = new AlbumsStoreAdapter(mCurrentStore, mLastItemsList,mPopularItemsList, mAlbumsList, getApplicationContext(), mItemsCount);
            mRecyclerView.setAdapter(mAlbumAdapter);
        }


    }

    void updateAlbumsRecyclerView() {
        if (mAlbumAdapter != null) {
            mAlbumAdapter.notifyDataSetChanged();
            mAlbumAdapter.setItemsCount(mItemsCount);
        }
    }

    private void setVisibility(int i) {
        switch (i) {
            case START_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);

                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);

            }
            break;
            case PROGRESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.VISIBLE)
                    mProgressView.setVisibility(View.VISIBLE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case SUCCESS_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.VISIBLE)
                    mRecyclerView.setVisibility(View.VISIBLE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

            case ERROR_VISIBILITY: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.VISIBLE)
                    mErrorView.setVisibility(View.VISIBLE);

                mErrorText.setText(getString(R.string.error_load));
                mErrorButton.setVisibility(View.VISIBLE);
                Picasso.with(getApplicationContext()).load(R.drawable.vk_error_dog).into(mImageError);

                mErrorButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        getStore();
                    }
                });

            }
            break;

            case ALL_INVISIBLE: {
                if (mProgressView.getVisibility() != View.GONE)
                    mProgressView.setVisibility(View.GONE);
                if (mRecyclerView.getVisibility() != View.GONE)
                    mRecyclerView.setVisibility(View.GONE);
                if (mErrorView.getVisibility() != View.GONE)
                    mErrorView.setVisibility(View.GONE);
                mImageError.setImageResource(0);
                mErrorButton.setOnClickListener(null);
            }
            break;

        }
    }
}

