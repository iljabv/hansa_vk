package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.UI.CustomViews.ManageStoreCellView;

import java.util.List;

/**
 * Created by Ilja on 17.01.2016.
 */
public class ManagesStoresAdapter extends RecyclerView.Adapter<ManagesStoresAdapter.PersonViewHolder> {


    public static class PersonViewHolder extends RecyclerView.ViewHolder {
        PersonViewHolder(View itemView) {
            super(itemView);
        }

    }

    private VkStoreItem mCurrentStore;
    private List<VkStoreItem> mArrayStores;
    private ManageStoreCellView mManageStoreCellView;
    private Context mContext;
    private final String LOG_TAG = "MyStoresAdapter";

    public ManagesStoresAdapter(List<VkStoreItem> array_stores, Context context) {
        this.mArrayStores = array_stores;
        this.mContext = context;
    }

    @Override
    public void onAttachedToRecyclerView(RecyclerView recyclerView) {
        super.onAttachedToRecyclerView(recyclerView);
    }

    @Override
    public PersonViewHolder onCreateViewHolder(ViewGroup viewGroup, int i) {
        View v;
        PersonViewHolder pvh;

        v = new ManageStoreCellView(viewGroup.getContext());
        pvh = new PersonViewHolder(v);

        return pvh;
    }


    @Override
    public void onBindViewHolder(PersonViewHolder personViewHolder, int i) {


        mCurrentStore = mArrayStores.get(i);
        mManageStoreCellView = (ManageStoreCellView) personViewHolder.itemView;
        mManageStoreCellView.setStore(mCurrentStore);


    }


    @Override
    public int getItemCount() {
        return mArrayStores.size();
    }
}