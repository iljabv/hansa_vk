package com.ilja.vk_hansa.UI.UIHelps;

import com.ilja.vk_hansa.Model.Vk.VkPhotoItem;

/**
 * Created by Ilja on 11.02.2016.
 */
public class HelpGetExistingPhoto {

    public HelpGetExistingPhoto() {
    }

    public String getPhoto(VkPhotoItem item) {

        if (item.getPhoto604() != null) {
            return item.getPhoto604();
        } else if (item.getPhoto807() != null) {
            return item.getPhoto807();
        } else if (item.getPhoto1280() != null) {
            return item.getPhoto1280();
        } else if (item.getPhoto130() != null) {
            return item.getPhoto130();
        } else if (item.getPhoto75() != null) {
            return item.getPhoto75();
        } else if (item.getPhoto2560() != null) {
            return item.getPhoto2560();
        }

        return null;

    }

}
