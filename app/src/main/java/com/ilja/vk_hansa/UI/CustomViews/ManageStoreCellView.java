package com.ilja.vk_hansa.UI.CustomViews;

import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Handler;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.ilja.vk_hansa.Model.Hansa.HansaCrawlerResponse;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.StoreActivity;
import com.ilja.vk_hansa.UI.UIHelps.CircleTransform;
import com.ilja.vk_hansa.Web.IHansaAPI;
import com.rey.material.widget.ProgressView;
import com.squareup.picasso.Picasso;

import java.lang.ref.WeakReference;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.GsonConverterFactory;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * Created by Ilja on 07.02.2016.
 */
public class ManageStoreCellView extends CardView implements View.OnClickListener {
    private final String APIURL = "http://vkcrawler-tinymagic.rhcloud.com/";
    private final String IN_BASE = "IN_BASE";
    private final String NOT_FOUND = "NOT_FOUND";
    private final String PROCESSING = "PROCESSING";
    private final String SUCCESS = "success";
    private final String LOG_TAG = "ManageStoreCellView";

    private Call request;
    private RelativeLayout mRelativeLayout;
    private TextView mTextName;
    private TextView mTextMembers;
    private TextView mTextPrices;
    private TextView mTextAbout;
    private Button mButtonCall;
    private ImageView mImagePhoto;
    private ProgressView mProgressBar;
    private RelativeLayout mRelativeLayoutHansaControl;

    private Picasso mImageLoader;
    private IHansaAPI mRestApi;


    private StringBuilder mStringBuilder;

    private int mColorButton;
    private int mColorText;
    private Drawable mDrawableButton;
    private String mStrTextButton;
    private String mStrTextAbout;

    private boolean mEnabledButton;
    private Handler mHandler;

    /*optimized calls, cashing used resources in holder*/
    private final int colorPrimary;
    private final int colorWhite;
    private final int colorLowGray;
    private final int colorLightPrimary;
    private final Drawable drawableButtonOrange;
    private final Drawable drawableButtonLightOrange;
    private final Drawable drawableButtonWhite;

    private final String textRegistrationAbout;
    private final String textRegistration;
    private final String textInProgressAbout;
    private final String textInProgress;
    private final String textRegistredAbout;
    private final String textRegistred;
    private final String textClosedAbout;
    private final String textClosed;

    private VkStoreItem mCurrentStore;
    private int mStoreId;
    private int mCurrentStatus; //0 - Not found in DataBase; 1 - Proccessing; 2 - found in DataBase

    private static class HansaCallback implements Callback<HansaCrawlerResponse> {

        private final String TAG = "VkItemsRequestListner";
        private final WeakReference<ManageStoreCellView> mWeakRefParent;
        private boolean mResponseType;   //true - registration request, false - getstatus request

        private HansaCallback(ManageStoreCellView view, boolean response_type) {
            this.mWeakRefParent = new WeakReference<>(view);
            this.mResponseType = response_type;
        }

        @Override
        public void onResponse(Response<HansaCrawlerResponse> response) {

            if (mWeakRefParent.get() != null) {

                if (mResponseType) {
                    mWeakRefParent.get().onRegistrationResponseLoaded(response.body().getResponse().getMessage());
                } else {
                    mWeakRefParent.get().onStatusLoaded(response.body().getResponse().getMessage());
                }
            }
        }

        @Override
        public void onFailure(Throwable t) {
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }

        }
    }

    private static class AutoRefresh implements Runnable {

        WeakReference<ManageStoreCellView> mWeakRef;

        public AutoRefresh(ManageStoreCellView view) {
            this.mWeakRef = new WeakReference<ManageStoreCellView>(view);
        }

        @Override
        public void run() {

            if (mWeakRef.get() != null) {
                mWeakRef.get().mProgressBar.setVisibility(View.VISIBLE);
                mWeakRef.get().mRelativeLayoutHansaControl.setVisibility(View.INVISIBLE);
                mWeakRef.get().apiGetStoreStatus();
            }
        }
    }


    private void onStatusLoaded(String message) {

        mProgressBar.setVisibility(View.GONE);
        mRelativeLayoutHansaControl.setVisibility(View.VISIBLE);

        switch (message) {

            case NOT_FOUND:
                //  mCurrentStore.setmStatusRegistration(0);
                setmCurrentStatus(0);
                break;
            case IN_BASE:
                // mCurrentStore.setmStatusRegistration(2);
                setmCurrentStatus(2);
                break;
            case PROCESSING:
                // mCurrentStore.setmStatusRegistration(1);
                setmCurrentStatus(1);
                break;
            default:
                //mCurrentStore.setmStatusRegistration(-1);
                setmCurrentStatus(-1);
                break;
        }

    }

    //TODO handle error UI
    private void onLoadError(boolean b) {
        // Log.d(LOG_TAG, "Registration error");
    }

    private void onRegistrationResponseLoaded(String message) {

        if (message.equals(SUCCESS)) {
            //  Log.d(LOG_TAG, "Success, try get status " + mStoreId);
            apiGetStoreStatus();

            mHandler.postDelayed(new AutoRefresh(this), 15000);

        }
    }


    public ManageStoreCellView(Context context) {
        super(context);
        init(context);

        colorPrimary = getContext().getResources().getColor(R.color.colorPrimary);
        colorLightPrimary = getContext().getResources().getColor(R.color.primary_light);
        colorWhite = getContext().getResources().getColor(R.color.white);
        colorLowGray = getContext().getResources().getColor(R.color.gray300);
        drawableButtonOrange = getContext().getResources().getDrawable(R.drawable.rounded_button_orange);
        drawableButtonWhite = getContext().getResources().getDrawable(R.drawable.rounded_button_gray);
        drawableButtonLightOrange = getContext().getResources().getDrawable(R.drawable.rounded_button_lightorange);
        textRegistrationAbout = getContext().getString(R.string.registration_about);
        textRegistration = getContext().getString(R.string.registration);
        textInProgressAbout = getContext().getString(R.string.in_process_about);
        textInProgress = getContext().getString(R.string.in_process);
        textRegistredAbout = getContext().getString(R.string.registred_about);
        textRegistred = getContext().getString(R.string.registred);
        textClosedAbout = getContext().getString(R.string.store_closed_about);
        textClosed = getContext().getString(R.string.store_closed);

    }


    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (request != null && request.isExecuted()) {
            request.cancel();
            //    Log.d(LOG_TAG, "Requset stoped");
        }
        if (mHandler != null) {
            mHandler.removeCallbacksAndMessages(null);
        }


    }


    private void init(Context context) {

        inflate(getContext(), R.layout.item_mystores_manage, this);

        //  Log.d(LOG_TAG, "Init");
        this.mHandler = new Handler();
        this.mRelativeLayout = (RelativeLayout) findViewById(R.id.relativelayout_mystores);
        this.mTextName = (TextView) findViewById(R.id.text_comment_name);
        this.mTextMembers = (TextView) findViewById(R.id.text_comment_date);
        this.mTextAbout = (TextView) findViewById(R.id.text_about_registration);
        this.mButtonCall = (Button) findViewById(R.id.button_registration);
        this.mProgressBar = (ProgressView) findViewById(R.id.progressbar_managestores_hansacontrol);
        this.mRelativeLayoutHansaControl = (RelativeLayout) findViewById(R.id.relativelayout_managestore_hansacontrol);
        this.mImagePhoto = (ImageView) findViewById(R.id.image_comment_photo);

        Retrofit restAdapter = new Retrofit.Builder().baseUrl(APIURL).addConverterFactory(GsonConverterFactory.create()).build();

        mRestApi = restAdapter.create(IHansaAPI.class);

        mStringBuilder = new StringBuilder();


        this.mRelativeLayout.setOnClickListener(this);

        this.mButtonCall.setOnClickListener(this);

        this.setUseCompatPadding(true);
    }


    private void apiTryRegDelStore(int cur_status) {


        mProgressBar.setVisibility(View.VISIBLE);
        mRelativeLayoutHansaControl.setVisibility(View.INVISIBLE);

        switch (cur_status) {
            case 0:
                //  Log.d(LOG_TAG, "Try registration store" + mStoreId);
                mRestApi.regStore(mStoreId).enqueue(new HansaCallback(this, true));
                break;
            case 2:
                //   Log.d(LOG_TAG, "Try delete store" + mStoreId);
                mRestApi.deleteStore(mStoreId).enqueue(new HansaCallback(this, true));
                break;
            default:
                //  Log.d(LOG_TAG, "Unknown store status");
        }
    }

    public void setStore(VkStoreItem store) {
        //  Log.d(LOG_TAG, "Try set store: ");
        if (store != null) {
            // Log.d(LOG_TAG, "Set store: " + store.getId());
            mCurrentStore = store;
            mStoreId = store.getId();
            setmCurrentStatus(store.getmStatusRegistration());
            setmTextName(store.getName());
            setmTextMembers(store.getMembersCount());
            setmPhoto(store.getPhoto200());
        }
    }


    private void apiGetStoreStatus() {

        request = mRestApi.getStatusStore(mStoreId);

        request.enqueue(new HansaCallback(this, false));
    }

    //TODO: refactor style
    public void setmCurrentStatus(int status) {

        if (status == -1) return;  //TODO: dirty hack :(


        if (mProgressBar.getVisibility() != View.GONE) mProgressBar.setVisibility(View.GONE);
        if (mRelativeLayoutHansaControl.getVisibility() != View.VISIBLE)
            mRelativeLayoutHansaControl.setVisibility(View.VISIBLE);

        mCurrentStore.setmStatusRegistration(status);
        mCurrentStatus = status;

        if (mCurrentStore.getIsClosed() == 0) {
            switch (status) {
                case 0:
                    mColorButton = colorPrimary;
                    mColorText = colorWhite;
                    mEnabledButton = true;
                    mStrTextAbout = textRegistrationAbout;
                    mStrTextButton = textRegistration;
                    mDrawableButton = drawableButtonOrange;
                    break;
                case 1:
                    mColorButton = colorLightPrimary;
                    mColorText = colorWhite;
                    mEnabledButton = false;
                    mStrTextAbout = textInProgressAbout;
                    mStrTextButton = textInProgress;
                    mDrawableButton = drawableButtonLightOrange;
                    break;
                case 2:
                    mColorButton = colorLowGray;
                    mColorText = colorPrimary;
                    mEnabledButton = true;
                    mStrTextAbout = textRegistredAbout;
                    mStrTextButton = textRegistred;
                    mDrawableButton = drawableButtonWhite;
                    break;
                default:
                    mColorButton = colorPrimary;
                    mColorText = colorWhite;
                    mEnabledButton = true;
                    mStrTextAbout = textRegistrationAbout;
                    mStrTextButton = textRegistration;
                    mDrawableButton = drawableButtonOrange;
                    break;
            }
        } else {
            mColorButton = colorLowGray;
            mColorText = colorLightPrimary;
            mEnabledButton = false;
            mStrTextAbout = textClosedAbout;
            mStrTextButton = textClosed;
            mDrawableButton = drawableButtonWhite;
        }
        mButtonCall.setBackgroundDrawable(mDrawableButton);
        //    mButtonCall.setBackgroundColor(mColorButton);

        mButtonCall.setTextColor(mColorText);
        mButtonCall.setEnabled(mEnabledButton);
        mButtonCall.setText(mStrTextButton);
        mTextAbout.setText(mStrTextAbout);

    }

    public void setmPhoto(String photo_url) {
        this.mImageLoader.with(getContext()).load(photo_url).transform(new CircleTransform(false, 0)).
                error(R.drawable.vk_error_dog).into(mImagePhoto);
    }


    public void setmTextName(String name) {

        this.mTextName.setText(name);
    }

    public void setmTextMembers(int members) {
        mStringBuilder.setLength(0);
        this.mTextMembers.setText(mStringBuilder.append(members).append(" ").append(getContext().getResources().getQuantityString(R.plurals.members, members)));
    }

//    public void setmTextPrices(VkMarketItem market) {
//        mStringBuilder.setLength(0);
//        this.mTextPrices.setText(mStringBuilder.append(getContext().getString(R.string.prices)).append(": ").append(market.getPriceMin()).append(" ").append(market.getPriceMax()).append(" ").append(market.getCurrency().getName()));
//    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_registration:

                if (mCurrentStore != null) {
                    apiTryRegDelStore(mCurrentStatus);
                }
                break;
            case R.id.relativelayout_mystores:
                if (mCurrentStore != null) {
                    // Log.d(LOG_TAG, "Open store activity, id:" + mCurrentStore.getId());
                    Intent intent = new Intent(getContext(), StoreActivity.class);

                    intent.putExtra("id", mCurrentStore.getId());
                    getContext().startActivity(intent);
                }

                break;

        }
    }
}
