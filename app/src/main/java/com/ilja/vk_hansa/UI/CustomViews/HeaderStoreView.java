package com.ilja.vk_hansa.UI.CustomViews;

import android.animation.ObjectAnimator;
import android.content.Context;
import android.content.Intent;
import android.support.v4.widget.TextViewCompat;
import android.support.v7.widget.CardView;
import android.text.Layout;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.ilja.vk_hansa.HansaApplication;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.ilja.vk_hansa.R;
import com.ilja.vk_hansa.UI.Activity.ExtraInfoStoreActivity;
import com.ilja.vk_hansa.UI.UIHelps.CircleTransform;
import com.squareup.picasso.Picasso;
import com.vk.sdk.api.VKApiConst;
import com.vk.sdk.api.VKError;
import com.vk.sdk.api.VKParameters;
import com.vk.sdk.api.VKRequest;
import com.vk.sdk.api.VKResponse;

import java.lang.ref.WeakReference;

/**
 * Created by Ilja on 13.02.2016.
 */
public class HeaderStoreView extends CardView implements View.OnClickListener {

    //const
    private final String LOG_TAG = "HeaderStoreView";
    private final String textMembers;
    private final String textLeave;
    private final String textSubscribe;
    private final String textExtraInfo;
    private final String textMessage;


    //UI
    private TextView mTextStoreName;
    private TextView mTextStoreDesc;
    private TextView mTextStoreStatus;
    private TextView mTextMembersCount;
    private TextView mTextPrices;
    private Button mButtonMessages;
    private Button mButtonJoin;
    private ImageView mImageStorePhoto;

    private Button mButtonExtraInfo;


    //UI help
    private StringBuilder mStringBuilder;
    private Picasso mImageLoader;

    //
    private int mStoreId;
    private VkStoreItem mStore;
    private VKRequest mVkRequest;
    private TextView mTextItemsCount;


    private static class VkStoreSubscribeLeaveListner extends VKRequest.VKRequestListener {

        private final String TAG = "VkStoreSubscribeLeaveListner";
        private final WeakReference<HeaderStoreView> mWeakRefParent;
        boolean mIfSubscribe;
        // private VkAPIFactory mVkApiFactory;

        private VkStoreSubscribeLeaveListner(HeaderStoreView view, boolean is_subcsribe) {
            this.mWeakRefParent = new WeakReference<>(view);
            this.mIfSubscribe = is_subcsribe;
            //   this.mVkApiFactory = new VkAPIFactory();
        }


        @Override
        public void onComplete(VKResponse response) {
            super.onComplete(response);

            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onSubscribeLeaveCompleted(mIfSubscribe);
            }


        }

        @Override
        public void attemptFailed(VKRequest request, int attemptNumber, int totalAttempts) {
            super.attemptFailed(request, attemptNumber, totalAttempts);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(false);
            }
        }

        @Override
        public void onError(VKError error) {
            super.onError(error);
            if (mWeakRefParent.get() != null) {
                mWeakRefParent.get().onLoadError(true);
            }
        }

        @Override
        public void onProgress(VKRequest.VKProgressType progressType, long bytesLoaded, long bytesTotal) {
            super.onProgress(progressType, bytesLoaded, bytesTotal);
        }


    }

    private void onLoadError(boolean b) {
        //  Toast.makeText(getContext(),"Error",Toast.LENGTH_SHORT);
    }

    private void onSubscribeLeaveCompleted(boolean if_subscribe) {

        if (if_subscribe) {
            mStore.setIsMember(1);
            setButtonSubscribe(false);
        } else {
            mStore.setIsMember(0);
            setButtonSubscribe(true);
        }
    }


    public HeaderStoreView(Context context) {
        super(context);


        textMembers = getContext().getString(R.string.members);
        textLeave = getContext().getString(R.string.leave);
        textSubscribe = getContext().getString(R.string.subscribe);
        textExtraInfo = getContext().getString(R.string.conditions);
        textMessage = getContext().getString(R.string.message);
        init();
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (mVkRequest != null) mVkRequest.cancel();
    }

    private void init() {

        inflate(getContext(), R.layout.item_store_header, this);

        //  Log.d(LOG_TAG, "Init");
        mStoreId = 0;
        mStringBuilder = new StringBuilder();

        mTextStoreName = (TextView) findViewById(R.id.text_comment_name);
        mTextStoreDesc = (TextView) findViewById(R.id.text_store_description);
        mTextStoreStatus = (TextView) findViewById(R.id.text_store_status);
        mTextMembersCount = (TextView) findViewById(R.id.text_comment_date);
        mButtonJoin = (Button) findViewById(R.id.button_store_join);
        mImageStorePhoto = (ImageView) findViewById(R.id.image_comment_photo);
        mTextItemsCount = (TextView) findViewById(R.id.text_store_itemscount);

        mButtonExtraInfo = (Button) findViewById(R.id.button_store_extrainfo);
        mButtonExtraInfo.setOnClickListener(this);

        mButtonJoin.setOnClickListener(this);
    }

    private void setSubscribeLeave(boolean isSubscribe) {


        if (isSubscribe)
            mVkRequest = new VKRequest("groups.join", VKParameters.from(VKApiConst.GROUP_ID, mStoreId));
        else
            mVkRequest = new VKRequest("groups.leave", VKParameters.from(VKApiConst.GROUP_ID, mStoreId));


        mVkRequest.executeWithListener(new VkStoreSubscribeLeaveListner(this, isSubscribe));
    }

    public void setStore(VkStoreItem store, int itemscount) {
        if (store != null) {

            mStore = store;
            mStringBuilder.setLength(0);

            mImageLoader.with(getContext()).load(store.getPhoto200()).transform(new CircleTransform(false, 0)).
                    error(R.drawable.vk_error_dog).into(mImageStorePhoto);

            mTextStoreName.setText(store.getName());
            mTextStoreStatus.setText(store.getStatus());

            if (store.getDescription() != null && !store.getDescription().isEmpty()) {
                mTextStoreDesc.setText(store.getDescription());
            } else {
                mTextStoreDesc.setText(getResources().getString(R.string.no_description));
            }
            mTextMembersCount.setText(mStringBuilder.append(store.getMembersCount()));
            mStringBuilder.setLength(0);
            mTextItemsCount.setText(mStringBuilder.append(itemscount));


            if (store.getIsMember() == 0) setButtonSubscribe(true);
            else setButtonSubscribe(false);

            mButtonExtraInfo.setText(textExtraInfo);

            if (store.getMarket().getWiki() == null) {
                mButtonExtraInfo.setEnabled(false);
                mButtonExtraInfo.setVisibility(View.GONE);
            }

            mStoreId = store.getId();

            ViewTreeObserver vto = mTextStoreDesc.getViewTreeObserver();
            vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override
                public void onGlobalLayout() {
                    Layout l = mTextStoreDesc.getLayout();
                    if (l != null) {
                        int lines = l.getLineCount();
                        if (lines > 0) {
                            setDescriptionTextviewExpandable();
                            mTextStoreDesc.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                        }
                    }
                }
            });
        }
    }

    private void setButtonSubscribe(boolean isSubscribe) {
        if (isSubscribe) mButtonJoin.setText(textSubscribe);
        else mButtonJoin.setText(textLeave);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.button_store_extrainfo:
                if (mStore != null && mStore.getMarket() != null && mStore.getMarket().getWiki() != null) {
                    Intent intent = new Intent(getContext(), ExtraInfoStoreActivity.class);

                    intent.putExtra(HansaApplication.WIKI_URL, mStore.getMarket().getWiki().getViewUrl());

                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);

                    getContext().startActivity(intent);

                }

                break;
            case R.id.button_store_join:
                if (mStoreId != 0) {
                    if (mStore.getIsMember() == 0) {
                        setSubscribeLeave(true);
                    } else {
                        setSubscribeLeave(false);
                    }
                }
                break;

        }
    }

    public void setDescriptionTextviewExpandable() {
        if (TextViewCompat.getMaxLines(mTextStoreDesc) < mTextStoreDesc.getLineCount()) {
            mTextStoreDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_keyboard_arrow_down_orange_36dp);

            mTextStoreDesc.setOnClickListener(new OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (TextViewCompat.getMaxLines(mTextStoreDesc) < mTextStoreDesc.getLineCount()) {
                        ObjectAnimator animation = ObjectAnimator.ofInt(mTextStoreDesc, "maxLines", mTextStoreDesc.getLineCount());
                        animation.setDuration(200).start();

                        mTextStoreDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_keyboard_arrow_up_orange_36dp);
                    } else {
                        ObjectAnimator animation = ObjectAnimator.ofInt(mTextStoreDesc, "maxLines", 5);
                        animation.setDuration(200).start();

                        mTextStoreDesc.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.ic_keyboard_arrow_down_orange_36dp);
                    }
                }
            });
        }
    }
}
