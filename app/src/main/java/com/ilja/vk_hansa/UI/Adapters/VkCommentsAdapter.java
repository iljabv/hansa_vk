package com.ilja.vk_hansa.UI.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.ViewGroup;

import com.ilja.vk_hansa.Model.Vk.Market.Comments.AdaptiveVkComments;
import com.ilja.vk_hansa.UI.CustomViews.CommentView;

import java.util.List;

/**
 * Created by Ilja on 20.03.2016.
 */
public class VkCommentsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {


    private static class VkCommentHolder extends RecyclerView.ViewHolder {

        public VkCommentHolder(View itemView) {
            super(itemView);

        }
    }

    private static final String LOG_TAG = "VKCommentsdapter";
    private CommentView mCommentView;
    private List<AdaptiveVkComments> mCommentList;
    private Context mContext;


    public VkCommentsAdapter(Context context, List<AdaptiveVkComments> itemList) {
        this.mCommentList = itemList;
        this.mContext = context;

    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View v;
        VkCommentHolder pvh;

        v = new CommentView(mContext);
        pvh = new VkCommentHolder(v);
        return pvh;

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        if (mCommentList != null) {

            mCommentView = (CommentView) holder.itemView;
            mCommentView.setAddaptiveComment(mCommentList.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return this.mCommentList.size();
    }
}