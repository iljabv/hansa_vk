package com.ilja.vk_hansa.Web;

import java.util.HashMap;

/**
 * Created by Ilja on 24.01.2016.
 */
public class HansaParamsApi {

    public HansaParamsApi() {
    }

    private String mSort;  // {0,1,2,3}; set -1 for not used
    private String mSearch; //text query; set null for not used

    public HansaParamsApi setmSort(int mSort) {
        this.mSort = String.valueOf(mSort);
        return this;
    }

    public HansaParamsApi setmSearch(String mSearch) {
        this.mSearch = mSearch;
        return this;
    }

    public HansaParamsApi setmMaxprice(int mMaxprice) {
        this.mMaxprice = String.valueOf(mMaxprice);
        return this;
    }

    public HansaParamsApi setmMinprice(int mMinprice) {
        this.mMinprice = String.valueOf(mMinprice);
        return this;
    }

    public HansaParamsApi setmMpage(int mMpage) {
        this.mMpage = String.valueOf(mMpage);
        return this;
    }

    public HansaParamsApi setmAfterdate(int mAfterdate) {
        this.mAfterdate = String.valueOf(mAfterdate);
        return this;
    }

    public HansaParamsApi setmCatid(int mCatid) {
        this.mCatid = String.valueOf(mCatid);
        return this;
    }

    public HansaParamsApi setmRev(int mRev) {
        this.mRev = String.valueOf(mRev);
        return this;
    }

    private String mMaxprice; // max price; set -1 for not used
    private String mMinprice; //min price; set -1 for not used
    private String mMpage;  // page; set -1 for not used
    private String mAfterdate; // Data; set -1 for not used;
    private String mCatid;  //Category ID; set -1 for not used
    private String mRev;

    public HansaParamsApi(int sort, String search, int maxprice, int minprice, int page, int afterdate, int catid, int rev) {


        this.mSort = sort == -1 ? null : String.valueOf(sort);
        this.mSearch = search;
        this.mMaxprice = maxprice == -1 ? null : String.valueOf(maxprice);
        this.mMinprice = minprice == -1 ? null : String.valueOf(minprice);
        this.mMpage = page == -1 ? null : String.valueOf(page);
        this.mAfterdate = afterdate == -1 ? null : String.valueOf(afterdate);
        this.mCatid = catid == -1 ? null : String.valueOf(catid);
        this.mRev = rev == -1 ? null : String.valueOf(rev);
    }

    public HansaParamsApi(String sort, String search, String maxprice, String minprice, String page, String afterdate, String catid, String rev) {
        this.mSort = sort;
        this.mSearch = search;
        this.mMaxprice = maxprice;
        this.mMinprice = minprice;
        this.mMpage = page;
        this.mAfterdate = afterdate;
        this.mCatid = catid;
        this.mRev = rev;
    }

    public HashMap<String, String> GetMap() {
        HashMap<String, String> map = new HashMap<String, String>();
        if (mSort != null) {
            map.put("sort", mSort);
        }
        if (mSearch != null) {
            map.put("search", mSearch);
        }
        if (mMaxprice != null) {
            map.put("maxprice", mMaxprice);
        }
        if (mMinprice != null) {
            map.put("minprice", mMinprice);
        }
        if (mMpage != null) {
            map.put("page", mMpage);
        }
        if (mAfterdate != null) {
            map.put("afterdate", mAfterdate);
        }
        if (mCatid != null) {
            map.put("catid", mCatid);
        }
        if (mRev != null) {
            map.put("rev", mRev);
        }

        return map;
    }

}
