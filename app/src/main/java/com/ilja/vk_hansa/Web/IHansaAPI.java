package com.ilja.vk_hansa.Web;

import com.ilja.vk_hansa.Model.Hansa.HansaCrawlerResponse;
import com.ilja.vk_hansa.Model.Hansa.HansaSearchResponse;

import java.util.Map;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.QueryMap;

/**
 * Methods regmarket and status use only with VkCrawler URL
 * Method dataset use with ApiHansa URL
 */


public interface IHansaAPI {

    @GET("regmarket")
    Call<HansaCrawlerResponse> regStore(@Query("id") int store_id);

    @GET("status")
    Call<HansaCrawlerResponse> getStatusStore(@Query("id") int store_id);

    @GET("statuses")
    Call<HansaCrawlerResponse> getStatusesStores(@Query("ids") String ids);

    @GET("delmarket")
    Call<HansaCrawlerResponse> deleteStore(@Query("id") int store_id);

    @GET("datasetshort")
    Call<HansaSearchResponse> getItems(@Query("page") int page, @Query("search") String str, @Query("sort") int sort);

    @GET("datasetshort")
    Call<HansaSearchResponse> getItemsWithParams(@QueryMap Map<String, String> params);
}
