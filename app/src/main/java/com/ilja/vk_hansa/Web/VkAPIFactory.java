package com.ilja.vk_hansa.Web;

import com.google.gson.Gson;
import com.ilja.vk_hansa.Model.Vk.Market.Comments.AdaptiveVkComments;
import com.ilja.vk_hansa.Model.Vk.Market.Comments.VkCommentItem;
import com.ilja.vk_hansa.Model.Vk.Market.Comments.VkResponseComments;
import com.ilja.vk_hansa.Model.Vk.Market.VkAlbumItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.Model.Vk.VkResponseAlbum;
import com.ilja.vk_hansa.Model.Vk.VkStoreItem;
import com.vk.sdk.api.VKResponse;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 26.01.2016.
 */

public class VkAPIFactory {

    private Gson gsonFactory;
    private final String LOG_TAG = "VkAPIResolver";

    public VkAPIFactory() {
        gsonFactory = new Gson();
    }

    public VkStoreItem getStore(VKResponse response) {
        VkStoreItem curStore = null;

        try {
            JSONObject jsonStore = response.json.getJSONArray("response").getJSONObject(0);

            curStore = gsonFactory.fromJson(jsonStore.toString(), VkStoreItem.class);
            curStore.setmStatusRegistration(0);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return curStore;

    }


    public VkStoreItem getStore(JSONObject jsonStore) {
        VkStoreItem curStore;


        curStore = gsonFactory.fromJson(jsonStore.toString(), VkStoreItem.class);
        curStore.setmStatusRegistration(0);


        return curStore;

    }

    public List<VkStoreItem> getStoreAdminList(VKResponse response) {
        List<VkStoreItem> listVkStores = new ArrayList<>();

        try {
            JSONObject jsonResponse = response.json.getJSONObject("response");
            JSONArray arrayResponse = jsonResponse.getJSONArray("items");
            for (int i = 0; i < arrayResponse.length(); i++) {

                JSONObject jsonStore = arrayResponse.getJSONObject(i);
                JSONObject jsonMarket = jsonStore.getJSONObject("market");
                if (jsonStore.getInt("is_admin") == 1 && jsonMarket.getInt("enabled") == 1) {

                    listVkStores.add(getStore(jsonStore));

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listVkStores;
    }


    public List<VkStoreItem> getStoreList(VKResponse response) {
        List<VkStoreItem> listVkStores = new ArrayList<>();

        try {
            JSONObject jsonResponse = response.json.getJSONObject("response");
            JSONArray arrayResponse = jsonResponse.getJSONArray("items");
            for (int i = 0; i < arrayResponse.length(); i++) {

                JSONObject jsonStore = arrayResponse.getJSONObject(i);
                JSONObject jsonMarket = jsonStore.getJSONObject("market");
                if (jsonMarket.getInt("enabled") == 1) {

                    listVkStores.add(getStore(jsonStore));

                }

            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return listVkStores;
    }


    public VkItem getItem(VKResponse response) {
        VkItem currentItem = new VkItem();
        try {
            JSONObject jsonItem = response.json.getJSONObject("response").getJSONArray("items").getJSONObject(0);

            currentItem = gsonFactory.fromJson(jsonItem.toString(), VkItem.class);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return currentItem;
    }

    public int getItemCount(VKResponse response) {
        int result;

        try {
            JSONObject jsonItem = response.json.getJSONObject("response");
            result = jsonItem.getInt("count");

            // Log.d("VKSearchActivity", Integer.toString(result));

        } catch (JSONException e) {
            e.printStackTrace();
            // Log.d("VKSearchActivity", "Error");
            result = -1;
        }
        return result;
    }

    public VkItem getItem(JSONObject jsonStore) {
        VkItem curItem;


        curItem = gsonFactory.fromJson(jsonStore.toString(), VkItem.class);

        return curItem;

    }

    public List<VkItem> getItemList(VKResponse response) {
        List<VkItem> itemList = new ArrayList<>();
        //Log.d(LOG_TAG, "START PARSE");
        try {
            JSONObject jsonResponse = response.json.getJSONObject("response");
            JSONArray arrayResponse = jsonResponse.getJSONArray("items");
            for (int i = 0; i < arrayResponse.length(); i++) {

                JSONObject jsonStore = arrayResponse.getJSONObject(i);
                //JSONObject jsonMarket = jsonStore.getJSONObject("item");
                itemList.add(getItem(jsonStore));
                //Log.d(LOG_TAG, getItem(jsonStore).getTitle());

            }
        } catch (JSONException e) {
            e.printStackTrace();
            //Log.d(LOG_TAG, e.getMessage());
        }

        return itemList;
    }

    public List<VkAlbumItem> getAlbumsList(VKResponse response) {

        List<VkAlbumItem> listAlbums = new ArrayList<>();
        try {

            JSONObject jsonResponse = response.json.getJSONObject("response");

            VkResponseAlbum rAlbums = gsonFactory.fromJson(jsonResponse.toString(), VkResponseAlbum.class);

            //Log.d(LOG_TAG,rAlbums.getCount().toString());

            listAlbums = rAlbums.getItems();

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return listAlbums;

    }

    public List<AdaptiveVkComments> getAdaptiveComments(VKResponse response) {
        List<AdaptiveVkComments> resultCommentList = new ArrayList<>();

        try {
            StringBuilder mStringBuilder = new StringBuilder();
            JSONObject jsonResponse = response.json.getJSONObject("response");

            VkResponseComments vkCommentList = gsonFactory.fromJson(jsonResponse.toString(), VkResponseComments.class);

            /*TODO: edit this shit:*/
            vkCommentList.setGroups(vkCommentList.getGroups());
            vkCommentList.setProfiles(vkCommentList.getProfiles());

            for (VkCommentItem vkComment : vkCommentList.getItems()) {

                AdaptiveVkComments adaptComment = new AdaptiveVkComments();

                adaptComment.setText(vkComment.getText());
                adaptComment.setLikes(vkComment.getLikes());
                adaptComment.setUnix_date(vkComment.getDate());
                adaptComment.setStringDateFromUnix(vkComment.getDate());
                adaptComment.setOwner_id(vkComment.getFromId());

                //  Log.d(LOG_TAG, vkComment.getFromId() + " size: " + vkCommentList.getHash_profiles().size());
                //  Log.d(LOG_TAG,vkComment.getFromId()+" size: "+vkCommentList.getHash_groups().size());

                if (vkCommentList.getHash_profiles().get(vkComment.getFromId()) != null) {
                    mStringBuilder.setLength(0);
                    mStringBuilder.append(vkCommentList.getHash_profiles().get(vkComment.getFromId()).getFirstName()).append(" ").append(vkCommentList.getHash_profiles().get(vkComment.getFromId()).getLastName());
                    adaptComment.setOwner_name(mStringBuilder.toString());
                    adaptComment.setOwner_photo_url(vkCommentList.getHash_profiles().get(vkComment.getFromId()).getPhoto100());


                } else if (vkCommentList.getHash_groups().get(vkComment.getFromId()) != null) {

                    adaptComment.setOwner_name(vkCommentList.getHash_groups().get(vkComment.getFromId()).getName());
                    adaptComment.setOwner_photo_url(vkCommentList.getHash_groups().get(vkComment.getFromId()).getPhoto100());
                }

                resultCommentList.add(adaptComment);
            }

        } catch (JSONException e1) {
            e1.printStackTrace();
        }


        return resultCommentList;
    }

    public String[] getAvatar(VKResponse response) {
        String[] result = new String[3];
        //Log.d("AVATAR", response.responseString);

        try {
            JSONArray jsonArray = response.json.getJSONArray("response");
            result[0] = jsonArray.getJSONObject(0).getString("first_name");
            result[1] = jsonArray.getJSONObject(0).getString("last_name");
            result[2] = jsonArray.getJSONObject(0).getString("photo_200_orig");

        } catch (JSONException e) {
            e.printStackTrace();
            //   Log.d("VKSearchActivity", "Error");
            //result = "";
        }

        //Log.d("AVATAR", result);
        return result;
    }
}
