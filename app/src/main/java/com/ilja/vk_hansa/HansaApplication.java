package com.ilja.vk_hansa;

import android.app.Application;
import android.content.SharedPreferences;
import android.content.res.Configuration;
import android.preference.PreferenceManager;

import com.vk.sdk.VKAccessToken;
import com.vk.sdk.VKAccessTokenTracker;
import com.vk.sdk.VKSdk;

/**
 * Created by Ilja on 07.01.2016.
 */
public class HansaApplication extends Application {
    public static final String STORE_ID = "store_id";
    public static final String SEARCH_QUERY = "search_query";
    public static final String POSITION_ID = "position_id";
    public static final String ID = "id";
    public static final String WIKI_URL = "wiki_url";

    public static String login;
    public static final int RECYCLERVIEW_SPACES = 4;
    public static final String FRAGMENT_ID = "fragment_id";
    public static final String ITEM_ID = "item_id";
    public static final String SELLER_ID = "SELLER_ID";
    public static final String OWNER_ID = "OWNER_ID";


    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }

    VKAccessTokenTracker vkAccessTokenTracker = new VKAccessTokenTracker() {
        @Override
        public void onVKAccessTokenChanged(VKAccessToken oldToken, VKAccessToken newToken) {
            if (newToken == null) {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(getString(R.string.login_check), Boolean.FALSE);
                edit.commit();
            } else {
                SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getBaseContext());
                SharedPreferences.Editor edit = prefs.edit();
                edit.putBoolean(getString(R.string.login_check), Boolean.TRUE);
                edit.commit();
            }
        }
    };

//    public static RefWatcher getRefWatcher(Context context) {
//        HansaApplication application = (HansaApplication) context.getApplicationContext();
//        return application.refWatcher;
//    }

    // private RefWatcher refWatcher;

    @Override
    public void onCreate() {
        super.onCreate();

        //  refWatcher = LeakCanary.install(this);
        vkAccessTokenTracker.startTracking();
        VKSdk.initialize(this.getApplicationContext());
    }


    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

}
