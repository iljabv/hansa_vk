package com.ilja.vk_hansa.Model.Vk.Market;

/**
 * Created by Ilja on 30.01.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ilja.vk_hansa.Model.Vk.VkPhotoItem;


public class VkAlbumItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("updated_time")
    @Expose
    private Integer updatedTime;
    @SerializedName("photo")
    @Expose
    private VkPhotoItem photo;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The ownerId
     */
    public Integer getOwnerId() {
        return ownerId;
    }

    /**
     * @param ownerId The owner_id
     */
    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The updatedTime
     */
    public Integer getUpdatedTime() {
        return updatedTime;
    }

    /**
     * @param updatedTime The updated_time
     */
    public void setUpdatedTime(Integer updatedTime) {
        this.updatedTime = updatedTime;
    }

    /**
     * @return The photo
     */
    public VkPhotoItem getPhoto() {
        return photo;
    }

    /**
     * @param photo The photo
     */
    public void setPhoto(VkPhotoItem photo) {
        this.photo = photo;
    }

}