package com.ilja.vk_hansa.Model.Hansa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 07.02.2016.
 */
public class HansaCrawlerResponseMessage {

    @SerializedName("items")
    @Expose
    private List<HansaCrawlerResponseItems> items = new ArrayList<>();
    @SerializedName("message")
    @Expose
    private String message;
    @SerializedName("code")
    @Expose
    private Integer code;


    /**
     * @return The items
     */
    public List<HansaCrawlerResponseItems> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<HansaCrawlerResponseItems> items) {
        this.items = items;
    }

    /**
     * @return The message
     */
    public String getMessage() {
        return message;
    }

    /**
     * @param message The message
     */
    public void setMessage(String message) {
        this.message = message;
    }

    /**
     * @return The code
     */
    public Integer getCode() {
        return code;
    }

    /**
     * @param code The code
     */
    public void setCode(Integer code) {
        this.code = code;
    }

}

