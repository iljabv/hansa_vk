package com.ilja.vk_hansa.Model.Hansa;

/**
 * Created by Ilja on 07.02.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class HansaCrawlerResponse {

    @SerializedName("response")
    @Expose
    private HansaCrawlerResponseMessage response;

    /**
     * @return The response
     */
    public HansaCrawlerResponseMessage getResponse() {
        return response;
    }

    /**
     * @param response The response
     */
    public void setResponse(HansaCrawlerResponseMessage response) {
        this.response = response;
    }


}
