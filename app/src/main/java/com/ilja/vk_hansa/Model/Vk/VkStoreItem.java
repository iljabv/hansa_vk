package com.ilja.vk_hansa.Model.Vk;

/**
 * Created by Ilja on 30.01.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ilja.vk_hansa.Model.Vk.Market.VkMarketItem;


public class VkStoreItem {


    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("screen_name")
    @Expose
    private String screenName;
    @SerializedName("is_closed")
    @Expose
    private Integer isClosed;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_admin")
    @Expose
    private Integer isAdmin;
    @SerializedName("is_member")
    @Expose
    private Integer isMember;
    @SerializedName("members_count")
    @Expose
    private Integer membersCount;
    @SerializedName("place")
    @Expose
    private VkPlaceItem place;
    @SerializedName("verified")
    @Expose
    private Integer verified;
    @SerializedName("site")
    @Expose
    private String site;
    @SerializedName("market")
    @Expose
    private VkMarketItem market;
    @SerializedName("photo_50")
    @Expose
    private String photo50;
    @SerializedName("photo_100")
    @Expose
    private String photo100;
    @SerializedName("photo_200")
    @Expose
    private String photo200;

    @SerializedName("description")
    @Expose
    private String description;

    @SerializedName("status")
    @Expose
    private String status;
    private int mStatusRegistration;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The screenName
     */
    public String getScreenName() {
        return screenName;
    }

    /**
     * @param screenName The screen_name
     */
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    /**
     * @return The isClosed
     */
    public Integer getIsClosed() {
        return isClosed;
    }

    /**
     * @param isClosed The is_closed
     */
    public void setIsClosed(Integer isClosed) {
        this.isClosed = isClosed;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The isAdmin
     */
    public Integer getIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin The is_admin
     */
    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * @return The isMember
     */
    public Integer getIsMember() {
        return isMember;
    }

    /**
     * @param isMember The is_member
     */
    public void setIsMember(Integer isMember) {
        this.isMember = isMember;
    }

    /**
     * @return The membersCount
     */
    public Integer getMembersCount() {
        return membersCount;
    }

    /**
     * @param membersCount The members_count
     */
    public void setMembersCount(Integer membersCount) {
        this.membersCount = membersCount;
    }

    /**
     * @return The place
     */
    public VkPlaceItem getPlace() {
        return place;
    }

    /**
     * @param place The place
     */
    public void setPlace(VkPlaceItem place) {
        this.place = place;
    }

    /**
     * @return The verified
     */
    public Integer getVerified() {
        return verified;
    }

    /**
     * @param verified The verified
     */
    public void setVerified(Integer verified) {
        this.verified = verified;
    }

    /**
     * @return The site
     */
    public String getSite() {
        return site;
    }

    /**
     * @param site The site
     */
    public void setSite(String site) {
        this.site = site;
    }

    /**
     * @return The market
     */
    public VkMarketItem getMarket() {
        return market;
    }

    /**
     * @param market The market
     */
    public void setMarket(VkMarketItem market) {
        this.market = market;
    }

    /**
     * @return The photo50
     */
    public String getPhoto50() {
        return photo50;
    }

    /**
     * @param photo50 The photo_50
     */
    public void setPhoto50(String photo50) {
        this.photo50 = photo50;
    }

    /**
     * @return The photo100
     */
    public String getPhoto100() {
        return photo100;
    }

    /**
     * @param photo100 The photo_100
     */
    public void setPhoto100(String photo100) {
        this.photo100 = photo100;
    }

    /**
     * @return The photo200
     */
    public String getPhoto200() {
        return photo200;
    }

    /**
     * @param photo200 The photo_200
     */
    public void setPhoto200(String photo200) {
        this.photo200 = photo200;
    }


    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * @return The status
     */
    public String getStatus() {
        return status;
    }

    /**
     * @param status The status
     */
    public void setStatus(String status) {
        this.status = status;
    }

    public int getmStatusRegistration() {
        return mStatusRegistration;
    }

    public void setmStatusRegistration(int mStatusRegistration) {
        this.mStatusRegistration = mStatusRegistration;
    }
}