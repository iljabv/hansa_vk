package com.ilja.vk_hansa.Model.Vk;

/**
 * Created by Ilja on 01.02.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class VkLikesItem {

    @SerializedName("user_likes")
    @Expose
    private Integer userLikes;
    @SerializedName("count")
    @Expose
    private Integer count;

    /**
     * @return The userLikes
     */
    public Integer getUserLikes() {
        return userLikes;
    }

    /**
     * @param userLikes The user_likes
     */
    public void setUserLikes(Integer userLikes) {
        this.userLikes = userLikes;
    }

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

}
