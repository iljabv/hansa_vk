package com.ilja.vk_hansa.Model.Hansa;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Ilja on 24.01.2016.
 */

public class HansaSearchResponse {

    @SerializedName("itemscount")
    @Expose
    private Integer itemscount;

    @SerializedName("maxpage")
    @Expose
    private Integer maxpage;
    @SerializedName("page")
    @Expose
    private String page;
    @SerializedName("results")
    @Expose
    private List<HansaSearchResponseItem> results = new ArrayList<HansaSearchResponseItem>();

    /**
     * @return The itemstcount
     */
    public Integer getItemscount() {
        return itemscount;
    }

    /**
     * @param itemscount The itemscount
     */
    public void setItemstcount(Integer itemscount) {
        this.itemscount = itemscount;
    }


    /**
     * @return The maxpage
     */
    public Integer getMaxpage() {
        return maxpage;
    }

    /**
     * @param maxpage The maxpage
     */
    public void setMaxpage(Integer maxpage) {
        this.maxpage = maxpage;
    }

    /**
     * @return The page
     */
    public String getPage() {
        return page;
    }

    /**
     * @param page The page
     */
    public void setPage(String page) {
        this.page = page;
    }

    /**
     * @return The results
     */
    public List<HansaSearchResponseItem> getResults() {
        return results;
    }

    /**
     * @param results The results
     */
    public void setResults(List<HansaSearchResponseItem> results) {
        this.results = results;
    }

}