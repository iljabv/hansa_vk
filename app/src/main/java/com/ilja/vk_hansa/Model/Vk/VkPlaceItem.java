package com.ilja.vk_hansa.Model.Vk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by Ilja on 30.01.2016.
 */
public class VkPlaceItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("latitude")
    @Expose
    private Double latitude;
    @SerializedName("longitude")
    @Expose
    private Double longitude;
    @SerializedName("created")
    @Expose
    private Integer created;
    @SerializedName("icon")
    @Expose
    private String icon;
    @SerializedName("group_id")
    @Expose
    private Integer groupId;
    @SerializedName("group_photo")
    @Expose
    private String groupPhoto;
    @SerializedName("checkins")
    @Expose
    private Integer checkins;
    @SerializedName("updated")
    @Expose
    private Integer updated;
    @SerializedName("type")
    @Expose
    private Integer type;
    @SerializedName("country")
    @Expose
    private Integer country;
    @SerializedName("city")
    @Expose
    private Integer city;
    @SerializedName("address")
    @Expose
    private String address;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * @param latitude The latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * @return The longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * @param longitude The longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    /**
     * @return The created
     */
    public Integer getCreated() {
        return created;
    }

    /**
     * @param created The created
     */
    public void setCreated(Integer created) {
        this.created = created;
    }

    /**
     * @return The icon
     */
    public String getIcon() {
        return icon;
    }

    /**
     * @param icon The icon
     */
    public void setIcon(String icon) {
        this.icon = icon;
    }

    /**
     * @return The groupId
     */
    public Integer getGroupId() {
        return groupId;
    }

    /**
     * @param groupId The group_id
     */
    public void setGroupId(Integer groupId) {
        this.groupId = groupId;
    }

    /**
     * @return The groupPhoto
     */
    public String getGroupPhoto() {
        return groupPhoto;
    }

    /**
     * @param groupPhoto The group_photo
     */
    public void setGroupPhoto(String groupPhoto) {
        this.groupPhoto = groupPhoto;
    }

    /**
     * @return The checkins
     */
    public Integer getCheckins() {
        return checkins;
    }

    /**
     * @param checkins The checkins
     */
    public void setCheckins(Integer checkins) {
        this.checkins = checkins;
    }

    /**
     * @return The updated
     */
    public Integer getUpdated() {
        return updated;
    }

    /**
     * @param updated The updated
     */
    public void setUpdated(Integer updated) {
        this.updated = updated;
    }

    /**
     * @return The type
     */
    public Integer getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(Integer type) {
        this.type = type;
    }

    /**
     * @return The country
     */
    public Integer getCountry() {
        return country;
    }

    /**
     * @param country The country
     */
    public void setCountry(Integer country) {
        this.country = country;
    }

    /**
     * @return The city
     */
    public Integer getCity() {
        return city;
    }

    /**
     * @param city The city
     */
    public void setCity(Integer city) {
        this.city = city;
    }

    /**
     * @return The address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address The address
     */
    public void setAddress(String address) {
        this.address = address;
    }

}