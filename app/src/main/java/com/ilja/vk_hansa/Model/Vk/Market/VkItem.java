package com.ilja.vk_hansa.Model.Vk.Market;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ilja.vk_hansa.Model.Vk.VkLikesItem;
import com.ilja.vk_hansa.Model.Vk.VkPhotoItem;

import java.util.ArrayList;
import java.util.List;


public class VkItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;
    @SerializedName("title")
    @Expose
    private String title;
    @SerializedName("description")
    @Expose
    private String description;
    @SerializedName("price")
    @Expose
    private VkPriceItem price;
    @SerializedName("category")
    @Expose
    private VkCategoryItem category;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("thumb_photo")
    @Expose
    private String thumbPhoto;
    @SerializedName("availability")
    @Expose
    private Integer availability;
    @SerializedName("photos")
    @Expose
    private List<VkPhotoItem> photos = new ArrayList<VkPhotoItem>();
    @SerializedName("can_comment")
    @Expose
    private Integer canComment;
    @SerializedName("can_repost")
    @Expose
    private Integer canRepost;
    @SerializedName("likes")
    @Expose
    private VkLikesItem likes;
    @SerializedName("views_count")
    @Expose
    private Integer viewsCount;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The ownerId
     */
    public Integer getOwnerId() {
        return ownerId;
    }

    /**
     * @param ownerId The owner_id
     */
    public void setOwnerId(Integer ownerId) {
        this.ownerId = ownerId;
    }

    /**
     * @return The title
     */
    public String getTitle() {
        return title;
    }

    /**
     * @param title The title
     */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
     * @return The description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description The description
     */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
     * @return The price
     */
    public VkPriceItem getPrice() {
        return price;
    }

    /**
     * @param price The price
     */
    public void setPrice(VkPriceItem price) {
        this.price = price;
    }

    /**
     * @return The category
     */
    public VkCategoryItem getCategory() {
        return category;
    }

    /**
     * @param category The category
     */
    public void setCategory(VkCategoryItem category) {
        this.category = category;
    }

    /**
     * @return The date
     */
    public Integer getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(Integer date) {
        this.date = date;
    }

    /**
     * @return The thumbPhoto
     */
    public String getThumbPhoto() {
        return thumbPhoto;
    }

    /**
     * @param thumbPhoto The thumb_photo
     */
    public void setThumbPhoto(String thumbPhoto) {
        this.thumbPhoto = thumbPhoto;
    }

    /**
     * @return The availability
     */
    public Integer getAvailability() {
        return availability;
    }

    /**
     * @param availability The availability
     */
    public void setAvailability(Integer availability) {
        this.availability = availability;
    }

    /**
     * @return The photos
     */
    public List<VkPhotoItem> getPhotos() {
        return photos;
    }

    /**
     * @param photos The photos
     */
    public void setPhotos(List<VkPhotoItem> photos) {
        this.photos = photos;
    }

    /**
     * @return The canComment
     */
    public Integer getCanComment() {
        return canComment;
    }

    /**
     * @param canComment The can_comment
     */
    public void setCanComment(Integer canComment) {
        this.canComment = canComment;
    }

    /**
     * @return The canRepost
     */
    public Integer getCanRepost() {
        return canRepost;
    }

    /**
     * @param canRepost The can_repost
     */
    public void setCanRepost(Integer canRepost) {
        this.canRepost = canRepost;
    }

    /**
     * @return The likes
     */
    public VkLikesItem getLikes() {
        return likes;
    }

    /**
     * @param likes The likes
     */
    public void setLikes(VkLikesItem likes) {
        this.likes = likes;
    }

    /**
     * @return The viewsCount
     */
    public Integer getViewsCount() {
        return viewsCount;
    }

    /**
     * @param viewsCount The views_count
     */
    public void setViewsCount(Integer viewsCount) {
        this.viewsCount = viewsCount;
    }

}