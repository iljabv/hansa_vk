package com.ilja.vk_hansa.Model.Vk.Market.Comments;

import com.ilja.vk_hansa.Model.Vk.VkLikesItem;

import java.text.SimpleDateFormat;

/**
 * Created by Ilja on 20.03.2016.
 */
public class AdaptiveVkComments {


    private long unix_date;
    private String date;
    private String text;
    private VkLikesItem likes;

    private int owner_id;
    private String owner_name;

    public long getUnix_date() {
        return unix_date;
    }

    public void setUnix_date(Integer unix_date) {
        this.unix_date = unix_date;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public VkLikesItem getLikes() {
        return likes;
    }

    public void setLikes(VkLikesItem likes) {
        this.likes = likes;
    }

    public int getOwner_id() {
        return owner_id;
    }

    public void setOwner_id(int owner_id) {
        this.owner_id = owner_id;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getOwner_photo_url() {
        return owner_photo_url;
    }

    public void setOwner_photo_url(String owner_photo_url) {
        this.owner_photo_url = owner_photo_url;
    }

    private String owner_photo_url;

    public AdaptiveVkComments() {
    }


    public void setStringDateFromUnix(long unixtime) {

        java.util.Date date = new java.util.Date(unixtime * 1000);
        String formattedDate = new SimpleDateFormat("dd/MM/yyyy HH:mm").format(date);
        this.date = formattedDate;

    }

}
