package com.ilja.vk_hansa.Model.Vk.Market;

/**
 * Created by Ilja on 30.01.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class VkMarketItem {

    @SerializedName("enabled")
    @Expose
    private Integer enabled;
    @SerializedName("price_min")
    @Expose
    private String priceMin;
    @SerializedName("price_max")
    @Expose
    private String priceMax;
    @SerializedName("wiki")
    @Expose
    private VkWiki wiki;
    @SerializedName("contact_id")
    @Expose
    private Integer contactId;
    @SerializedName("currency")
    @Expose
    private VkCurrencyItem currency;

    /**
     * @return The enabled
     */
    public Integer getEnabled() {
        return enabled;
    }

    /**
     * @param enabled The enabled
     */
    public void setEnabled(Integer enabled) {
        this.enabled = enabled;
    }

    /**
     * @return The priceMin
     */
    public String getPriceMin() {
        return priceMin;
    }

    /**
     * @param priceMin The price_min
     */
    public void setPriceMin(String priceMin) {
        this.priceMin = priceMin;
    }

    /**
     * @return The priceMax
     */
    public String getPriceMax() {
        return priceMax;
    }

    /**
     * @param priceMax The price_max
     */
    public void setPriceMax(String priceMax) {
        this.priceMax = priceMax;
    }

    /**
     * @return The wiki
     */
    public VkWiki getWiki() {
        return wiki;
    }

    /**
     * @param wiki The wiki
     */
    public void setWiki(VkWiki wiki) {
        this.wiki = wiki;
    }

    /**
     * @return The contactId
     */
    public Integer getContactId() {
        return contactId;
    }

    /**
     * @param contactId The contact_id
     */
    public void setContactId(Integer contactId) {
        this.contactId = contactId;
    }

    /**
     * @return The currency
     */
    public VkCurrencyItem getCurrency() {
        return currency;
    }

    /**
     * @param currency The currency
     */
    public void setCurrency(VkCurrencyItem currency) {
        this.currency = currency;
    }

}