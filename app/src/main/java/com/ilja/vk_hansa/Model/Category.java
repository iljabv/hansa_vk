package com.ilja.vk_hansa.Model;

public class Category {
    private String name;
    private String photo;
    private int id;

    public Category(String name, String photo, int id) {
        this.name = name;
        this.photo = photo;
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return this.name;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public String getPhoto() {
        return this.photo;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getId() {
        return this.id;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
