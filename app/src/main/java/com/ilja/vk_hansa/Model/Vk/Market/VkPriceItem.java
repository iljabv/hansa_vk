package com.ilja.vk_hansa.Model.Vk.Market;

/**
 * Created by Ilja on 01.02.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VkPriceItem {

    @SerializedName("amount")
    @Expose
    private String amount;
    @SerializedName("currency")
    @Expose
    private VkCurrencyItem currency;
    @SerializedName("text")
    @Expose
    private String text;

    /**
     * @return The amount
     */
    public String getAmount() {
        return amount;
    }

    /**
     * @param amount The amount
     */
    public void setAmount(String amount) {
        this.amount = amount;
    }

    /**
     * @return The currency
     */
    public VkCurrencyItem getCurrency() {
        return currency;
    }

    /**
     * @param currency The currency
     */
    public void setCurrency(VkCurrencyItem currency) {
        this.currency = currency;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }

}