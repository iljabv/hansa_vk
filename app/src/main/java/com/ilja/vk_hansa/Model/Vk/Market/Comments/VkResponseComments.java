package com.ilja.vk_hansa.Model.Vk.Market.Comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class VkResponseComments {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("items")
    @Expose
    private List<VkCommentItem> items = new ArrayList<VkCommentItem>();

    @SerializedName("profiles")
    @Expose
    private List<Profile> profiles = new ArrayList<Profile>();
    @SerializedName("groups")
    @Expose
    private List<Group> groups = new ArrayList<Group>();


    private HashMap<Integer, Profile> hash_profiles = new HashMap<Integer, Profile>();

    private HashMap<Integer, Group> hash_groups = new HashMap<Integer, Group>();

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The items
     */
    public List<VkCommentItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<VkCommentItem> items) {
        this.items = items;
    }

    /**
     * @return The profiles
     */
    public List<Profile> getProfiles() {
        return profiles;
    }

    /**
     * @param profiles The profiles
     */
    public void setProfiles(List<Profile> profiles) {

        HashMap<Integer, Profile> nemap = new HashMap<Integer, Profile>();

        for (Profile i : profiles) nemap.put(i.getId(), i);

        this.hash_profiles = nemap;
        this.profiles = profiles;
    }

    /**
     * @return The groups
     */
    public List<Group> getGroups() {
        return groups;
    }

    /**
     * @param groups The groups
     */
    public void setGroups(List<Group> groups) {
        HashMap<Integer, Group> nemap = new HashMap<Integer, Group>();

        for (Group i : groups) nemap.put(-i.getId(), i);

        this.hash_groups = nemap;
        this.groups = groups;
    }


    public HashMap<Integer, Group> getHash_groups() {
        return hash_groups;
    }

    public HashMap<Integer, Profile> getHash_profiles() {
        return hash_profiles;
    }

}