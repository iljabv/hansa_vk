package com.ilja.vk_hansa.Model.Vk.Market.Comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Group {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("screen_name")
    @Expose
    private String screenName;
    @SerializedName("is_closed")
    @Expose
    private Integer isClosed;
    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("is_admin")
    @Expose
    private Integer isAdmin;
    @SerializedName("admin_level")
    @Expose
    private Integer adminLevel;
    @SerializedName("is_member")
    @Expose
    private Integer isMember;
    @SerializedName("photo_100")
    @Expose
    private String photo100;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The screenName
     */
    public String getScreenName() {
        return screenName;
    }

    /**
     * @param screenName The screen_name
     */
    public void setScreenName(String screenName) {
        this.screenName = screenName;
    }

    /**
     * @return The isClosed
     */
    public Integer getIsClosed() {
        return isClosed;
    }

    /**
     * @param isClosed The is_closed
     */
    public void setIsClosed(Integer isClosed) {
        this.isClosed = isClosed;
    }

    /**
     * @return The type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type The type
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return The isAdmin
     */
    public Integer getIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin The is_admin
     */
    public void setIsAdmin(Integer isAdmin) {
        this.isAdmin = isAdmin;
    }

    /**
     * @return The adminLevel
     */
    public Integer getAdminLevel() {
        return adminLevel;
    }

    /**
     * @param adminLevel The admin_level
     */
    public void setAdminLevel(Integer adminLevel) {
        this.adminLevel = adminLevel;
    }

    /**
     * @return The isMember
     */
    public Integer getIsMember() {
        return isMember;
    }

    /**
     * @param isMember The is_member
     */
    public void setIsMember(Integer isMember) {
        this.isMember = isMember;
    }

    /**
     * @return The photo100
     */
    public String getPhoto100() {
        return photo100;
    }

    /**
     * @param photo100 The photo_100
     */
    public void setPhoto200(String photo200) {
        this.photo100 = photo100;
    }

}