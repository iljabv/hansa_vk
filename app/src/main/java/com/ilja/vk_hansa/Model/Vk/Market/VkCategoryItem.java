package com.ilja.vk_hansa.Model.Vk.Market;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ilja.vk_hansa.Model.Vk.VkSectionItem;


public class VkCategoryItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("section")
    @Expose
    private VkSectionItem section;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name The name
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return The section
     */
    public VkSectionItem getSection() {
        return section;
    }

    /**
     * @param section The section
     */
    public void setSection(VkSectionItem section) {
        this.section = section;
    }

}