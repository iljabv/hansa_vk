package com.ilja.vk_hansa.Model.Vk;

/**
 * Created by Ilja on 30.01.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ilja.vk_hansa.Model.Vk.Market.VkAlbumItem;

import java.util.ArrayList;
import java.util.List;


public class VkResponseAlbum {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("items")
    @Expose
    private List<VkAlbumItem> items = new ArrayList<VkAlbumItem>();

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The items
     */
    public List<VkAlbumItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<VkAlbumItem> items) {
        this.items = items;
    }

}