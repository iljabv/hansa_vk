package com.ilja.vk_hansa.Model.Hansa;

/**
 * Created by Ilja on 24.01.2016.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HansaSearchResponseItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("owner_id")
    @Expose
    private Integer ownerId;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The ownerId
     */
    public Integer getOwnerId() {
        return ownerId;
    }
}