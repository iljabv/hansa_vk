package com.ilja.vk_hansa.Model.Vk.Market.Comments;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.ilja.vk_hansa.Model.Vk.VkLikesItem;

public class VkCommentItem {

    @SerializedName("id")
    @Expose
    private Integer id;
    @SerializedName("from_id")
    @Expose
    private Integer fromId;
    @SerializedName("can_edit")
    @Expose
    private Integer canEdit;
    @SerializedName("date")
    @Expose
    private Integer date;
    @SerializedName("text")
    @Expose
    private String text;

    @SerializedName("likes")
    @Expose
    private VkLikesItem likes;

    /**
     * @return The id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id The id
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return The fromId
     */
    public Integer getFromId() {
        return fromId;
    }

    /**
     * @param fromId The from_id
     */
    public void setFromId(Integer fromId) {
        this.fromId = fromId;
    }

    /**
     * @return The canEdit
     */
    public Integer getCanEdit() {
        return canEdit;
    }

    /**
     * @param canEdit The can_edit
     */
    public void setCanEdit(Integer canEdit) {
        this.canEdit = canEdit;
    }

    /**
     * @return The date
     */
    public Integer getDate() {
        return date;
    }

    /**
     * @param date The date
     */
    public void setDate(Integer date) {
        this.date = date;
    }

    /**
     * @return The text
     */
    public String getText() {
        return text;
    }

    /**
     * @param text The text
     */
    public void setText(String text) {
        this.text = text;
    }


    public VkLikesItem getLikes() {
        return likes;
    }

    /**
     * @param text The text
     */
    public void setLikes(VkLikesItem likes) {
        this.likes = likes;
    }


}