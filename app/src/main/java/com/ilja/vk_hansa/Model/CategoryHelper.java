package com.ilja.vk_hansa.Model;

import android.content.res.Resources;

import com.ilja.vk_hansa.R;

import java.util.ArrayList;
import java.util.List;

public class CategoryHelper {

    public List<Category> getMainCategories(Resources res) {
        List<Category> mainCategories = new ArrayList<>();

        mainCategories.add(new Category("Гардероб", "category/wardrobe.png", res.getInteger(R.integer.section_clothes_id)));
        mainCategories.add(new Category("Детские товары", "category/baby-goods.png", res.getInteger(R.integer.section_child_id)));
        mainCategories.add(new Category("Электроника", "category/electronic.png", res.getInteger(R.integer.section_electronics_id)));
        mainCategories.add(new Category("Компьютеры и ПО", "category/computers.png", res.getInteger(R.integer.section_computers_id)));
        mainCategories.add(new Category("Транспорт", "category/transport.png", res.getInteger(R.integer.section_transport_id)));
        mainCategories.add(new Category("Недвижимость", "category/real-estate.png", res.getInteger(R.integer.section_realty_id)));
        mainCategories.add(new Category("Дом и дача", "category/home-goods.png", res.getInteger(R.integer.section_home_id)));
        mainCategories.add(new Category("Красота и здоровье", "category/beauty.png", res.getInteger(R.integer.section_beauty_id)));
        mainCategories.add(new Category("Спорт и отдых", "category/sports.png", res.getInteger(R.integer.section_sport_id)));
        mainCategories.add(new Category("Досуг и подарки", "category/gifts-and-leisure.png", res.getInteger(R.integer.section_gift_id)));
        mainCategories.add(new Category("Домашние питомцы", "category/pets.png", res.getInteger(R.integer.section_pets_id)));
        mainCategories.add(new Category("Продукты питания", "category/food.png", res.getInteger(R.integer.section_food_id)));
        mainCategories.add(new Category("Услуги", "category/sevices.png", res.getInteger(R.integer.section_service_id)));

        return mainCategories;
    }

    public List<Category> getClothesSubCategories(Resources res) {
        List<Category> clothesSubCategories = new ArrayList<>();

        clothesSubCategories.add(new Category("Женская одежда", "category/wardrobe/women_clothes.png", res.getInteger(R.integer.category_clothes_woman_id)));
        clothesSubCategories.add(new Category("Мужская одежда", "category/wardrobe/men_clothes.png", res.getInteger(R.integer.category_clothes_man_id)));
        clothesSubCategories.add(new Category("Детская одежда", "category/wardrobe/baby_clothes.png", res.getInteger(R.integer.category_clothes_children_id)));
        clothesSubCategories.add(new Category("Обувь и сумки", "category/wardrobe/bagd_and_shoes.png", res.getInteger(R.integer.category_clothes_boots_id)));
        clothesSubCategories.add(new Category("Аксессуары и украшения", "category/wardrobe/accessories_and_jewelry.png", res.getInteger(R.integer.category_clothes_accessories_id)));

        return clothesSubCategories;
    }

    public List<Category> getChildSubCategories(Resources res) {
        List<Category> childSubCategories = new ArrayList<>();

        childSubCategories.add(new Category("Автокресла", "category/baby_goods/baby-seat.png", res.getInteger(R.integer.category_child_seats_id)));
        childSubCategories.add(new Category("Детские коляски", "category/baby_goods/pram.png", res.getInteger(R.integer.category_child_carriage_id)));
        childSubCategories.add(new Category("Детская комната", "category/baby_goods/baby-room.png", res.getInteger(R.integer.category_child_room_id)));
        childSubCategories.add(new Category("Игрушки", "category/baby_goods/toys.png", res.getInteger(R.integer.category_child_toys_id)));
        childSubCategories.add(new Category("Мамам и малышам", "category/baby_goods/mother-and-babies.png", res.getInteger(R.integer.category_child_mother_id)));
        childSubCategories.add(new Category("Обучение и творчество", "category/baby_goods/creation.png", res.getInteger(R.integer.category_child_learn_id)));
        childSubCategories.add(new Category("Школьникам", "category/baby_goods/school.png", res.getInteger(R.integer.category_child_school_id)));

        return childSubCategories;
    }

    public List<Category> getElectronicSubCategories(Resources res) {
        List<Category> electronicSubCategories = new ArrayList<>();

        electronicSubCategories.add(new Category("Телефоны и аксессуары", "category/electronic/mobile-phones.png", res.getInteger(R.integer.category_electronics_phones_id)));
        electronicSubCategories.add(new Category("Фото- и видеокамеры", "category/electronic/photo-video.png", res.getInteger(R.integer.category_electronics_photo_id)));
        electronicSubCategories.add(new Category("Аудио- и видеотехника", "category/electronic/audio-technics.png", res.getInteger(R.integer.category_electronics_audio_id)));
        electronicSubCategories.add(new Category("Портативная техника", "category/electronic/portable.png", res.getInteger(R.integer.category_electronics_portable_id)));
        electronicSubCategories.add(new Category("Игровые приставки и игры", "category/electronic/console.png", res.getInteger(R.integer.category_electronics_games_id)));
        electronicSubCategories.add(new Category("Техника для автомобилей", "category/electronic/auto-electronic.png", res.getInteger(R.integer.category_electronics_auto_id)));
        electronicSubCategories.add(new Category("Оптические приборы", "category/electronic/optical.png", res.getInteger(R.integer.category_electronics_optics_id)));

        return electronicSubCategories;
    }

    public List<Category> getComputerSubCategories(Resources res) {
        List<Category> computerSubCategories = new ArrayList<>();

        computerSubCategories.add(new Category("Компьютеры", "category/computers/computers.png", res.getInteger(R.integer.category_computers_computers_id)));
        computerSubCategories.add(new Category("Ноутбуки, нетбуки", "category/computers/laptop.png", res.getInteger(R.integer.category_computers_laptops_id)));
        computerSubCategories.add(new Category("Комплектующие и аксессуары", "category/computers/computer-hardware.png", res.getInteger(R.integer.category_computers_accessories_id)));
        computerSubCategories.add(new Category("Периферийные устройства", "category/computers/peripherals.png", res.getInteger(R.integer.category_computers_peripherals_id)));
        computerSubCategories.add(new Category("Сетевое оборудование", "category/computers/network-equipment.png", res.getInteger(R.integer.category_computers_network_id)));
        computerSubCategories.add(new Category("Оргтехника и расходники", "category/computers/office-equipment.png", res.getInteger(R.integer.category_computers_office_id)));
        computerSubCategories.add(new Category("Фильмы, музыка, программы", "category/computers/music-films-programms.png", res.getInteger(R.integer.category_computers_media_id)));

        return computerSubCategories;
    }

    public List<Category> getTransportSubCategories(Resources res) {
        List<Category> transportSubCategories = new ArrayList<>();

        transportSubCategories.add(new Category("Автомобили", "category/transport/cars.png", res.getInteger(R.integer.category_transport_cars_id)));
        transportSubCategories.add(new Category("Мотоциклы и мототехника", "category/transport/motorcircle.png", res.getInteger(R.integer.category_transport_motorbikes_id)));
        transportSubCategories.add(new Category("Грузовики и спецтехника", "category/transport/trucks.png", res.getInteger(R.integer.category_transport_trucks_id)));
        transportSubCategories.add(new Category("Водный транспорт", "category/transport/water-transport.png", res.getInteger(R.integer.category_transport_sea_id)));
        transportSubCategories.add(new Category("Запчасти и аксессуары", "category/transport/spare-parts.png", res.getInteger(R.integer.category_transport_spare_id)));

        return transportSubCategories;
    }

    public List<Category> getRealtySubCategories(Resources res) {
        List<Category> realtySubCategories = new ArrayList<>();

        realtySubCategories.add(new Category("Квартиры", "category/real_estate/apartment.png", res.getInteger(R.integer.category_realty_flats_id)));
        realtySubCategories.add(new Category("Комнаты", "category/real_estate/rooms.png", res.getInteger(R.integer.category_realty_rooms_id)));
        realtySubCategories.add(new Category("Дома, дачи, коттеджи", "category/real_estate/cottage.png", res.getInteger(R.integer.category_realty_country_id)));
        realtySubCategories.add(new Category("Земельные участки", "category/real_estate/land-estate.png", res.getInteger(R.integer.category_realty_land_id)));
        realtySubCategories.add(new Category("Гаражи и машиноместа", "category/real_estate/garage.png", res.getInteger(R.integer.category_realty_garage_id)));
        realtySubCategories.add(new Category("Коммерческая недвижимость", "category/real_estate/commercial-estate.png", res.getInteger(R.integer.category_realty_commerce_id)));
        realtySubCategories.add(new Category("Недвижимость за рубежом", "category/real_estate/abroad-estate.png", res.getInteger(R.integer.category_realty_foreign_id)));

        return realtySubCategories;
    }

    public List<Category> getHomeSubCategories(Resources res) {
        List<Category> homeSubCategories = new ArrayList<>();

        homeSubCategories.add(new Category("Бытовая техника", "category/home_goods/appliances.png", res.getInteger(R.integer.category_home_tech_id)));
        homeSubCategories.add(new Category("Мебель и интерьер", "category/home_goods/furniture.png", res.getInteger(R.integer.category_home_furniture_id)));
        homeSubCategories.add(new Category("Кухонные принадлежности", "category/home_goods/kitchen.png", res.getInteger(R.integer.category_home_kitchen_id)));
        homeSubCategories.add(new Category("Текстиль", "category/home_goods/textile.png", res.getInteger(R.integer.category_home_textile_id)));
        homeSubCategories.add(new Category("Хозяйственные товары", "category/home_goods/household.png", res.getInteger(R.integer.category_home_goods_id)));
        homeSubCategories.add(new Category("Ремонт и строительство", "category/home_goods/repair.png", res.getInteger(R.integer.category_home_repair_id)));
        homeSubCategories.add(new Category("Дача, сад и город", "category/home_goods/garden.png", res.getInteger(R.integer.category_home_country_id)));

        return homeSubCategories;
    }

    public List<Category> getBeautySubCategories(Resources res) {
        List<Category> beautySubCategories = new ArrayList<>();

        beautySubCategories.add(new Category("Декоративная косметика", "category/beauty/cosmetics.png", res.getInteger(R.integer.category_beauty_cosmetics_id)));
        beautySubCategories.add(new Category("Парфюмерия", "category/beauty/perfume.png", res.getInteger(R.integer.category_beauty_perfume_id)));
        beautySubCategories.add(new Category("Уход за лицом и телом", "category/beauty/facial-and-body.png", res.getInteger(R.integer.category_beauty_face)));
        beautySubCategories.add(new Category("Приборы и аксессуары", "category/beauty/tools.png", res.getInteger(R.integer.category_beauty_accessories_id)));
        beautySubCategories.add(new Category("Оптика", "category/beauty/optics.png", res.getInteger(R.integer.category_beauty_optics_id)));

        return beautySubCategories;
    }

    public List<Category> getSportSubCategories(Resources res) {
        List<Category> sportSubCategories = new ArrayList<>();

        sportSubCategories.add(new Category("Активный отдых", "category/sports/active-sport.png", res.getInteger(R.integer.category_sport_active_id)));
        sportSubCategories.add(new Category("Туризм", "category/sports/tourism.png", res.getInteger(R.integer.category_sport_tourism_id)));
        sportSubCategories.add(new Category("Отдых и рыбалка", "category/sports/hunt-and-fishing.png", res.getInteger(R.integer.category_sport_relax)));
        sportSubCategories.add(new Category("Тренажеры и фитнес", "category/sports/fintess.png", res.getInteger(R.integer.category_sport_fitness_id)));
        sportSubCategories.add(new Category("Игры", "category/sports/sport-games.png", res.getInteger(R.integer.category_sport_games_id)));

        return sportSubCategories;
    }

    public List<Category> getGiftSubCategories(Resources res) {
        List<Category> giftSubCategories = new ArrayList<>();

        giftSubCategories.add(new Category("Билеты и путешествия", "category/gifts_and_leisure/travel.png", res.getInteger(R.integer.category_gift_tickets_id)));
        giftSubCategories.add(new Category("Книги и журналы", "category/gifts_and_leisure/books.png", res.getInteger(R.integer.category_gift_books_id)));
        giftSubCategories.add(new Category("Коллекционирование", "category/gifts_and_leisure/collecting.png", res.getInteger(R.integer.category_gift_collection_id)));
        giftSubCategories.add(new Category("Музыкальные инструменты", "category/gifts_and_leisure/music.png", res.getInteger(R.integer.category_gift_music_id)));
        giftSubCategories.add(new Category("Настольные игры", "category/gifts_and_leisure/board-games.png", res.getInteger(R.integer.category_gift_games_id)));
        giftSubCategories.add(new Category("Подарочные наборы и сертификаты", "category/gifts_and_leisure/gifts-and-certificates.png", res.getInteger(R.integer.category_gift_certificate_id)));
        giftSubCategories.add(new Category("Сувениры и цветы", "category/gifts_and_leisure/souvenirs-and-flowers.png", res.getInteger(R.integer.category_gift_flowers_id)));
        giftSubCategories.add(new Category("Рукоделие", "category/gifts_and_leisure/sewing.png", res.getInteger(R.integer.category_gift_handmade_id)));

        return giftSubCategories;
    }

    public List<Category> getPetsSubCategories(Resources res) {
        List<Category> petsSubCategories = new ArrayList<>();

        petsSubCategories.add(new Category("Собаки", "category/pets/dogs.png", res.getInteger(R.integer.category_pets_dogs_id)));
        petsSubCategories.add(new Category("Кошки", "category/pets/cats.png", res.getInteger(R.integer.category_pets_cats_id)));
        petsSubCategories.add(new Category("Грызуны", "category/pets/rodents.png", res.getInteger(R.integer.category_pets_rats_id)));
        petsSubCategories.add(new Category("Птицы", "category/pets/birds.png", res.getInteger(R.integer.category_pets_birds_id)));
        petsSubCategories.add(new Category("Рыбы", "category/pets/fish.png", res.getInteger(R.integer.category_pets_fish_id)));
        petsSubCategories.add(new Category("Другие животные", "category/pets/other-pets.png", res.getInteger(R.integer.category_pets_other_id)));
        petsSubCategories.add(new Category("Корма и аксессуары", "category/pets/food-and-equipment.png", res.getInteger(R.integer.category_pets_food_id)));

        return petsSubCategories;
    }

    public List<Category> getFoodSubCategories(Resources res) {
        List<Category> foodSubCategories = new ArrayList<>();

        foodSubCategories.add(new Category("Бакалея", "category/food/grocery.png", res.getInteger(R.integer.category_food_grocery_id)));
        foodSubCategories.add(new Category("Биопродукты", "category/food/bio-food.png", res.getInteger(R.integer.category_food_bio_id)));
        foodSubCategories.add(new Category("Детское питание", "category/food/baby-food.png", res.getInteger(R.integer.category_food_child)));
        foodSubCategories.add(new Category("Еда на заказ", "category/food/meal-to-order.png", res.getInteger(R.integer.category_food_order_id)));
        foodSubCategories.add(new Category("Напитки", "category/food/drinks.png", res.getInteger(R.integer.category_food_drink_id)));

        return foodSubCategories;
    }

    public List<Category> getServiceSubCategories(Resources res) {
        List<Category> serviceFoodCategories = new ArrayList<>();

        serviceFoodCategories.add(new Category("Фото- и видеосъемка", "category/services/photo-services.png", res.getInteger(R.integer.category_service_photo_id)));
        serviceFoodCategories.add(new Category("Удаленная работа", "category/services/remote-work.png", res.getInteger(R.integer.category_service_remote_id)));
        serviceFoodCategories.add(new Category("Организация мероприятий", "category/services/event-organization.png", res.getInteger(R.integer.category_service_organization_id)));
        serviceFoodCategories.add(new Category("Красота и здоровье", "category/services/beauty-services.png", res.getInteger(R.integer.category_service_beauty_id)));
        serviceFoodCategories.add(new Category("Установка и ремонт техники", "category/services/repair-services.png", res.getInteger(R.integer.category_service_tech_id)));
        serviceFoodCategories.add(new Category("Уборка и помощь по хозяйству", "category/services/cleaning-services.png", res.getInteger(R.integer.category_service_cleaning_id)));
        serviceFoodCategories.add(new Category("Курьеры и грузоперевозки", "category/services/delivery-services.png", res.getInteger(R.integer.category_service_courier_id)));
        serviceFoodCategories.add(new Category("Обучение и развитие", "category/services/education-services.png", res.getInteger(R.integer.category_service_learn_id)));
        serviceFoodCategories.add(new Category("Финансовые услуги", "category/services/finance-services.png", res.getInteger(R.integer.category_service_finance_id)));
        serviceFoodCategories.add(new Category("Консультации специалистов", "category/services/consultation.png", res.getInteger(R.integer.category_service_consult_id)));

        return serviceFoodCategories;
    }
}
