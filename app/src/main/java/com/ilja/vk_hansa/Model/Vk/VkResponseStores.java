package com.ilja.vk_hansa.Model.Vk;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;


public class VkResponseStores {

    @SerializedName("count")
    @Expose
    private Integer count;
    @SerializedName("items")
    @Expose
    private List<VkStoreItem> items = new ArrayList<>();

    /**
     * @return The count
     */
    public Integer getCount() {
        return count;
    }

    /**
     * @param count The count
     */
    public void setCount(Integer count) {
        this.count = count;
    }

    /**
     * @return The items
     */
    public List<VkStoreItem> getItems() {
        return items;
    }

    /**
     * @param items The items
     */
    public void setItems(List<VkStoreItem> items) {
        this.items = items;
    }

}