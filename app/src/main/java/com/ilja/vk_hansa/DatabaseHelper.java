package com.ilja.vk_hansa;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.ilja.vk_hansa.Model.Vk.Market.VkCategoryItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkItem;
import com.ilja.vk_hansa.Model.Vk.Market.VkPriceItem;
import com.ilja.vk_hansa.Model.Vk.VkPhotoItem;
import com.ilja.vk_hansa.UI.UIHelps.HelpGetExistingPhoto;

import java.util.ArrayList;
import java.util.List;

public class DatabaseHelper extends SQLiteOpenHelper {
    private final String LOG_TAG = "DatabaseHelper";
    private static final String DATABASE_NAME = "hansa.db";
    SQLiteDatabase db;
    private static final int SCHEMA = 3;
    static final String TABLE = "items";

    public final String COLUMN_ID = "_id";
    public final String COLUMN_ITEMID = "itemid";
    public final String COLUMN_OWNERID = "ownerid";
    public final String COLUMN_NAME = "tile";
    public final String COLUMN_PRICE = "price";
    public final String COLUMN_CATEGORYID = "category";
    public final String COLUMN_PHOTO = "photo";
    public final String COLUMN_ISINTERST = "interest";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, SCHEMA);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL("CREATE TABLE " + TABLE + " (" +
                COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                COLUMN_ITEMID + " INTEGER, " +
                COLUMN_OWNERID + " INTEGER, " +
                COLUMN_NAME + " TEXT, " +
                COLUMN_PRICE + " TEXT, " +
                COLUMN_CATEGORYID + " TEXT, " +
                COLUMN_PHOTO + " TEXT, " +
                COLUMN_ISINTERST + " INTEGER);");


        // добавление начальных данных
//        db.execSQL("INSERT INTO "+ TABLE +" (" + COLUMN_NAME
//                + ", " + COLUMN_YEAR  + ") VALUES ('Том Смит', 1981);");
    }


    public void addRecentlyViewedItems(List<VkItem> list, boolean interest) {
        //    Log.d(LOG_TAG, "Add Recently Viewed Items");

        db = this.getReadableDatabase();

        //    String[] args = {item.getId().toString(),item.getOwnerId().toString()};

        Cursor mCursor;

        if (interest) {
            mCursor = db.rawQuery("SELECT * FROM " + TABLE + " WHERE " + COLUMN_ISINTERST + " = 1" + " ORDER BY " + COLUMN_ID + " DESC", null);
        } else {
            mCursor = db.rawQuery("SELECT * FROM " + TABLE + " ORDER BY " + COLUMN_ID + " DESC", null);
        }


        if (mCursor.getCount() < 1) return;

        mCursor.moveToFirst();
        for (int i = 0; i < mCursor.getCount(); i++) {

            VkItem item = new VkItem();

            item.setId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_ITEMID)));
            item.setOwnerId(mCursor.getInt(mCursor.getColumnIndex(COLUMN_OWNERID)));
            item.setTitle(mCursor.getString(mCursor.getColumnIndex(COLUMN_NAME)));

            VkPriceItem price = new VkPriceItem();
            price.setText(mCursor.getString(mCursor.getColumnIndex(COLUMN_PRICE)));
            item.setPrice(price);

            //   Log.d(LOG_TAG,mCursor.getInt(mCursor.getColumnIndex(COLUMN_ISINTERST))+" ");

            VkCategoryItem category = new VkCategoryItem();
            category.setName(mCursor.getString(mCursor.getColumnIndex(COLUMN_CATEGORYID)));
            item.setCategory(category);

            VkPhotoItem photo = new VkPhotoItem();
            photo.setPhoto604(mCursor.getString(mCursor.getColumnIndex(COLUMN_PHOTO)));
            List<VkPhotoItem> list_photo = new ArrayList<>();
            list_photo.add(photo);
            item.setPhotos(list_photo);

            list.add(item);
            mCursor.moveToNext();
        }
        db.close();
    }

    public boolean insertItem(VkItem item) {

        //  Log.d(LOG_TAG,"Insert Item " + item.getOwnerId()+"_"+item.getId());

        if (itemExistInDB(item)) {
            return false;
        }

        db = this.getReadableDatabase();

        ContentValues values = new ContentValues();

        values.put(COLUMN_ITEMID, item.getId());
        values.put(COLUMN_OWNERID, item.getOwnerId());
        values.put(COLUMN_NAME, item.getTitle());
        values.put(COLUMN_PRICE, item.getPrice().getText());
        values.put(COLUMN_CATEGORYID, item.getCategory().getName());

        HelpGetExistingPhoto helpPhoto = new HelpGetExistingPhoto();

        if (item.getPhotos() != null && item.getPhotos().size() > 0) {
            String photoURL = helpPhoto.getPhoto(item.getPhotos().get(0));
            if (photoURL != null) values.put(COLUMN_PHOTO, photoURL);
        } else {
            values.put(COLUMN_PHOTO, R.drawable.vk_error_dog);
        }
        values.put(COLUMN_ISINTERST, 0);

        db.insert(TABLE, null, values);
        db.close();
        return true;

    }

    private boolean itemExistInDB(VkItem item) {
        //   Log.d(LOG_TAG,"Check Item Exist " + item.getOwnerId()+"_"+item.getId());

        db = this.getReadableDatabase();
        String[] args = {item.getId().toString(), item.getOwnerId().toString()};

        Cursor c = db.rawQuery("SELECT * FROM " + TABLE + " WHERE " +
                COLUMN_ITEMID + "=" + item.getId() + " AND " + COLUMN_OWNERID + "=" + item.getOwnerId(), null);

        c.moveToFirst();
        //  Log.d(LOG_TAG, "Cursor count " + c.getCount());

        if (c.getCount() > 0) {
            db.close();/*Log.d(LOG_TAG,"Item exist");*/
            return true;
        } else {
            db.close();/*Log.d(LOG_TAG, "Item doesn't exist");*/
            return false;
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE);
        onCreate(db);
    }

    public void setInterested(VkItem item) {

        // Log.d(LOG_TAG,"Update Interest: " + item.getOwnerId()+"_"+item.getId());

        if (!itemExistInDB(item)) return;

        db = this.getReadableDatabase();

        db.execSQL("UPDATE " + TABLE + " SET " + COLUMN_ISINTERST + " = 1 " + " WHERE " + COLUMN_ITEMID + "=" + item.getId() + " AND " + COLUMN_OWNERID + " = " + item.getOwnerId());

        db.close();

    }
}